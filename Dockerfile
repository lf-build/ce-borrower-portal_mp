# Pure nginx / html
# FROM funkygibbon/nginx-pagespeed
# ADD /build /app/www
# EXPOSE 80 443

FROM node

ADD /.npmrc /tmp/.npmrc
ADD /package.json /tmp/package.json
WORKDIR /tmp
RUN npm install
ADD . /tmp/
EXPOSE 3000

ENTRYPOINT npm run start:production