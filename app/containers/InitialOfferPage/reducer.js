/*
 *
 * InitialOfferPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  GENERATE_INITIAL_OFFER_REQUEST_FULFILLED,
  GENERATE_INITIAL_OFFER_REQUEST_REJECTED,
  GENERATE_INITIAL_OFFER_REQUEST_FAILED,
  GENERATE_INITIAL_OFFER_REQUEST_DENIED,
  GENERATE_INITIAL_OFFER_REQUEST_STARTED,
  GENERATE_INITIAL_OFFER_REQUEST_ENDED,
} from './constants';

const initialState = fromJS({});

function initialOfferPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case GENERATE_INITIAL_OFFER_REQUEST_STARTED:
      return state
             .set('status', undefined)
             .set('offer', undefined)
             .set('loading', true);
    case GENERATE_INITIAL_OFFER_REQUEST_ENDED:
      return state
             .set('loading', false);
    case GENERATE_INITIAL_OFFER_REQUEST_DENIED:
      return state
             .set('status', 'denied')
             .set('offer', action.meta.body);
    case GENERATE_INITIAL_OFFER_REQUEST_FAILED:
      return state
             .set('status', 'failed')
             .set('offer', action.meta.body);
    case GENERATE_INITIAL_OFFER_REQUEST_REJECTED:
      return state
             .set('status', 'rejected')
             .set('offer', action.meta.body);
    case GENERATE_INITIAL_OFFER_REQUEST_FULFILLED:
      return state
             .set('status', 'generated')
             .set('offer', action.meta.body);
    default:
      return state;
  }
}

export default initialOfferPageReducer;
