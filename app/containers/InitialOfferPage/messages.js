/*
 * InitialOfferPage Messages
 *
 * This contains all the text for the InitialOfferPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.InitialOfferPage.header',
    defaultMessage: 'Congratulations {name}!',
  },
  loadingHeader: {
    id: 'app.containers.ReviewApplicationPage.loadingHeader',
    defaultMessage: 'Determining your credit eligibility',
  },
  errorHeader: {
    id: 'app.containers.ReviewApplicationPage.errorHeader',
    defaultMessage: 'Oops! Something went wrong, we are unable to generate your offer as of now. Please try again later.',
  },
  loadingSubHeader: {
    id: 'app.containers.ReviewApplicationPage.loadingSubHeader',
    defaultMessage: 'Please wait while we determine your credit eligibility! This may take a few minutes.',
  },
});
