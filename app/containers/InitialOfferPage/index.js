/*
 *
 * InitialOfferPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { injectIntl, FormattedMessage } from 'react-intl';
// import { injectIntl, intlShape, FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import makeSelectInitialOfferPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import InitialOfferNavigationPanel from '../../components/InitialOfferNavigationPanel';
import { CheckPassIcon, HourglassIcon } from '../../components/Icons';
import makeSelectApp from '../App/selectors';
import { generateInitialOffer, getBasicAppInfoRequest } from './actions';
import internalServerError from './internal-server-error.png';
// import RBLLogo from '../../components/Icons/assets/images/rbl-logo.png'; // commenting as per CEPROD-376

const CongratulationText = styled.p`
    // font-size: 18px;
    // font-weight: 400;
    margin: 20px 0 20px;
    text-align: center;
    font-size: 19px;
    line-height: 27px;
    font-weight: 100;
    letter-spacing: 0.025em;
`;

// const StrongText = styled.strong`
//     font-size: 22px;
//     text-align: center;
// `;

const Heading = styled.p`
    text-align: center;
    font-weight: 600;
    line-height: 16px;
    text-transform: uppercase;
    font-size: 11px;
    letter-spacing: 1px;
`;

const AppNoOrName = styled.p`
    text-align: center;
    font-size: 14px;
    letter-spacing: 0.025em;
    font-weight: 100;
`;

export class InitialOfferPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.dispatch(generateInitialOffer());

    const {
      App: {
        application,
      },
    } = this.props;

    if (!application) {
      this.props.dispatch(getBasicAppInfoRequest());
    }
  }
  render() {
    const {
      InitialOfferPage: {
        loading,
      },
      App: {
        application,
        initialOffer: offer,
      },
    } = this.props;

    if (!application) {
      return <span />;
    }

    if (offer && offer.status === 'Failed') {
      return (
        <LoanStep noBorder name={'initial-offer'} title={messages.errorHeader} navigationPanel={() => <span />}>
          <center>
            <img
              alt="Internal Server Error"
              style={{
                width: 450,
              }} src={internalServerError}
            />
          </center>
        </LoanStep>
      );
    }


    const {
          firstName,
          lastName,
          applicationNumber,
        } = application;
    // const formatNumber = (text) => this.props.intl.formatNumber(text, {
    //   style: 'currency',
    //   currency: 'INR',
    //   minimumFractionDigits: 0,
    // });

    const { header: headerMessage } = messages;
    const nameArray = firstName.split('');
    nameArray[0] = nameArray[0].toUpperCase();
    headerMessage.defaultMessage = headerMessage.defaultMessage.replace('{name}', nameArray.join(''));

    if (!offer || loading || (offer && offer.status === 'rejected')) {
      return (
        <LoanStep name={'initial-offer'} title={messages.loadingHeader} navigationPanel={() => <span />}>
          <HourglassIcon style={{ transform: 'scale(1.5)' }} />
          <div className="spinner">
            <div className="bounce1" style={{ backgroundColor: '#79cdd5' }}></div>
            <div className="bounce2" style={{ backgroundColor: '#79cdd5' }}></div>
            <div className="bounce3" style={{ backgroundColor: '#79cdd5' }}></div>
          </div>
          <span style={{ textAlign: 'center', marginTop: '20px' }} className={'center-block'}>
            <CongratulationText>
              <FormattedMessage {...messages.loadingSubHeader} />
            </CongratulationText>
          </span>
        </LoanStep>
      );
    }
    return (
      <LoanStep
        name={'initial-offer'}
        title={headerMessage}
        navigationPanel={(props) => <InitialOfferNavigationPanel nextDisabled={false} offerType={'initial'} {...props} />}
        noBack
      >
        <CheckPassIcon />
        <CongratulationText>
          You are eligible
          {/* You are eligible for a loan of
          <br />
          <StrongText>{formatNumber(offer.finalOfferAmount)}</StrongText> */}
        </CongratulationText>

        <Heading>
          APPLICATION NUMBER
        </Heading>
        <AppNoOrName> {applicationNumber} </AppNoOrName>

        <Heading>
          APPLICANT NAME
        </Heading>
        <AppNoOrName style={{ textTransform: 'capitalize' }}> {` ${firstName} ${lastName}` } </AppNoOrName>

        <AppNoOrName>
          Complete two more easy steps to generate your personalised loan offer!
        </AppNoOrName>
        {/* <Heading style={{ textTransform: 'none' }}>
          Your loan will be disbursed by
        </Heading>
        <div style={{ textAlign: 'center' }}>
          <img alt="RBL BANK" src={RBLLogo} style={{ width: '120px' }} />
        </div> */}
      </LoanStep>
    );
  }
}

InitialOfferPage.propTypes = {
  InitialOfferPage: PropTypes.object,
  // intl: intlShape.isRequired,
  App: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  InitialOfferPage: makeSelectInitialOfferPage(),
  App: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(InitialOfferPage));
