import { put, takeLatest, select } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { navigationHelper } from '../NavigationHelper/helper';


// import { LOCATION_CHANGE } from 'react-router-redux';
import {
  GENERATE_INITIAL_OFFER_REQUEST,
  GET_BASIC_APP_INFO_REQUEST,
  GENERATE_INITIAL_OFFER_REQUEST_FULFILLED,
} from './constants';
import * as actions from './actions';

export function* generateInitialOffer() {

  const form = 'review';
  // Select username from store
  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'offer',
    command: 'generate-initial',
  }, { tag: form });

  if (executionRequestAccepted) {
    yield put(actions.generateInitialOfferStarted());

    // Wait for Uplink to either fail or succeed.
    const { valid, invalid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);
    if (valid) {
      if (valid.meta && valid.meta.body && valid.meta.body.status === 'Rejected') {
        navigationHelper.goTo({ name: 'initialOffer' }, 'applicationRejectedPage', false);
      }
      yield put(actions.generateInitialOfferFulfilled(valid.meta));
    } else {
      yield put(actions.generateInitialOfferFailed(invalid.meta));
      navigationHelper.goTo({ name: 'initialOffer' }, 'cibilFailed', false);
    }
  } else {
    yield put(actions.generateInitialOfferDenied({ message: 'Uplink did not accept the request.' }));
    navigationHelper.goTo({ name: 'initialOffer' }, 'cibilFailed', false);
  }

  yield put(actions.generateInitialOfferEnded());
}

export function* getBasicAppInfo() {
  const form = 'get-basic-app-info';

  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'application',
    command: 'get-basic-app-info',
  }, { tag: form });

  if (executionRequestAccepted) {
    yield put(actions.getBasicAppInfoRequestStarted());

    const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);

    if (valid) {
      yield put(actions.getBasicAppInfoRequestFulfilled(valid.meta.body));
    } else {
      yield put(actions.getBasicAppInfoRequestFailed('error while fetching basic app information'));
    }

    yield put(actions.getBasicAppInfoRequestEnded());
  }
}
export function* getLogos() {
  const form = 'get-logos';

  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'helper',
    command: 'get-logos',
  }, { tag: form });

  if (executionRequestAccepted) {
    yield put(actions.getLogosRequestStarted());

    const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);

    if (valid) {
      const state = yield select();
      const appState = state.toJS().app;

      const { initialOffer } = appState;

      let headerImg;
      let lenderImg;
      let productId;

      if (initialOffer) {
        productId = initialOffer.productId;
      } else {
        return;
      }

      const getLogo = (logosReponse) => {
        if (logosReponse) {
          const { headerLogo, lenderLogo } = logosReponse;

          if (headerLogo) {
            headerImg = headerLogo[productId];
          }

          if (lenderLogo) {
            lenderImg = lenderLogo[productId];
          }

          return {
            headerLogo: headerImg,
            lenderLogo: lenderImg,
          };
        }

        return undefined;
      };

      const logos = getLogo(valid.meta.body);
      yield put(actions.getLogosRequestFulfilled(logos));
    } else {
      yield put(actions.getLogosRequestFailed('error while fetching logos'));
    }

    yield put(actions.getLogosRequestEnded());
  }
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeLatest(GENERATE_INITIAL_OFFER_REQUEST, generateInitialOffer);
  yield takeLatest(GET_BASIC_APP_INFO_REQUEST, getBasicAppInfo);
  yield takeLatest(GENERATE_INITIAL_OFFER_REQUEST_FULFILLED, getLogos);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
