/*
 *
 * InitialOfferPage actions
 *
 */

import {
  DEFAULT_ACTION,
  GENERATE_INITIAL_OFFER_REQUEST,
  GENERATE_INITIAL_OFFER_REQUEST_STARTED,
  GENERATE_INITIAL_OFFER_REQUEST_FULFILLED,
  GENERATE_INITIAL_OFFER_REQUEST_REJECTED,
  GENERATE_INITIAL_OFFER_REQUEST_FAILED,
  GENERATE_INITIAL_OFFER_REQUEST_ENDED,
  GENERATE_INITIAL_OFFER_REQUEST_DENIED,
  GET_BASIC_APP_INFO_REQUEST,
  GET_BASIC_APP_INFO_REQUEST_STARTED,
  GET_BASIC_APP_INFO_REQUEST_FULFILLED,
  GET_BASIC_APP_INFO_REQUEST_ENDED,
  GET_BASIC_APP_INFO_REQUEST_FAILED,
  GET_LOGOS_REQUEST,
  GET_LOGOS_REQUEST_STARTED,
  GET_LOGOS_REQUEST_FULFILLED,
  GET_LOGOS_REQUEST_ENDED,
  GET_LOGOS_REQUEST_FAILED,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function generateInitialOffer() {
  return {
    type: GENERATE_INITIAL_OFFER_REQUEST,
  };
}

export function generateInitialOfferStarted() {
  return {
    type: GENERATE_INITIAL_OFFER_REQUEST_STARTED,
  };
}

export function generateInitialOfferFulfilled(meta) {
  return {
    type: GENERATE_INITIAL_OFFER_REQUEST_FULFILLED,
    meta,
  };
}

export function generateInitialOfferRejected() {
  return {
    type: GENERATE_INITIAL_OFFER_REQUEST_REJECTED,
  };
}

export function generateInitialOfferFailed(meta) {
  return {
    type: GENERATE_INITIAL_OFFER_REQUEST_FAILED,
    meta,
  };
}

export function generateInitialOfferDenied(meta) {
  return {
    type: GENERATE_INITIAL_OFFER_REQUEST_DENIED,
    meta,
  };
}

export function generateInitialOfferEnded() {
  return {
    type: GENERATE_INITIAL_OFFER_REQUEST_ENDED,
  };
}

export function getBasicAppInfoRequest() {
  return {
    type: GET_BASIC_APP_INFO_REQUEST,
  };
}

export function getBasicAppInfoRequestStarted() {
  return {
    type: GET_BASIC_APP_INFO_REQUEST_STARTED,
  };
}

export function getBasicAppInfoRequestFulfilled(appInfo) {
  return {
    type: GET_BASIC_APP_INFO_REQUEST_FULFILLED,
    meta: {
      appInfo,
    },
  };
}

export function getBasicAppInfoRequestFailed(error) {
  return {
    type: GET_BASIC_APP_INFO_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function getBasicAppInfoRequestEnded() {
  return {
    type: GET_BASIC_APP_INFO_REQUEST_ENDED,
  };
}
export function getLogosRequest() {
  return {
    type: GET_LOGOS_REQUEST,
  };
}

export function getLogosRequestStarted() {
  return {
    type: GET_LOGOS_REQUEST_STARTED,
  };
}

export function getLogosRequestFulfilled(logoInfo) {
  return {
    type: GET_LOGOS_REQUEST_FULFILLED,
    meta: {
      logoInfo,
    },
  };
}

export function getLogosRequestFailed(error) {
  return {
    type: GET_LOGOS_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function getLogosRequestEnded() {
  return {
    type: GET_LOGOS_REQUEST_ENDED,
  };
}
