/*
 *
 * LoanStep
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Paper from 'material-ui/Paper';
// import RaisedButton from 'material-ui/RaisedButton';
import styled from 'styled-components';
import LinearProgress from 'material-ui/LinearProgress';
// import Dialog from 'material-ui/Dialog';
// import FlatButton from 'material-ui/FlatButton';
// import RaisedButton from 'material-ui/RaisedButton';
// import Warning from 'material-ui/svg-icons/action/report-problem';
import * as actions from './actions';

import withNavigationHelper from '../NavigationHelper';
import makeSelectLoanStep from './selectors';
import DefaultLoanStepNavigationPanel from '../../components/DefaultLoanStepNavigationPanel';
// import ErrorDialog from '../../components/ErrorDialog';

const Title = styled.h2`
    font-size: 22px;
    font-weight: 100;
    letter-spacing: 0.03em;
    margin-top: 20px;
    // padding: 0 70px;
    line-height: normal;
    text-align: center;
    color: #3c3c3c;
`;

const Wrapper = styled.div`
  // marginTop: 30px;
  // position: fixed;
  width: 100%;
  // height: calc(100vh - 64px);
  // overflow-y: auto;
  // overflow-x: hidden;
`;

const ShortDescriptionContainer = styled.div`
    font-size: 13px;
    font-weight: 100;
    line-height: 1em;
    margin: 18px 0 0 0;
    padding: 0;
    letter-spacing: 0.025em;
    text-align: center;
`;

export class LoanStep extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.dispatch(actions.stepLoaded(this.props.name || 'unnamed'));
  }
  // componentDidMount() {
  //   const { loanStep: { saveFailed } } = this.props;
  //   if (saveFailed) {
  //     this.handleOpen();
  //   }
  // }
  handleClose = () => {
    this.props.dispatch(actions.clearError());
  };

  render() {
    const {
      title,
      children,
      navigationHelper,
      // height,
      ShortDescription,
      noPadding,
      valid,
      // submitting,
      formAttached,
      readonly,
      name,
      // pristine,
      navigationPanel,
      payloadGenerator,
      loanStep: { loading, saveFailed, validationFailed },
      reduceWidth,
      saveErrorClass,
      simulateCurrentRoute,
      noBack,
      nextButtonName,
      noBorder,
      fullWidth,
      disableNext,
    } = this.props;
    const NavigationPanel = navigationPanel || DefaultLoanStepNavigationPanel;
    const handleGoBack = () => {
      navigationHelper.goBack();
    };
    const handleGoNext = () => {
      navigationHelper.goNext(formAttached ? name : undefined, payloadGenerator, simulateCurrentRoute);
    };

    // To disable next if form is not valid then use (!valid || submitting) below
    const isLoadingOrSubmiting = loading || false; // (formAttached && loading) || false; // (submitting);

    // for showing Documents required note.
    // const pathSplit = location.pathname.split('/');
    // const page = pathSplit.length >= 3 ? pathSplit[2] : 'none';
    // const hideNotePages = [
    //   'request-failed',
    //   'application-rejected',
    //   'sign-in',
    //   'cibil-failed',
    // ];
    // const hideNote = hideNotePages.filter((p) => p === page).length > 0;

    const getPaperStyle = () => {
      if (readonly) {
        return 'disabled';
      }
      if (loading) {
        return 'disabled';
      }
      if (saveFailed) {
        // return `error ${saveErrorClass || 'save'}`;
        return `${saveErrorClass || 'save'}`;
      }

      if (!valid && validationFailed) {
        return 'validation';
      }

      return '';
    };
    const blockWidthClasses = () => {
      if (reduceWidth) {
        return 'col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1';
      } else if (fullWidth) {
        return 'col-xs-12';
      }

      return 'col-sm-10 col-sm-offset-1 col-xs-12';
    };
    return (
      <Wrapper style={{ minHeight: '100%' }}>
        {/* <Dialog
          title="Oops !"
          actions={[
            <RaisedButton
              label="Ok"
              primary
              keyboardFocused
              onTouchTap={this.handleClose}
            />,
          ]}
          modal={false}
          open={saveFailed || false}
          onRequestClose={this.handleClose}
        > <div style={{ paddingTop: 10, paddingLeft: 10 }}>
          <Warning
            style={{
              color: '#ff756a',
              verticalAlign: 'middle',
              display: 'inline-block',
              transform: 'scale(2)',
            }}
          />
          <span style={{ verticalAlign: 'middle', marginLeft: 10, width: '73%', display: 'inline-block', paddingLeft: 10 }}>Failed to save. Please try again after some time. </span>
        </div>
        </Dialog>*/}
        <div className="row" style={{ marginLeft: 0, marginRight: 0, display: 'block' }}>
          <div className={blockWidthClasses()}>
            <div className="center-block pageHeadingSpacing">
              <Title> {title.defaultMessage} </Title>
              <ShortDescriptionContainer className={'pageSubHeading'}> {ShortDescription && <ShortDescription navigationHelper={navigationHelper} />} </ShortDescriptionContainer>
            </div>
            <Paper className={getPaperStyle()} style={{ ...(noBorder ? { boxShadow: undefined } : { boxShadow: '0 0 12px 0 rgba(0, 0, 0, 0.2)' }), position: 'relative', overflow: 'hidden', marginTop: '0px', borderRadius: '10px', padding: noPadding ? '0px' : '30px' }}>
              <form id={name || 'unammed-form'} name={name || 'unammed-form'}>
                { loading && !noBorder ? <LinearProgress color={'#59d2d3'} style={{ position: 'fixed', height: '4px', left: '0px', top: '65px', backgroundColor: '#cff0f1', zIndex: '1001' }} mode="indeterminate" /> : <span />}
                {children}
              </form>
            </Paper>
            <div className="center-block navigationButtonSpacing">
              <NavigationPanel valid={valid} isLoadingOrSubmiting={isLoadingOrSubmiting} disableNext={disableNext} {...{ nextButtonName, noBack, handleGoBack, handleGoNext }} />
              {/* <div className={`row ${hideNote ? 'hidden' : ''}`}>
                <div className={`col-xs-12 ${page === 'reason' ? 'reason-page-note' : ''}`} style={{ marginTop: '20px' }}>
                  <span style={{ marginTop: '10px', fontSize: '13px', lineHeight: '18px' }}>Documents required for successful disbursal within 48 hours:</span>
                  <ul style={{ marginTop: '10px', marginLeft: '55px', fontSize: '11px', lineHeight: '18px' }}>
                    <li>Copy of PAN card.</li>
                    <li>Copy of Aadhaar card.</li>
                    <li>One passport size photograph.</li>
                    <li>4 cheques from your salary bank account.</li>
                    <li>Local residence address proof.</li>
                  </ul>
                </div>
              </div> */}
            </div>
            <div style={{ height: '100px' }}></div>
          </div>
        </div>
      </Wrapper>
    );
  }
}

LoanStep.propTypes = {
  title: React.PropTypes.shape({
    id: React.PropTypes.string,
    defaultMessage: React.PropTypes.string,
  }),
  children: React.PropTypes.node,
  navigationHelper: React.PropTypes.object,
  // height: React.PropTypes.string,
  ShortDescription: React.PropTypes.func,
  noPadding: React.PropTypes.bool,
  valid: React.PropTypes.bool,
  navigationPanel: React.PropTypes.func,
  // pristine: React.PropTypes.bool,
  // submitting: React.PropTypes.bool,
  formAttached: React.PropTypes.bool,
  readonly: React.PropTypes.bool,
  payloadGenerator: React.PropTypes.func,
  dispatch: React.PropTypes.func,
  loanStep: React.PropTypes.shape({
    loading: React.PropTypes.bool,
    saveFailed: React.PropTypes.bool,
    validationFailed: React.PropTypes.bool,
  }),
  name: React.PropTypes.string,
  reduceWidth: React.PropTypes.bool,
  saveErrorClass: React.PropTypes.string,
  simulateCurrentRoute: React.PropTypes.string,
  noBack: React.PropTypes.bool,
  nextButtonName: React.PropTypes.string,
  noBorder: React.PropTypes.bool,
  fullWidth: React.PropTypes.bool,
  disableNext: React.PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  loanStep: makeSelectLoanStep(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationHelper(LoanStep));
