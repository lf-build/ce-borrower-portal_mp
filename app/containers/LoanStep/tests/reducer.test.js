
import { fromJS } from 'immutable';
import loanStepReducer from '../reducer';

describe('loanStepReducer', () => {
  it('returns the initial state', () => {
    expect(loanStepReducer(undefined, {})).toEqual(fromJS({}));
  });
});
