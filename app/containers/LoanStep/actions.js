/*
 *
 * LoanStep actions
 *
 */

import {
  CLEAR_SAVE_FAILED,
  LOAN_STEP_LOADED,
} from './constants';

export function clearError() {
  return {
    type: CLEAR_SAVE_FAILED,
  };
}

export function stepLoaded(name) {
  return {
    type: LOAN_STEP_LOADED,
    meta: {
      name,
    },
  };
}
