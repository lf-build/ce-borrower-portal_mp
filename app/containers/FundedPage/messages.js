/*
 * FundedPage Messages
 *
 * This contains all the text for the FundedPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.FundedPage.header',
    defaultMessage: 'Congratulations!',
  },
  loadingHeader: {
    id: 'app.containers.FundedPage.loadingHeader',
    defaultMessage: 'Checking if application is funded',
  },
});
