/*
 *
 * FundedPage constants
 *
 */

export const DEFAULT_ACTION = 'app/FundedPage/DEFAULT_ACTION';
export const APP_FUNDED_REQUEST = 'app/FundedPage/APP_FUNDED_REQUEST';
export const APP_FUNDED_REQUEST_STARTED = 'app/FundedPage/APP_FUNDED_REQUEST_STARTED';
export const APP_FUNDED_REQUEST_FULFILLED = 'app/FundedPage/APP_FUNDED_REQUEST_FULFILLED';
export const APP_FUNDED_REQUEST_FAILED = 'app/FundedPage/APP_FUNDED_REQUEST_FAILED';
export const APP_FUNDED_REQUEST_ENDED = 'app/FundedPage/APP_FUNDED_REQUEST_ENDED';
