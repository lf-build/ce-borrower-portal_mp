/*
 *
 * FundedPage actions
 *
 */

import {
  DEFAULT_ACTION,
  APP_FUNDED_REQUEST,
  APP_FUNDED_REQUEST_STARTED,
  APP_FUNDED_REQUEST_FULFILLED,
  APP_FUNDED_REQUEST_FAILED,
  APP_FUNDED_REQUEST_ENDED,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function appFundedRequest() {
  return {
    type: APP_FUNDED_REQUEST,
  };
}

export function appFundedRequestStarted() {
  return {
    type: APP_FUNDED_REQUEST_STARTED,
  };
}

export function appFundedRequestFulfilled(data) {
  return {
    type: APP_FUNDED_REQUEST_FULFILLED,
    meta: {
      data,
    },
  };
}

export function appFundedRequestFailed(error) {
  return {
    type: APP_FUNDED_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function appFundedRequestEnded() {
  return {
    type: APP_FUNDED_REQUEST_ENDED,
  };
}
