import { takeEvery, put, select } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import messages from './messages';
import { ERROR_OCCURRED } from './constants';

function* handleIfscAddressFetch(action) {
  if (action.meta.form === 'banking' && action.meta.field === 'ifscCode') {
    if (!(yield uplink.requestUplinkExecution({
      dock: 'on-boarding',
      section: 'lookup',
      command: 'bank-details',
    }, {
      tag: action.payload,
      payload: {
        ifscCode: action.payload,
      },
    }))) {
      return;
    }

    const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(action.payload);
    if (valid) {
      yield put({
        type: '@@redux-form/CHANGE',
        meta: {
          form: 'banking',
          field: 'bankAddress',
          touch: false,
          persistentSubmitErrors: false,
        },
        payload: valid.meta.body.address,
      });
    } else {
      yield put({
        type: '@@redux-form/CHANGE',
        meta: {
          form: 'banking',
          field: 'bankAddress',
          touch: false,
          persistentSubmitErrors: false,
        },
        payload: '',
      });
    }
  }
}

function* handleBankNameChange(action) {
  // query the state using the exported selector
  const bankName = yield select();
  const bankState = bankName.toJS().form.banking;

  // when page is loaded for the first time and user starts entering a bank name, so here to avoid undefined error putting this logic.
  if (!bankState || !bankState.values || !bankState.values.bank) {
    return;
  }

  // const { values: { bank }, fields: { bank: bankField }, anyTouched } = bankName.toJS().form.banking;
  const { values: { bank }, anyTouched } = bankState;

  if (!bank && anyTouched) {
    // console.log('required error');
    yield put({
      type: '@@redux-form/UPDATE_SYNC_ERRORS',
      meta: {
        form: 'banking',
      },
      payload: {
        syncErrors: {
          bank: 'Bank Name cannot be left blank',
        },
      },
    });
    // raise required error
    return;
  }

  if (typeof bank !== 'object' && anyTouched) {
    // console.log('select from dropdown list error');
    yield put({
      type: '@@redux-form/UPDATE_SYNC_ERRORS',
      meta: {
        form: 'banking',
      },
      payload: {
        syncErrors: {
          bank: 'Select Bank Name from dropdown',
        },
      },
    });
    // raise select from dropdown list error
    return;
  }

  if (bank.name !== action.payload.text && anyTouched) {
    // console.log('select from dropdown list error', bank.name, action.payload.text);
    yield put({
      type: '@@redux-form/UPDATE_SYNC_ERRORS',
      meta: {
        form: 'banking',
      },
      payload: {
        syncErrors: {
          bank: 'Select Bank Name from dropdown',
        },
      },
    });
    // // raise select from dropdown list error
    return;
  }
  // console.log('All is well. in hanlde bank name chage ', bank, bankField);
  yield put({
    type: '@@redux-form/UPDATE_SYNC_ERRORS',
    meta: {
      form: 'banking',
    },
    payload: {
      syncErrors: { },
    },
  });
}

function* handleErrors(action) {
  const { meta: { error: { body: { status } } } } = action;

  if (status) {
    const errMsg = status === 'apiError' ? messages.apiErrorHeader.defaultMessage : messages.statementErrorHeader.defaultMessage;

    yield put({
      type: ERROR_OCCURRED,
      meta: {
        form: 'banking',
      },
      payload: {
        errorMsg: errMsg,
      },
    });
  }
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery('@@redux-form/BLUR', handleIfscAddressFetch);
  yield takeEvery('app/BankingPage/BANK_NAME_CHANGE', handleBankNameChange);
  yield takeEvery('app/uplink/EXECUTE_UPLINK_REQUEST_FAILED_banking', handleErrors);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
