import { createSelector } from 'reselect';

/**
 * Direct selector to the bankingPage state domain
 */
const selectBankingPageDomain = () => (state) => state.get('bankingPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by BankingPage
 */

const makeSelectBankingPage = () => createSelector(
  selectBankingPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectBankingPage;
export {
  selectBankingPageDomain,
};
