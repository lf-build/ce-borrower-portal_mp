/*
 *
 * BankingPage actions
 *
 */

import {
  STATEMENT_RECIEVED,
  DEFAULT_ACTION,
  STATEMENT_REMOVED,
  BANK_NAME_CHANGE,
  SET_NET_LINK_TRANSACTION,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function statementRecieved({ file, dataUrl, type }) {
  return {
    type: STATEMENT_RECIEVED,
    payload: {
      file,
      dataUrl,
      type,
    },
  };
}

export function statementRemoved({ index }) {
  return {
    type: STATEMENT_REMOVED,
    payload: {
      index,
    },
  };
}

export function bankNameChange(text) {
  return {
    type: BANK_NAME_CHANGE,
    payload: {
      text,
    },
  };
}

export function setNetlinkTransaction(status) {
  return {
    type: SET_NET_LINK_TRANSACTION,
    payload: {
      status,
    },
  };
}
