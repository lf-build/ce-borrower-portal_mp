/*
 *
 * ReviewApplicationPage actions
 *
 */

import {
  SUBMIT_APPLICATION_REQUEST,
  SUBMIT_APPLICATION_REQUEST_STARTED,
  SUBMIT_APPLICATION_REQUEST_FULFILLED,
  SUBMIT_APPLICATION_REQUEST_REJECTED,
  SUBMIT_APPLICATION_REQUEST_FAILED,
  SUBMIT_APPLICATION_REQUEST_ENDED,
  SUBMIT_APPLICATION_REQUEST_DENIED,
  DETECT_IP_ADDRESS,
  VALIDATE_BEFORE_SUBMIT,
  SHOW_VALIDATION_ERRORS,
  GET_APPLICATION_NUMBER_REQUEST,
  GET_APPLICATION_NUMBER_REQUEST_STARTED,
  GET_APPLICATION_NUMBER_REQUEST_FAILED,
  GET_APPLICATION_NUMBER_REQUEST_FULFILLED,
  GET_APPLICATION_NUMBER_REQUEST_ENDED,
} from './constants';

export function submitApplication(ip, accepted) {
  return {
    type: SUBMIT_APPLICATION_REQUEST,
    meta: {
      ip,
      accepted,
    },
  };
}

export function submitApplicationStarted() {
  return {
    type: SUBMIT_APPLICATION_REQUEST_STARTED,
  };
}

export function submitApplicationFulfilled(meta) {
  return {
    type: SUBMIT_APPLICATION_REQUEST_FULFILLED,
    meta,
  };
}

export function submitApplicationRejected(meta) {
  return {
    type: SUBMIT_APPLICATION_REQUEST_REJECTED,
    meta,
  };
}

export function submitApplicationDenied(meta) {
  return {
    type: SUBMIT_APPLICATION_REQUEST_DENIED,
    meta,
  };
}

export function submitApplicationFailed(meta) {
  return {
    type: SUBMIT_APPLICATION_REQUEST_FAILED,
    meta,
  };
}

export function submitApplicationEnded() {
  return {
    type: SUBMIT_APPLICATION_REQUEST_ENDED,
  };
}

export function detectIP() {
  return {
    type: DETECT_IP_ADDRESS,
  };
}

export function validateBeforeSubmit(errors) {
  return {
    type: VALIDATE_BEFORE_SUBMIT,
    meta: {
      errors,
    },
  };
}

export function showValidationErrors(show) {
  return {
    type: SHOW_VALIDATION_ERRORS,
    meta: {
      show,
    },
  };
}

export function getApplicationNumberRequest() {
  return {
    type: GET_APPLICATION_NUMBER_REQUEST,
  };
}

export function getApplicationNumberRequestStarted() {
  return {
    type: GET_APPLICATION_NUMBER_REQUEST_STARTED,
  };
}

export function getApplicationNumberRequestFulfilled(applicationNumber) {
  return {
    type: GET_APPLICATION_NUMBER_REQUEST_FULFILLED,
    meta: {
      applicationNumber,
    },
  };
}

export function getApplicationNumberRequestFailed(error) {
  return {
    type: GET_APPLICATION_NUMBER_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function getApplicationNumberRequestEnded() {
  return {
    type: GET_APPLICATION_NUMBER_REQUEST_ENDED,
  };
}
