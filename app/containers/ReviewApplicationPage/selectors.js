import { createSelector } from 'reselect';
import { getFormValues } from 'redux-form/immutable';

/**
 * Direct selector to the reviewApplicationPage state domain
 */
const selectReviewApplicationPageDomain = () => (state) => state.get('reviewApplicationPage');

/**
 * Other specific selectors
 */
const selectReviewApplicationForm = (state) => ({
  amountForm: getFormValues('amount')(state) && getFormValues('amount')(state).toJS(),
  reasonForm: getFormValues('reason')(state) && getFormValues('reason')(state).toJS(),
  reasonPage: state.get('reasonPage') && state.get('reasonPage').toJS(),
  // creditCardExpenseDetailsForm: getFormValues('credit-card-expense-details')(state) && getFormValues('credit-card-expense-details')(state).toJS(),
  residenceExpenseDetailsForm: getFormValues('residence-expense-details')(state) && getFormValues('residence-expense-details')(state).toJS(),
  workDetailsForm: getFormValues('work-details')(state) && getFormValues('work-details')(state).toJS(),
  otpForm: getFormValues('otp')(state) && getFormValues('otp')(state).toJS(),
  basicInformationForm: getFormValues('basic-information')(state) && getFormValues('basic-information')(state).toJS(),
  // otherExpenseDetailsForm: getFormValues('other-expense-details')(state) && getFormValues('other-expense-details')(state).toJS(),
  personalDetailsForm: getFormValues('personal-details')(state) && getFormValues('personal-details')(state).toJS(),
  mobileForm: getFormValues('mobile')(state) && getFormValues('mobile')(state).toJS(),
  createAccountForm: getFormValues('create-account')(state) && getFormValues('create-account')(state).toJS(),
  fakevale: 'ReallyFack',
});


/**
 * Default selector used by ReviewApplicationPage
 */

const makeSelectReviewApplicationPage = () => createSelector(
  selectReviewApplicationPageDomain(),
  (substate) => substate && substate.toJS()
);

export default makeSelectReviewApplicationPage;
export {
  selectReviewApplicationPageDomain,
  selectReviewApplicationForm,
};
