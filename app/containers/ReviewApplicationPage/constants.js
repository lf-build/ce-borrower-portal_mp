/*
 *
 * ReviewApplicationPage constants
 *
 */

export const SUBMIT_APPLICATION_REQUEST = 'app/ReviewApplicationPage/SUBMIT_APPLICATION_REQUEST';
export const SUBMIT_APPLICATION_REQUEST_DENIED = 'app/ReviewApplicationPage/SUBMIT_APPLICATION_REQUEST_DENIED';
export const SUBMIT_APPLICATION_REQUEST_STARTED = 'app/ReviewApplicationPage/SUBMIT_APPLICATION_REQUEST_STARTED';
export const SUBMIT_APPLICATION_REQUEST_FULFILLED = 'app/ReviewApplicationPage/SUBMIT_APPLICATION_REQUEST_FULFILLED';
export const SUBMIT_APPLICATION_REQUEST_REJECTED = 'app/ReviewApplicationPage/SUBMIT_APPLICATION_REQUEST_REJECTED';
export const SUBMIT_APPLICATION_REQUEST_FAILED = 'app/ReviewApplicationPage/SUBMIT_APPLICATION_REQUEST_FAILED';
export const SUBMIT_APPLICATION_REQUEST_ENDED = 'app/ReviewApplicationPage/SUBMIT_APPLICATION_REQUEST_ENDED';
export const DETECT_IP_ADDRESS = 'app/ReviewApplicationPage/DETECT_IP_ADDRESS';
export const IP_DETECTED_SUCCESS = 'app/ReviewApplicationPage/IP_DETECTED_SUCCESS';
export const IP_DETECTED_FAILED = 'app/ReviewApplicationPage/IP_DETECTED_FAILED';
export const VALIDATE_BEFORE_SUBMIT = 'app/ReviewApplicationPage/VALIDATE_BEFORE_SUBMIT';
export const SHOW_VALIDATION_ERRORS = 'app/ReviewApplicationPage/SHOW_VALIDATION_ERRORS';
export const GET_APPLICATION_NUMBER_REQUEST = 'app/InitialOfferPage/GET_APPLICATION_NUMBER_REQUEST';
export const GET_APPLICATION_NUMBER_REQUEST_STARTED = 'app/InitialOfferPage/GET_APPLICATION_NUMBER_REQUEST_STARTED';
export const GET_APPLICATION_NUMBER_REQUEST_FULFILLED = 'app/InitialOfferPage/GET_APPLICATION_NUMBER_REQUEST_FULFILLED';
export const GET_APPLICATION_NUMBER_REQUEST_FAILED = 'app/InitialOfferPage/GET_APPLICATION_NUMBER_REQUEST_FAILED';
export const GET_APPLICATION_NUMBER_REQUEST_ENDED = 'app/InitialOfferPage/GET_APPLICATION_NUMBER_REQUEST_ENDED';

