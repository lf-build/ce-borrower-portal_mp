
import {
  submitApplication,
} from '../actions';
import {
  SUBMIT_APPLICATION_REQUEST,
} from '../constants';

describe('ReviewApplicationPage actions', () => {
  describe('Default Action', () => {
    it('has a type of DEFAULT_ACTION', () => {
      const expected = {
        type: SUBMIT_APPLICATION_REQUEST,
        meta: {
          ip: '127.0.0.1',
        },
      };
      expect(submitApplication('127.0.0.1')).toEqual(expected);
    });
  });
});
