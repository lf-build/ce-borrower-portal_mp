/*
 *
 * ReviewApplicationPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { browserHistory } from 'react-router';

// import { FormattedMessage, injectIntl, intlShape } from 'react-intl';
// import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import makeSelectReviewApplicationPage, { selectReviewApplicationForm } from './selectors';
import messages from './messages';
import LoanStep from '../LoanStep';
import { InlineEditIcon } from '../../components/Icons';
import {
  submitApplication as submitApplicationAction,
  detectIP as detectIPAction,
  validateBeforeSubmit as validateBeforeSubmitAction,
} from './actions';
import ReviewApplicationNavigationPanel from '../../components/ReviewApplicationNavigationPanel';
const Block = styled.div`
 
`;
const BlockLabel = styled.label`
    font-size: 10px;
    text-transform: uppercase;
    font-weight: 300;
    color: #9ca3a9;
    line-height: 16px;
    letter-spacing: 1px;
`;
const BlockContent = styled.div`
  color: #4e4e4e;
  font-size: 12px;
`;

const Header = styled.div`
    background: #e9f7f8 none repeat scroll 0 0;
    border-radius: 10px 10px 0 0;
    color: #64acaf;
    padding: 5px 20px;
    height: 35px;
    font-size: 16px;
    font-weight: 300;
    letter-spacing: 0.025em;
    line-height: 1.7em;
`;
const Padder = styled.div`
     margin: 10px;
`;

export class ReviewApplicationPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    // Required for consents
    this.props.detectIP();

    const errors = this.performValidation();
    this.props.validateBeforeSubmit(errors);
  }

  performValidation() {
    const { form: { basicInformationForm, personalDetailsForm } } = this.props;

    let errors;
    let basicInfo;
    let addressInfo;
    let currAdd;
    let perAdd;

    const len = 3;

    // First Name, Middle Name and Last Name validation.
    if (basicInformationForm) {
      const { firstName, middleName, lastName } = basicInformationForm;

      if (middleName && middleName.toLowerCase() === firstName.toLowerCase()) {
        basicInfo = Object.assign({}, basicInfo, {
          middleName: 'First Name and Middle Name (Optional) cannot be same.',
        });
      }

      if (lastName.toLowerCase() === firstName.toLowerCase()) {
        basicInfo = Object.assign({}, basicInfo, {
          lastName: 'First Name and Last Name cannot be same.',
        });
      }

      if (middleName && lastName.toLowerCase() === middleName.toLowerCase()) {
        basicInfo = Object.assign({}, basicInfo, {
          lastName: 'Last Name and Middle Name (Optional) cannot be same.',
        });
      }

      if (basicInfo) {
        errors = Object.assign({}, errors, {
          basicInformation: basicInfo,
        });
      }
    }

    if (personalDetailsForm) {
      const {
        addressLine1,
        addressLine2,
        locality,
      } = personalDetailsForm.currentResidentialAddress;

      if (addressLine1.length < len) {
        currAdd = Object.assign({}, currAdd, {
          addressLine1: `Address Line 1 must have at least ${len} characters`,
        });
      }

      if (addressLine2 && addressLine2.length < len) {
        currAdd = Object.assign({}, currAdd, {
          addressLine2: `Address Line 2 (Optional) must have at least ${len} characters`,
        });
      }

      if (locality.length < len) {
        currAdd = Object.assign({}, currAdd, {
          locality: `Locality must have at least ${len} characters`,
        });
      }

      if (currAdd) {
        addressInfo = Object.assign({}, addressInfo, { currentResidentialAddress: currAdd });
        errors = Object.assign({}, errors, {
          details: addressInfo,
        });
      }

      // permanentResidentialAddress
      if (personalDetailsForm.permanentResidentialAddressSameAsCurrent &&
        (personalDetailsForm.permanentResidentialAddressSameAsCurrent === 'false' || personalDetailsForm.permanentResidentialAddressSameAsCurrent === false)) {
        const {
          addressLine1: addLine1,
          addressLine2: addLine2,
          locality: loc,
        } = personalDetailsForm.permanentResidentialAddress;

        if (addLine1.length < len) {
          perAdd = Object.assign({}, perAdd, {
            addressLine1: `Address Line 1 must have at least ${len} characters`,
          });
        }

        if (addLine2 && addLine2.length < len) {
          perAdd = Object.assign({}, perAdd, {
            addressLine2: `Address Line 2 (Optional) must have at least ${len} characters`,
          });
        }

        if (loc.length < len) {
          perAdd = Object.assign({}, perAdd, {
            locality: `Locality must have at least ${len} characters`,
          });
        }

        if (perAdd) {
          addressInfo = Object.assign({}, addressInfo, { permanentResidentialAddress: perAdd });
          errors = Object.assign({}, errors, {
            details: addressInfo,
          });
        }
      }
    }

    return errors;
  }

  render() {
    const blockResponsive2Items = 'col-lg-6 col-md-6 col-sm-6 col-xs-6';
    const blockResponsive3Items = 'col-lg-4 col-md-6 col-sm-6 col-xs-6';
    const blockResponsive4Items = 'col-lg-3 col-md-6 col-sm-6 col-xs-6';
    const blockFullWidth = 'col-xs-12';
    const formatNumber = (text) => formatAmount(text);
    // const formatNumber = (text) => this.props.intl.formatNumber(text, {
    //   style: 'currency',
    //   currency: 'INR',
    //   minimumFractionDigits: 0,
    // });
    const formatAmount = (amtToFormat) => {
      const amt = amtToFormat.toString();
      if (amt) {
        const amtArray = amt.replace('₹', '').replace(',', '').replace(' ', '').split('');
        if (amtArray.length === 1) {
          return `₹ ${amtArray[0]}`;
        }

        if (amtArray.length === 2) {
          return `₹ ${amtArray[0]}${amtArray[1]}`;
        }

        if (amtArray.length === 3) {
          return `₹ ${amtArray[0]}${amtArray[1]}${amtArray[2]}`;
        }

        if (amtArray.length === 4) {
          return `₹ ${amtArray[0]},${amtArray[1]}${amtArray[2]}${amtArray[3]}`;
        }

        if (amtArray.length === 5) {
          return `₹ ${amtArray[0]}${amtArray[1]},${amtArray[2]}${amtArray[3]}${amtArray[4]}`;
        }

        if (amtArray.length === 6) {
          return `₹ ${amtArray[0]},${amtArray[1]}${amtArray[2]},${amtArray[3]}${amtArray[4]}${amtArray[5]}`;
        }
      }
      return amt;
    };
    const capitalizeFirstLetter = (value) => value.charAt(0).toUpperCase() + value.slice(1);
    const formatAddress = (address) => {
      if (!address) {
        return '';
      }
      const formatField = (value) => value ? `, ${value}` : '';
      const {
        addressLine1,
        addressLine2,
        locality,
        pinCode,
        city,
        state,
      } = address;

      return `${addressLine1}${formatField(addressLine2)}${formatField(locality)}${formatField(pinCode)}${formatField(capitalizeFirstLetter(city))}${formatField(capitalizeFirstLetter(state))}`;
    };
    const formatTitle = (title) => {
      if (!title) {
        return '';
      }
      const [firstLetter, ...rest] = title;
      return `${firstLetter.toUpperCase()}${rest.join('')}.`;
    };
    const formatReason = (reason, reasonText) => {
      switch (reason) {
        case 'travel':
          return 'Travel';
        case 'vehiclepurchase':
          return 'Vehicle Purchase';
        case 'medical':
          return 'Medical';
        case 'loanrefinancing':
          return 'Transfer Existing Loan';
        case 'wedding':
          return 'Wedding';
        case 'homeimprovement':
          return 'Home Improvement';
        case 'business':
          return 'Business';
        case 'education':
          return 'Education';
        case 'assetacquisition':
          return 'Asset Acquisition';
        case 'agriculture':
          return 'Agriculture';
        case 'other':
          return `${reasonText} (Other)`;
        default:
          return reason;
      }
    };

    const formatResidenceType = (residenceType) => {
      switch (residenceType) {
        case 'OwnedSelf':
          return 'Self Owned';
        case 'Owned-Family':
          return 'Owned With Family';
        case 'Rented-Family':
          return 'Rented With Family';
        case 'Rented-Alone':
          return 'Rented Bachelor Staying Alone';
        case 'Rented-Friends':
          return 'Rented With Friends';
        case 'PGHostel':
          return 'PG or Hostel';
        case 'Company-Acco':
          return 'Company Provided Accommodation';
        default:
          return `${residenceType} (Other)`;
      }
    };

    const formatAccommodationHeader = (residenceType) => {
      if (residenceType === 'OwnedSelf' || residenceType === 'Owned-Family') {
        return 'HOME LOAN';
      }
      return 'RENT';
    };

    const formatAccommodationAmount = ({ residenceType, hasHomeLoan, monthlyRent, homeLoanEmi }) => {
      if (residenceType === 'OwnedSelf' || residenceType === 'Owned-Family') {
        return hasHomeLoan === 'true' ? formatNumber(homeLoanEmi) : 'No';
      }
      if (residenceType === 'Company-Acco') {
        return 'N/A';
      }
      return formatNumber(monthlyRent);
    };
    const { form: {
      amountForm,
      reasonForm,
      reasonPage,
      // creditCardExpenseDetailsForm,
      residenceExpenseDetailsForm,
      workDetailsForm,
      basicInformationForm,
      // otherExpenseDetailsForm,
      personalDetailsForm,
      mobileForm,
    },
      submitApplication,
      ReviewApplicationPage: {
        ip,
        show,
      },
      // ReviewApplicationPage: {
      //   // loading,
      //   // status,
      //   // offer,
      // },
    } = this.props;

    if (!(amountForm
      && (reasonForm || reasonPage)
      // && creditCardExpenseDetailsForm
      && residenceExpenseDetailsForm
      && workDetailsForm
      && basicInformationForm
      // && otherExpenseDetailsForm
      && personalDetailsForm
      && mobileForm
    )) {
      return (
        <LoanStep name={'review-application'} title={messages.header} noPadding navigationPanel={(props) => <ReviewApplicationNavigationPanel {...props} {...{ submitApplication }} />}>
          <div> All values not filled. </div>
        </LoanStep>
      );
    }

    const getDob = () => {
      const { date, month, year } = personalDetailsForm;
      return `${date}/${month}/${year}`;
    };

    const errors = this.performValidation();

    return (
      <LoanStep
        name={'review-application'}
        title={messages.header}
        noPadding
        ShortDescription={() => <p style={{ textAlign: 'center' }}> Please click on the pencil icon <InlineEditIcon style={{ cursor: 'default' }} /> to edit information </p>}
        navigationPanel={(props) => <ReviewApplicationNavigationPanel {...props} {...{ submitApplication: (accepted) => submitApplication(ip, accepted), show, errors }} />}
      >
        <Header> Loan Details </Header>
        <Padder />
        <div className={'row'} style={{ paddingLeft: 20 }}>
          <Block className={blockResponsive2Items} >
            <BlockLabel> Loan Amount <InlineEditIcon onClick={() => browserHistory.replace('/application/amount')} /></BlockLabel>
            <BlockContent> {formatAmount(amountForm.amountField)} </BlockContent>
          </Block>
          <Block className={blockResponsive2Items} >
            <BlockLabel> LOAN PURPOSE <InlineEditIcon onClick={() => browserHistory.replace('/application/reason')} /></BlockLabel>
            <BlockContent> {formatReason(reasonPage.selected, reasonForm && reasonForm.reasonText)} </BlockContent>
          </Block>
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
        </div>
        <Padder />
        <Header> Personal Details </Header>
        <Padder />
        <div className={'row'} style={{ paddingLeft: 20 }}>
          <Block className={blockResponsive3Items} >
            <BlockLabel> NAME <InlineEditIcon onClick={() => browserHistory.replace('/application/basic-information')} /> </BlockLabel>
            <BlockContent style={{ textTransform: 'capitalize' }}> {`${formatTitle(basicInformationForm.title)} ${basicInformationForm.firstName} ${basicInformationForm.lastName} `} </BlockContent>
          </Block>
          <Block className={blockResponsive3Items} >
            <BlockLabel> MOBILE NUMBER </BlockLabel>
            <BlockContent> {mobileForm.mobileNumber} </BlockContent>
          </Block>
          <Block className={blockResponsive3Items} >
            <BlockLabel> DATE OF BIRTH <InlineEditIcon onClick={() => browserHistory.replace('/application/personal-details')} /></BlockLabel>
            <BlockContent> {getDob()} </BlockContent>
          </Block>
          <Block className={'col-lg-12'} />
          <Block className={blockResponsive3Items} >
            <BlockLabel> PAN NUMBER <InlineEditIcon onClick={() => browserHistory.replace('/application/personal-details')} /></BlockLabel>
            <BlockContent style={{ textTransform: 'uppercase' }}> {personalDetailsForm.panNumber} </BlockContent>
          </Block>
          <Block className={blockResponsive2Items} >
            <BlockLabel> PERSONAL EMAIL ID <InlineEditIcon onClick={() => browserHistory.replace('/application/basic-information')} /> </BlockLabel>
            <BlockContent style={{ wordWrap: 'break-word' }}> {basicInformationForm.personalEmail} </BlockContent>
          </Block>
          <Block className={`${personalDetailsForm.aadhaarNumber ? blockResponsive3Items : 'hidden'}`} >
            <BlockLabel> AADHAAR NUMBER <InlineEditIcon onClick={() => browserHistory.replace('/application/personal-details')} /></BlockLabel>
            <BlockContent> {personalDetailsForm.aadhaarNumber} </BlockContent>
          </Block>
          <Block className={blockResponsive3Items} >
            <BlockLabel> MARITAL STATUS <InlineEditIcon onClick={() => browserHistory.replace('/application/basic-information')} /> </BlockLabel>
            <BlockContent style={{ textTransform: 'capitalize' }}> {basicInformationForm.maritalStatus} </BlockContent>
          </Block>
        </div>
        <div className={'row'} style={{ paddingLeft: 20 }}>
          <Block className={blockFullWidth} >
            <BlockLabel> current residential ADDRESS <InlineEditIcon onClick={() => browserHistory.replace('/application/personal-details')} /></BlockLabel>
            <BlockContent> {formatAddress(personalDetailsForm.currentResidentialAddress)} </BlockContent>
          </Block>
          <Block className={(personalDetailsForm.currentAddressYear) ? blockFullWidth : 'hidden'} >
            <BlockLabel> years lived in current residential ADDRESS <InlineEditIcon onClick={() => browserHistory.replace('/application/personal-details')} /></BlockLabel>
            <BlockContent> {`${personalDetailsForm.currentAddressYear} Year `}</BlockContent>
          </Block>
          <Block className={personalDetailsForm.permanentResidentialAddressSameAsCurrent === 'true' ? 'hidden' : blockFullWidth} >
            <BlockLabel> Permanent Residential ADDRESS </BlockLabel>
            <BlockContent >{personalDetailsForm.permanentResidentialAddressSameAsCurrent === 'false' ? formatAddress(personalDetailsForm.permanentResidentialAddress) : 'Same as current residential address'} </BlockContent>
          </Block>
          <Block className={blockResponsive4Items} />
        </div>
        <Padder />
        <Header> Employment Details </Header>
        <Padder />
        <div className={'row'} style={{ paddingLeft: 20 }}>
          <Block className={blockResponsive3Items} >
            <BlockLabel> COMPANY <InlineEditIcon onClick={() => browserHistory.replace('/application/work-details')} /></BlockLabel>
            <BlockContent> {typeof workDetailsForm.employer === 'string' ? workDetailsForm.employer : workDetailsForm.employer.name} </BlockContent>
          </Block>
          <Block className={blockResponsive3Items} >
            <BlockLabel> OFFICIAL EMAIL ID <InlineEditIcon onClick={() => browserHistory.replace('/application/work-details')} /></BlockLabel>
            <BlockContent style={{ wordWrap: 'break-word', paddingRight: 5 }}> {workDetailsForm.officialEmail} </BlockContent>
          </Block>
          <Block className={blockResponsive3Items} >
            <BlockLabel> SALARY <InlineEditIcon onClick={() => browserHistory.replace('/application/work-details')} /></BlockLabel>
            <BlockContent> {formatNumber(workDetailsForm.monthlyTakeHomeSalary)} </BlockContent>
          </Block>
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
        </div>
        <Padder />
        <Header> Expenses </Header>
        <Padder />
        <div className={'row'} style={{ paddingLeft: 20 }}>
          <Block className={blockResponsive3Items} >
            <BlockLabel> RESIDENCE TYPE <InlineEditIcon onClick={() => browserHistory.replace('/application/residence-expense-details')} /></BlockLabel>
            <BlockContent> {formatResidenceType(residenceExpenseDetailsForm.residenceType)} </BlockContent>
          </Block>
          {/* <Block className={blockResponsive3Items} >
            <BlockLabel style={{ textTransform: 'none' }}> OTHER EMIs <InlineEditIcon onClick={() => browserHistory.replace('/application/other-expense-details')} /></BlockLabel>
            <BlockContent> {formatNumber((otherExpenseDetailsForm || { otherEmis: 0 }).otherEmis) } </BlockContent>
          </Block> */}
          <Block className={blockResponsive3Items} >
            <BlockLabel> {formatAccommodationHeader(residenceExpenseDetailsForm.residenceType)} <InlineEditIcon onClick={() => browserHistory.replace('/application/residence-expense-details')} /></BlockLabel>
            <BlockContent> {formatAccommodationAmount(residenceExpenseDetailsForm)} </BlockContent>
            {/* <BlockContent> {residenceExpenseDetailsForm.hasHomeLoan === 'true' ? formatNumber(residenceExpenseDetailsForm.homeLoanEmi) : 'No'} </BlockContent> */}
          </Block>
          {/* <Block className={blockResponsive3Items} >
            <BlockLabel> MONTHLY EXPENSES </BlockLabel>
            <BlockContent> ₹ 8,000 </BlockContent>
          </Block>*/}
          {/* <Block className={blockResponsive2Items} >
            <BlockLabel> CREDIT CARD <InlineEditIcon onClick={() => browserHistory.replace('/application/credit-card-expense-details')} /></BlockLabel>
            <BlockContent> {creditCardExpenseDetailsForm.hasCreditCard === 'true' ? formatNumber(creditCardExpenseDetailsForm.creditCardOutstanding) : 'No'} </BlockContent>
          </Block> */}
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
          <Block className={blockResponsive2Items} />
        </div>
        <Padder />
      </LoanStep >
    );
  }
}

ReviewApplicationPage.propTypes = {
  submitApplication: PropTypes.func.isRequired,
  detectIP: PropTypes.func.isRequired,
  form: PropTypes.object,
  // intl: intlShape.isRequired,
  ReviewApplicationPage: PropTypes.object,
  validateBeforeSubmit: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ReviewApplicationPage: makeSelectReviewApplicationPage(),
  form: selectReviewApplicationForm,
});

function mapDispatchToProps(dispatch) {
  return {
    submitApplication: (ip, accepted) => dispatch(submitApplicationAction(ip, accepted)),
    detectIP: () => dispatch(detectIPAction()),
    validateBeforeSubmit: (errors) => dispatch(validateBeforeSubmitAction(errors)),
  };
}

// export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(ReviewApplicationPage));
export default connect(mapStateToProps, mapDispatchToProps)(ReviewApplicationPage);
