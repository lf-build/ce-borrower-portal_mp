/*
 *
 * ReviewApplicationPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  // SUBMIT_APPLICATION_REQUEST_STARTED,
  // SUBMIT_APPLICATION_REQUEST_ENDED,
  // SUBMIT_APPLICATION_REQUEST_FULFILLED,
  IP_DETECTED_SUCCESS,
  VALIDATE_BEFORE_SUBMIT,
  SHOW_VALIDATION_ERRORS,
  GET_APPLICATION_NUMBER_REQUEST_FULFILLED,
} from './constants';

const initialState = fromJS({});

function reviewApplicationPageReducer(state = initialState, action) {
  switch (action.type) {
    case IP_DETECTED_SUCCESS:
      return state.set('ip', action.ip);
    case VALIDATE_BEFORE_SUBMIT:
      return state.set('validationErrors', action.meta.errors);
    case SHOW_VALIDATION_ERRORS:
      return state.set('show', action.meta.show);
    case GET_APPLICATION_NUMBER_REQUEST_FULFILLED:
      return state.set('applicationNumber', action.meta.applicationNumber);
    // case '@@router/LOCATION_CHANGE':
    //   return state.set('show', false);
    // case SUBMIT_APPLICATION_REQUEST_STARTED:
    //   return state
    //          .set('loading', true);
    // case SUBMIT_APPLICATION_REQUEST_ENDED:
    //   return state
    //          .set('loading', false);
    // case SUBMIT_APPLICATION_REQUEST_FULFILLED:
    //   return state
    //          .set('application', action.meta.body);
    default:
      return state;
  }
}

export default reviewApplicationPageReducer;
