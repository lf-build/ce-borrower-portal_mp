
import { fromJS } from 'immutable';
import basicInformationPageReducer from '../reducer';

describe('basicInformationPageReducer', () => {
  it('returns the initial state', () => {
    expect(basicInformationPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
