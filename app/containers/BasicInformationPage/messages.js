/*
 * BasicInformationPage Messages
 *
 * This contains all the text for the BasicInformationPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.BasicInformationPage.header',
    defaultMessage: 'Tell us a little about yourself please',
  },
});
