/*
 *
 * BasicInformationPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  TITLE_NOT_SELECTED,
  TITLE_SELECTED,
  MARITAL_STATUS_NOT_SELECTED,
  MARITAL_STATUS_SELECTED,
} from './constants';

const initialState = fromJS({});

function basicInformationPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case TITLE_NOT_SELECTED:
      return state.set('titleNotSelected', true);
    case TITLE_SELECTED:
      return state.set('titleNotSelected', false);
    case MARITAL_STATUS_NOT_SELECTED:
      return state.set('maritalStatusNotSelected', true);
    case MARITAL_STATUS_SELECTED:
      return state.set('maritalStatusNotSelected', false);
    default:
      return state;
  }
}

export default basicInformationPageReducer;
