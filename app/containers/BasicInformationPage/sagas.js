import { takeEvery, put, select } from 'redux-saga/effects';
import {
  TITLE_SELECTED,
  TITLE_NOT_SELECTED,
  MARITAL_STATUS_SELECTED,
  MARITAL_STATUS_NOT_SELECTED
} from './constants';

function* handleSaveFailed() {
  const appState = yield select();
  const { form: { 'basic-information': basicInfoForm } } = appState.toJS();
  const { syncErrors: { title, maritalStatus } } = basicInfoForm;

  if (title) {
    yield put({
      type: TITLE_NOT_SELECTED,
    });
  }

  if (maritalStatus) {
    yield put({
      type: MARITAL_STATUS_NOT_SELECTED,
    });
  }
}

function* handleSaveSucceed() {
  yield put({
    type: TITLE_SELECTED,
  });

  yield put({
    type: MARITAL_STATUS_SELECTED,
  });
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery('@@redux-form/SET_SUBMIT_FAILED_basic-information', handleSaveFailed);
  yield takeEvery('@@redux-form/SET_SUBMIT_SUCCEEDED_basic-information', handleSaveSucceed);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
