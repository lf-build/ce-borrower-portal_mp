/*
 *
 * BasicInformationPage constants
 *
 */

export const DEFAULT_ACTION = 'app/BasicInformationPage/DEFAULT_ACTION';
export const TITLE_NOT_SELECTED = 'app/BasicInformationPage/TITLE_NOT_SELECTED';
export const TITLE_SELECTED = 'app/BasicInformationPage/TITLE_SELECTED';
export const MARITAL_STATUS_NOT_SELECTED = 'app/BasicInformationPage/MARITAL_STATUS_NOT_SELECTED';
export const MARITAL_STATUS_SELECTED = 'app/BasicInformationPage/MARITAL_STATUS_SELECTED';

// import { take, call, put, select } from 'redux-saga/effects';

// Individual exports for testing
export function* defaultSaga() {
    // See example in containers/HomePage/sagas.js
  }
  
  // All sagas to be loaded
  export default [
    defaultSaga,
  ];
  