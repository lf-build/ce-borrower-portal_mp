/*
 *
 * BasicInformationPage
 *
 */
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { RadioButton } from 'material-ui/RadioButton';
import styled from 'styled-components';

import { Field, formValueSelector } from 'redux-form/immutable';
import RadioButtonGroup from 'redux-form-material-ui/lib/RadioButtonGroup';

import makeSelectBasicInformationPage from './selectors';
import TextField from '../../components/RequiredTextField';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import { inputMask } from '../../components/MaskedTextField/masks';
import makeAppSelect from '../App/selectors';

const BasicInformationPageFormSelector = formValueSelector('basic-information');
const errorDivStyle = {
  bottom: '15px',
  fontSize: '12px',
  lineHeight: '15px',
  color: 'rgb(255, 117, 106)',
  transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
  marginTop: '10px',
};
export class BasicInformationPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { title, maritalStatus, reviewPage, titleNotSelected, maritalStatusNotSelected } = this.props;

    const styles = {
      row: {
        marginBottom: '20px',
        color: '#4e4e4e',
      },
      row2: {
        marginTop: '10px',
        marginBottom: '12px',
        color: '#4e4e4e',
      },
      radioButton: {
        marginTop: '1px',
        display: 'block',
        height: 40,
        // borderRight: '1px solid #dadada',
        background: '#fff none repeat scroll 0 0',
        border: '1px solid #dadada',
      },
      radioButtonSelected: {
        marginTop: '1px',
        display: 'block',
        height: 40,
        background: 'rgba(121, 205, 213, 0.15) none repeat scroll 0 0',
        border: '1px solid #79cdd5',
        boxShadow: '0 0',
        color: '#79cdd5',
      },
      iconStyle: {
        marginRight: '2px',
        color: '#4e4e4e',
        display: 'none',
      },
      labelStyle: {
        color: '#303030',
        fontSize: '14px',
        lineHeight: 3,
        textAlign: 'center',
        width: 'calc(100% - 0px)',
      },
      labelStyleSelected: {
        color: '#79cdd5',
        fontSize: '14px',
        lineHeight: 3,
        textAlign: 'center',
        width: 'calc(100% - 0px)',
      },
      fieldStyle: {
        display: 'flex',
        textAlign: 'center',
      },
      borderRadiusLeft: {
        borderBottomLeftRadius: 4,
        borderTopLeftRadius: 4,
      },
      borderRadiusRight: {
        borderBottomRightRadius: 4,
        borderTopRightRadius: 4,
      },
      radioButtonLabel: {
        lineHeight: 'normal',
        transition: 'all 240ms ease 0s',
        zIndex: 10,
        opacity: 1,
        color: 'rgba(0, 0, 0, 0.298039)',
        fontSize: 10,
        fontWeight: 400,
        letterSpacing: '0.025em',
        marginBottom: 0,
        textTransform: 'capitalize',
      },
    };

    const TitleLabel = styled.label``;

    // const { handleSubmit, pristine, submitting } = this.props;
    const normalizeInputs = ({ firstName, middleName, lastName }) => ({
      firstName: firstName && firstName.trim(),
      middleName: middleName && middleName.trim(),
      lastName: lastName && lastName.trim(),
    });

    const nameValidator = (field) => {
      const execute = document.getElementById('firstName') && document.getElementById(field)
        && document.getElementById('firstName') !== '' && document.getElementById(field) !== '';

      if (execute) {
        if (field === 'middleName' && document.getElementById(field).value !== '') {
          if (document.getElementById('firstName').value.trim().toLowerCase() === document.getElementById(field).value.trim().toLowerCase()) {
            return 'First Name and Middle Name (Optional) cannot be same.';
          }

          return undefined;
        }

        if (field === 'lastName') {
          if (document.getElementById('firstName').value.trim().toLowerCase() === document.getElementById(field).value.trim().toLowerCase()) {
            return 'First Name and Last Name cannot be same.';
          }
          if (document.getElementById('middleName').value.trim().toLowerCase() === document.getElementById(field).value.trim().toLowerCase()) {
            return 'Last Name and Middle Name (Optional) cannot be same.';
          }

          return undefined;
        }
      }

      return undefined;
    };
    return (
      <LoanStep
        simulateCurrentRoute={reviewPage}
        noBack={reviewPage !== undefined}
        nextButtonName={reviewPage && 'Go to review'}
        name={'basic-information'}
        title={messages.header}
        payloadGenerator={normalizeInputs}
      >
        <div style={styles.row}>
          <span style={styles.radioButtonLabel}> Title </span>
          {/* <RadioButtonGroup name="title" style={{ display: 'flex' }}>*/}

          <Field
            name="title"
            style={styles.fieldStyle}
            validate={(value) => value ? undefined : 'Title is required'}
            component={RadioButtonGroup}
          >
            <RadioButton
              value="Mr"
              label="Mr."
              checkedIcon={<TitleLabel />}
              uncheckedIcon={<TitleLabel />}
              iconStyle={styles.iconStyle}
              style={title === 'Mr' ? { ...styles.radioButtonSelected, ...styles.borderRadiusLeft } : { ...styles.radioButton, ...styles.borderRadiusLeft, borderRight: `${title === 'Ms' ? 'none' : '1px solid #dadada'}` }}
              labelStyle={title === 'Mr' ? styles.labelStyleSelected : styles.labelStyle}
            />
            <RadioButton
              value="Ms"
              label="Ms."
              checkedIcon={<TitleLabel />}
              uncheckedIcon={<TitleLabel />}
              iconStyle={styles.iconStyle}
              labelStyle={title === 'Ms' ? styles.labelStyleSelected : styles.labelStyle}
              style={title === 'Ms' ? { ...styles.radioButtonSelected, borderRight: '1px solid #79cdd5', borderLeft: '1px solid #79cdd5' } : { ...styles.radioButton, borderRight: 'none', borderLeft: 'none' }}
            />
            <RadioButton
              value="Mrs"
              label="Mrs."
              checkedIcon={<TitleLabel />}
              uncheckedIcon={<TitleLabel />}
              iconStyle={styles.iconStyle}
              labelStyle={title === 'Mrs' ? styles.labelStyleSelected : styles.labelStyle}
              style={title === 'Mrs' ? { ...styles.radioButtonSelected, ...styles.borderRadiusRight } : { ...styles.radioButton, ...styles.borderRadiusRight, borderLeft: `${title === 'Ms' ? 'none' : '1px solid #dadada'}` }}
            />
          </Field>
          {/* <span style={{ position: 'relative', fontSize: '12px', lineHeight: '12px', color: 'rgb(244, 67, 54)', transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms', top: '-5px' }}> {errors ? errors.title : ''} </span> */}
          <span style={{ display: `${titleNotSelected && titleNotSelected === true && title === undefined ? 'block' : 'none'}`, ...errorDivStyle }}>
            Title is required
          </span>
        </div>
        <div style={styles.row2}>
          <span style={styles.radioButtonLabel}> Marital Status </span>
          <Field
            name="maritalStatus"
            style={styles.fieldStyle}
            validate={(value) => value ? undefined : 'Marital Status is required'}
            component={RadioButtonGroup}
          >
            <RadioButton
              value="single"
              label="Single"
              checkedIcon={<TitleLabel />}
              uncheckedIcon={<TitleLabel />}
              labelStyle={maritalStatus === 'single' ? styles.labelStyleSelected : styles.labelStyle}
              iconStyle={styles.iconStyle}
              style={maritalStatus === 'single' ? { ...styles.radioButtonSelected, ...styles.borderRadiusLeft } : { ...styles.radioButton, ...styles.borderRadiusLeft, borderRight: `${maritalStatus ? 'none' : '1px solid #dadada'}` }}
            />
            <RadioButton
              value="married"
              labelStyle={maritalStatus === 'married' ? styles.labelStyleSelected : styles.labelStyle}
              checkedIcon={<TitleLabel />}
              uncheckedIcon={<TitleLabel />}
              label="Married"
              iconStyle={styles.iconStyle}
              style={maritalStatus === 'married' ? { ...styles.radioButtonSelected, ...styles.borderRadiusRight } : { ...styles.radioButton, ...styles.borderRadiusRight, borderLeft: 'none' }}
            />
          </Field>
        </div>
        {/* <span style={{ position: 'relative', fontSize: '12px', lineHeight: '12px', color: 'rgb(244, 67, 54)', transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms', top: '-15px' }}> {errors ? errors.maritalStatus : ''} </span> */}
        <span style={{ display: `${maritalStatusNotSelected && maritalStatusNotSelected === true && maritalStatus === undefined ? 'block' : 'none'}`, ...errorDivStyle }}>
          Marital Status is required
        </span>
        <TextField
          name="firstName"
          id="firstName"
          // hintText="First Name"
          floatingLabelText="First Name"
          fullWidth
          inputStyle={{ textTransform: 'capitalize' }}
          mask={inputMask}
          minLen={1}
          enforceMasking
        />

        <TextField
          name="middleName"
          id="middleName"
          // hintText="Middle Name"
          optional
          minLen={1}
          floatingLabelText="Middle Name (Optional)"
          fullWidth
          inputStyle={{ textTransform: 'capitalize' }}
          mask={inputMask}
          enforceMasking
          extraValidators={[() => nameValidator('middleName')]}
        />

        <TextField
          name="lastName"
          id="lastName"
          // hintText="Last Name"
          floatingLabelText="Last Name"
          fullWidth
          inputStyle={{ textTransform: 'capitalize' }}
          mask={inputMask}
          minLen={1}
          enforceMasking
          extraValidators={[() => nameValidator('lastName')]}
        />

        <TextField
          name="personalEmail"
          // hintText="Personal Email"
          floatingLabelText="E-mail ID (Personal)"
          email
          fullWidth
        />
        {/* mask={emailMask}*/}
      </LoanStep>
    );
  }
}

BasicInformationPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  // errors: PropTypes.object,
  title: PropTypes.string,
  maritalStatus: PropTypes.string,
  reviewPage: PropTypes.string,
  titleNotSelected: PropTypes.bool,
  maritalStatusNotSelected: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  ...(BasicInformationPageFormSelector(state, 'title', 'maritalStatus') || {}),
  ...(makeAppSelect()(state) || {}),
  ...(makeSelectBasicInformationPage()(state) || {}),
});


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BasicInformationPage);
