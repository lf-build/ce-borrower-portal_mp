import { createSelector } from 'reselect';

/**
 * Direct selector to the residenceExpenseDetailsPage state domain
 */
const selectResidenceExpenseDetailsPageDomain = () => (state) => state.get('residenceExpenseDetailsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ResidenceExpenseDetailsPage
 */

const makeSelectResidenceExpenseDetailsPage = () => createSelector(
  selectResidenceExpenseDetailsPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectResidenceExpenseDetailsPage;
export {
  selectResidenceExpenseDetailsPageDomain,
};
