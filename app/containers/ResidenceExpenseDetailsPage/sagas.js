import { takeEvery, put, select } from 'redux-saga/effects';
import { HOME_LAON_SELECTED, HOME_LAON_NOT_SELECTED } from './constants';
function* resetRentOrEMI(action) {
  if (action.meta && action.meta.form === 'residence-expense-details') {
    if (action.meta.field === 'residenceType') {
      yield put({
        type: '@@redux-form/CHANGE',
        meta: {
          form: 'residence-expense-details',
          field: 'hasHomeLoan',
          touch: false,
          persistentSubmitErrors: false,
        },
        payload: '',
      });
      yield put({
        type: '@@redux-form/CHANGE',
        meta: {
          form: 'residence-expense-details',
          field: 'homeLoanEmi',
          touch: false,
          persistentSubmitErrors: false,
        },
        payload: '',
      });

      yield put({
        type: '@@redux-form/CHANGE',
        meta: {
          form: 'residence-expense-details',
          field: 'monthlyRent',
          touch: false,
          persistentSubmitErrors: false,
        },
        payload: '',
      });
    }

    if (action.meta.field === 'hasHomeLoan') {
      // query the state using the exported selector
      const residenceExp = yield select();
      const { 'residence-expense-details': residenceExpState } = residenceExp.toJS().form;

      // when page is loaded for the first time
      if (!residenceExpState || !residenceExpState.values) {
        return;
      }

      const { values: { hasHomeLoan, homeLoanEmi } } = residenceExpState;
      if (hasHomeLoan && hasHomeLoan === 'false') {
        if (homeLoanEmi && (/[^\d]/.test(homeLoanEmi) || (homeLoanEmi.match(/^[0-9]+$/i) && homeLoanEmi.length > 6))) {
          yield put({
            type: '@@redux-form/CHANGE',
            meta: {
              form: 'residence-expense-details',
              field: 'homeLoanEmi',
              touch: false,
              persistentSubmitErrors: false,
            },
            payload: '',
          });
        }
      }
    }
  }
}
function* handleSaveFailed() {
  const appState = yield select();
  const { form: { 'residence-expense-details': residenceForm } } = appState.toJS();

  try {
    const { values: { residenceType }, syncErrors: { hasHomeLoan } } = residenceForm;

    if ((residenceType === 'OwnedSelf' || residenceType === 'Owned-Family') && hasHomeLoan !== undefined) {
      yield put({
        type: HOME_LAON_NOT_SELECTED,
      });
    }
  } catch (e) { // eslint-disable-line
    // some how when a borrower clicks Next Button immediately after landing on
    // residence expense details page this action gets called so just ignoring this call
    // and this call will be addressed on appropriate request when a homeloan option is
    // not selected.
  }
}

function* handleSaveSucceed() {
  yield put({
    type: HOME_LAON_SELECTED,
  });
}
// Individual exports for testing
export function* defaultSaga() {
  yield takeEvery('@@redux-form/CHANGE', resetRentOrEMI);
  yield takeEvery('@@redux-form/SET_SUBMIT_FAILED_residence-expense-details', handleSaveFailed);
  yield takeEvery('@@redux-form/SET_SUBMIT_SUCCEEDED_residence-expense-details', handleSaveSucceed);
  // See example in containers/HomePage/sagas.js
}

// All sagas to be loaded
export default [
  defaultSaga,
];
