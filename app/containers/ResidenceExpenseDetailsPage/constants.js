/*
 *
 * ResidenceExpenseDetailsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ResidenceExpenseDetailsPage/DEFAULT_ACTION';
export const HOME_LAON_SELECTED = 'app/ResidenceExpenseDetailsPage/HOME_LAON_SELECTED';
export const HOME_LAON_NOT_SELECTED = 'app/ResidenceExpenseDetailsPage/HOME_LAON_NOT_SELECTED';