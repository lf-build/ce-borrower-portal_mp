/*
 *
 * ResidenceExpenseDetailsPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  HOME_LAON_SELECTED,
  HOME_LAON_NOT_SELECTED,
} from './constants';

const initialState = fromJS({});

function residenceExpenseDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case HOME_LAON_NOT_SELECTED:
      return state.set('homeLoanNotSelected', true);
    case HOME_LAON_SELECTED:
      return state.set('homeLoanNotSelected', false);
    default:
      return state;
  }
}

export default residenceExpenseDetailsPageReducer;
