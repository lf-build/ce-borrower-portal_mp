/*
 *
 * ResidenceExpenseDetailsPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { RadioButton } from 'material-ui/RadioButton';
import { Field, formValueSelector } from 'redux-form/immutable';
import SelectField from 'redux-form-material-ui/lib/SelectField';
import RadioButtonGroup from 'redux-form-material-ui/lib/RadioButtonGroup';
import MenuItem from 'material-ui/MenuItem';
import styled from 'styled-components';
import makeAppSelect from '../App/selectors';

import messages from './messages';
import makeSelectResidenceExpenseDetailsPage from './selectors';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';
import { moneyMask, normalizeMoney } from '../../components/MaskedTextField/masks';


// import ExpenseDetailsTabs from '../../components/ExpenseDetailsTabs';

const shortDescription = () => <p> Let us quickly calculate your monthly expenses to find the most comfortable EMI for you. </p>;

// const BlockLabel = styled.label`
//     font-size: 12px;
//     font-weight: 400;
//     color: #79cdd5;
//     line-height: 16px;
// `;

// validation functions
const required = (value) => value == null ? 'Please select a Residence Type' : undefined;

const ResidenceExpensePageFormSelector = formValueSelector('residence-expense-details');
const errorStyle = {
  bottom: '15px',
  fontSize: '12px',
  lineHeight: '15px',
  color: 'rgb(255, 117, 106)',
  transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
  marginTop: '10px',
};
export class ResidenceExpenseDetailsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    value: 0,
  };

  handleChange = (event, index, value) => {
    this.setState({ value });
  };
  render() {
    const styles = {
      row: {
        marginBottom: '20px',
        color: '#4e4e4e',
      },
      row2: {
        marginTop: '10px',
        marginBottom: '12px',
        color: '#4e4e4e',
      },
      radioButton: {
        marginTop: '1px',
        display: 'block',
        height: 40,
        borderRight: '1px solid #dadada',
        background: '#fff none repeat scroll 0 0',
        border: '1px solid #dadada',
      },
      radioButtonSelected: {
        marginTop: '1px',
        display: 'block',
        height: 40,
        background: 'rgba(121, 205, 213, 0.15) none repeat scroll 0 0',
        border: '1px solid #79cdd5',
        boxShadow: '0 0',
        color: '#79cdd5',
      },
      iconStyle: {
        marginRight: '2px',
        color: '#4e4e4e',
        display: 'none',
      },
      labelStyle: {
        color: '#303030',
        fontSize: '14px',
        lineHeight: 3,
        textAlign: 'center',
        width: 'calc(100% - 0px)',
      },
      labelStyleSelected: {
        color: '#79cdd5',
        fontSize: '14px',
        lineHeight: 3,
        textAlign: 'center',
        width: 'calc(100% - 0px)',
      },
      borderRadiusLeft: {
        borderBottomLeftRadius: 4,
        borderTopLeftRadius: 4,
      },
      borderRadiusRight: {
        borderBottomRightRadius: 4,
        borderTopRightRadius: 4,
      },
      spanLabel: {
        lineHeight: 'normal',
        transition: 'all 240ms ease 0s',
        zIndex: 10,
        opacity: 1,
        color: 'rgba(0, 0, 0, 0.298039)',
        fontSize: 10,
        fontWeight: 400,
        letterSpacing: '0.025em',
        marginBottom: 0,
        textTransform: 'capitalize',
      },
    };
    const { hasHomeLoan, residenceType, homeLoanNotSelected } = this.props;
    const { reviewPage } = this.props;

    const showHomeLoan = (residenceType === 'OwnedSelf' || residenceType === 'Owned-Family');
    const showRent = residenceType && residenceType !== 'Company-Acco' && !showHomeLoan;
    const normalizeMoneyPayload = ({ homeLoanEmi, monthlyRent }) => ({
      homeLoanEmi: homeLoanEmi && normalizeMoney(homeLoanEmi),
      monthlyRent: monthlyRent && normalizeMoney(monthlyRent),
      hasHomeLoan: hasHomeLoan || false,
    });

    const HomeLoanIconStyle = styled.label``;

    return (
      <LoanStep
        simulateCurrentRoute={reviewPage}
        nextButtonName={reviewPage && 'Go to review'}
        noBack={reviewPage !== undefined}
        payloadGenerator={normalizeMoneyPayload}
        name={'residence-expense-details'}
        title={messages.header}
        ShortDescription={shortDescription}
        noPadding
      >
        {/* <ExpenseDetailsTabs tab={1} /> -- commented as per CE-1370 */}

        <div style={{ padding: '25px' }}>
          <Field
            name="residenceType"
            fullWidth
            floatingLabelText={'Residence Type'}
            floatingLabelStyle={{ fontSize: '12px' }}
            floatingLabelFocusStyle={{ fontSize: '14px' }}
            floatingLabelShrinkStyle={{ fontSize: '14px' }}
            component={SelectField}
            hintText="Residence Type"
            validate={required}
            value={this.state.value}
            selectedMenuItemStyle={{ color: '#79cdd5' }}
            errorStyle={{ fontSize: 12, color: '#ff756a', textAlign: 'left', lineHeight: '15px', marginTop: '6px' }}
          >
            <MenuItem value={'OwnedSelf'} primaryText="Self Owned" />
            <MenuItem value={'Owned-Family'} primaryText="Owned With Family" />
            <MenuItem value={'Rented-Family'} primaryText="Rented With Family" />
            <MenuItem value={'Rented-Alone'} primaryText="Rented Bachelor Staying Alone" />
            <MenuItem value={'Rented-Friends'} primaryText="Rented With Friends" />
            <MenuItem value={'PG-Hostel'} primaryText="PG or Hostel" />
            <MenuItem value={'Company-Acco'} primaryText="Company Provided Accommodation" />
          </Field>

          <div style={styles.row2} className={showHomeLoan ? '' : 'hidden'}>
            <span style={styles.spanLabel}> Do you have a home loan?</span>
            <Field
              name="hasHomeLoan"
              style={{ display: 'flex' }}
              validate={showHomeLoan ? (value) => value ? undefined : 'Please let us know if you have Home Loan' : undefined}
              component={RadioButtonGroup}
            >
              <RadioButton
                value={'true'}
                label="Yes"
                checkedIcon={<HomeLoanIconStyle />}
                uncheckedIcon={<HomeLoanIconStyle />}
                labelStyle={hasHomeLoan === 'true' ? { ...styles.labelStyleSelected, ...styles.borderRadiusLeft } : { ...styles.labelStyle, ...styles.borderRadiusLeft }}
                iconStyle={styles.iconStyle}
                style={hasHomeLoan === 'true' ? { ...styles.radioButtonSelected, ...styles.borderRadiusLeft } : { ...styles.radioButton, ...styles.borderRadiusLeft }}
              />
              <RadioButton
                value={'false'}
                labelStyle={hasHomeLoan === 'false' ? { ...styles.labelStyleSelected, ...styles.borderRadiusRight } : { ...styles.labelStyle, ...styles.borderRadiusRight }}
                checkedIcon={<HomeLoanIconStyle />}
                uncheckedIcon={<HomeLoanIconStyle />}
                label="No"
                iconStyle={styles.iconStyle}
                style={hasHomeLoan === 'false' ? { ...styles.radioButtonSelected, ...styles.borderRadiusRight } : { ...styles.radioButton, ...styles.borderRadiusRight }}
              />
            </Field>
            <span style={{ display: `${homeLoanNotSelected && homeLoanNotSelected === true && hasHomeLoan === undefined ? 'block' : 'none'}`, ...errorStyle }}>
              Please let us know if you have Home Loan
            </span>
          </div>

          <div className={(showHomeLoan && hasHomeLoan === 'true') ? 'row' : 'hidden'}>
            <div className={'amount-label-div'}>
              <label
                htmlFor="homeLoanEmi"
                className="prefix"
                style={{ paddingRight: '4px' }}
              >₹</label>
            </div>
            <div className={'amount-textfield-div'}>
              <TextField
                className={(showHomeLoan && hasHomeLoan === 'true') ? '' : 'hidden'}
                name="homeLoanEmi"
                fullWidth
                hintText=""
                minLen={1}
                floatingLabelText="What is the total EMI on your home loan(s)?"
                mask={moneyMask}
                optional={!(showHomeLoan && hasHomeLoan === 'true')}
                floatingLabelFocusStyle={{ width: '285px' }}
                floatingLabelShrinkStyle={{ width: '285px' }}
                extraValidators={[(value) => {
                  if (!(showHomeLoan && hasHomeLoan === 'true')) {
                    return undefined;
                  }
                  if (/[^\d]/.test(value)) {
                    return 'Please enter a valid number';
                  }
                  if (value.length > 6) {
                    return 'Entered amount is out of range';
                  }
                  return undefined;
                }]}
              />
            </div>

          </div>

          <div className={showRent ? 'row' : 'hidden'}>
            <div className={'amount-label-div'}>
              <label
                htmlFor="monthlyRent"
                className="prefix"
                style={{ paddingRight: '4px' }}
              >₹</label>
            </div>
            <div className={'amount-textfield-div'}>
              <TextField
                className={showRent ? '' : 'hidden'}
                name="monthlyRent"
                minLen={1}
                fullWidth
                hintText=""
                floatingLabelText="Monthly Rent"
                optional={!showRent}
                mask={moneyMask}
                extraValidators={[(value) => {
                  if (!showRent) {
                    return undefined;
                  }
                  if (/[^\d]/.test(value)) {
                    return 'Please enter a valid number';
                  }
                  if (value.length > 6) {
                    return 'Entered amount is out of range';
                  }
                  return undefined;
                }]}
              />
            </div>

          </div>
        </div>

      </LoanStep>
    );
  }
}

ResidenceExpenseDetailsPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  hasHomeLoan: PropTypes.string,
  residenceType: PropTypes.string,
  reviewPage: PropTypes.string,
  homeLoanNotSelected: PropTypes.bool,
};

// const mapStateToProps = createStructuredSelector({
//   ResidenceExpenseDetailsPage: makeSelectResidenceExpenseDetailsPage(),
// });

const mapStateToProps = (state) => ({
  ...(ResidenceExpensePageFormSelector(state, 'hasHomeLoan', 'residenceType') || {}),
  ...makeAppSelect()(state),
  ...(makeAppSelect()(state) || {}),
  ...(makeSelectResidenceExpenseDetailsPage()(state) || {}),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ResidenceExpenseDetailsPage);
