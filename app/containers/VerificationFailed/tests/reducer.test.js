
import { fromJS } from 'immutable';
import verificationFailedReducer from '../reducer';

describe('verificationFailedReducer', () => {
  it('returns the initial state', () => {
    expect(verificationFailedReducer(undefined, {})).toEqual(fromJS({}));
  });
});
