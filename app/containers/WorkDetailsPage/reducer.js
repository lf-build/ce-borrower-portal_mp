/*
 *
 * WorkDetailsPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  // DEFAULT_ACTION,
} from './constants';
import {
  LOOKUP_REQUEST_STARTED,
  LOOKUP_REQUEST_FULFILLED,
} from '../../sagas/lookup/constants';
const initialState = fromJS({
  // Temp. list of employeers from companydb
  // once autocomplete functionality with dynamic source
  // is done, will call actual api to fill this up.
  employers: [
    {
      cin: 'U45400MH2012PLC234318',
      name: "CAPACIT'E INFRAPROJECTS LIMITED",
    },
    {
      cin: 'U34200DL2013PLC261486',
      name: 'CAPARO AUTOTECH LIMITED',
    },
    {
      cin: 'U74101DL2000PLC105609',
      name: 'CAPARO ENGINEERING INDIA LIMITED',
    },
    {
      cin: 'U74899DL1994PLC058269',
      name: 'CAPARO MARUTI LIMITED',
    },
    {
      cin: 'U34300DL2007PTC158139',
      name: 'CAPARO MI STEEL PROCESSING PRIVATE LIMITED',
    },
    {
      cin: 'U40101DL2008PLC219872',
      name: 'CAPARO POWER LIMITED',
    },
    {
      cin: 'U72200MH1992PTC197069',
      name: 'CAPGEMINI INDIA PRIVATE LIMITED',
    },
    {
      cin: 'U74899DL1994PTC059878',
      name: 'CAPITAL ELECTRICALS PRIVATE LIMITED',
    },
    {
      cin: 'U51505DL2003PLC121799',
      name: 'CAPITAL ELECTROTECH LIMITED',
    },
    {
      cin: 'U65921KL1994PLC008301',
      name: 'CAPITAL FINSERVE LIMITED',
    },
    {
      cin: 'U65990MH2008PLC181572',
      name: 'CAPITAL FIRST COMMODITIES LIMITED',
    },
    {
      cin: 'L29120MH2005PLC156795',
      name: 'CAPITAL FIRST LIMITED',
    },
    {
      cin: 'U45400DL2010PTC203755',
      name: 'CAPITAL INFRAPROJECTS PRIVATE LIMITED',
    },
    {
      cin: 'U80903DL2004PTC124361',
      name: 'CAPITAL INSTITUTE OF COMPETITION TRAINING PRIVATE LIMITED',
    },
    {
      cin: 'U93000MH2008PTC182156',
      name: 'CAPITAL INVESTMENT RESEARCH SERVICES PRIVATE LIMITED',
    },
  ],
});

function workDetailsPageReducer(state = initialState, action) {
  if (action.type === `${LOOKUP_REQUEST_FULFILLED}_${state.get('tag')}`) {
    return state.set('employers', Object.keys(action.meta).map((key) => action.meta[key]));
  }

  switch (action.type) {
    case LOOKUP_REQUEST_STARTED:
      return state.set('tag', action.meta.tag);
    default:
      return state;
  }
}

export default workDetailsPageReducer;
