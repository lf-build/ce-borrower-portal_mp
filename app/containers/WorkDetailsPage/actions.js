/*
 *
 * WorkDetailsPage actions
 *
 */

import {
  DEFAULT_ACTION,
  EMPLOYER_NAME_CHANGE,
  SAVE_EMP_NAME_FREE_TEXT,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function employerNameChange(text) {
  return {
    type: EMPLOYER_NAME_CHANGE,
    payload: {
      text,
    },
  };
}

export function saveEmpNameFreeText(text) {
  return {
    type: SAVE_EMP_NAME_FREE_TEXT,
    payload: {
      text,
    },
  };
}
