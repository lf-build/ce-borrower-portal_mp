import { takeEvery, put, select } from 'redux-saga/effects';
import { EMPLOYER_NAME_CHANGE, SAVE_EMP_NAME_FREE_TEXT } from './constants';

function* handleEmployerNameChange(action) {
  // query the state using the exported selector
  const employerName = yield select();
  const { 'work-details': employerState } = employerName.toJS().form;

  // when page is loaded for the first time and user starts entering a employer name, so here to avoid undefined error putting this logic.
  if (!employerState || !employerState.values || !employerState.values.employer) {
    return;
  }

  const { values: { employer }, anyTouched } = employerState;

  if (!employer && anyTouched) {
    // console.log('required error');
    yield put({
      type: '@@redux-form/UPDATE_SYNC_ERRORS',
      meta: {
        form: 'work-details',
      },
      payload: {
        syncErrors: {
          employer: 'Employer Name cannot be left blank',
        },
      },
    });
    // raise required error
    return;
  }

  if (typeof employer !== 'object' && anyTouched) {
    // console.log('select from dropdown list error');
    yield put({
      type: '@@redux-form/UPDATE_SYNC_ERRORS',
      meta: {
        form: 'work-details',
      },
      payload: {
        syncErrors: {
          employer: 'Select Employer Name from dropdown',
        },
      },
    });
    // raise select from dropdown list error
    return;
  }

  if (employer.name !== action.payload.text) {
    // console.log('select from dropdown list error', employer.name, action.payload.text);
    yield put({
      type: '@@redux-form/UPDATE_SYNC_ERRORS',
      meta: {
        form: 'work-details',
      },
      payload: {
        syncErrors: {
          employer: 'Select Employer Name from dropdown',
        },
      },
    });
    // // raise select from dropdown list error
    return;
  }
  // console.log('All is well. in hanlde employer name chage ', employer);
  yield put({
    type: '@@redux-form/UPDATE_SYNC_ERRORS',
    meta: {
      form: 'work-details',
    },
    payload: {
      syncErrors: { },
    },
  });
}

function* handleEmpNameFreeText(action) {
  const { payload: { text } } = action;

  if (text) {
    yield put({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'work-details',
        field: 'employer',
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: {
        cin: text.toUpperCase(),
        name: text.toUpperCase(),
      },
    });
  }
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery(EMPLOYER_NAME_CHANGE, handleEmployerNameChange);
  yield takeEvery(SAVE_EMP_NAME_FREE_TEXT, handleEmpNameFreeText);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
