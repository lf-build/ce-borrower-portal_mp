/*
 * WorkDetailsPage Messages
 *
 * This contains all the text for the WorkDetailsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.WorkDetailsPage.header',
    defaultMessage: 'What about work?',
  },
});
