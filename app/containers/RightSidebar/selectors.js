import { createSelector } from 'reselect';

/**
 * Direct selector to the rightSidebar state domain
 */
const selectRightSidebarDomain = () => (state) => state.get('rightSidebar');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RightSidebar
 */

const makeSelectRightSidebar = () => createSelector(
  selectRightSidebarDomain(),
  (substate) => substate.toJS()
);

export default makeSelectRightSidebar;
export {
  selectRightSidebarDomain,
};
