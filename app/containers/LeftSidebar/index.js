/*
 *
 * LeftSidebar
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import ApplicationProgress from '../ApplicationProgress';

export class LeftSidebar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return <ApplicationProgress orientation={'vertical'} />;
  }
}

LeftSidebar.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};


function mapDispatchToProps() { // dispatch) {
  return {
    // dispatch,
  };
}

export default connect(null, mapDispatchToProps)(LeftSidebar);
