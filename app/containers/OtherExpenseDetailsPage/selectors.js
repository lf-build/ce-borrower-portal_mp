import { createSelector } from 'reselect';

/**
 * Direct selector to the otherExpenseDetailsPage state domain
 */
const selectOtherExpenseDetailsPageDomain = () => (state) => state.get('otherExpenseDetailsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by OtherExpenseDetailsPage
 */

const makeSelectOtherExpenseDetailsPage = () => createSelector(
  selectOtherExpenseDetailsPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectOtherExpenseDetailsPage;
export {
  selectOtherExpenseDetailsPageDomain,
};
