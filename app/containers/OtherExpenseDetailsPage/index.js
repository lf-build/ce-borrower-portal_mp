/*
 *
 * OtherExpenseDetailsPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import makeSelectOtherExpenseDetailsPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';
import ExpenseDetailsTabs from '../../components/ExpenseDetailsTabs';
import { moneyMask, normalizeMoney } from '../../components/MaskedTextField/masks';
import makeAppSelect from '../App/selectors';

const shortDescription = () => <p> Let us quickly calculate your monthly expenses to find the most comfortable EMI for you. </p>;

export class OtherExpenseDetailsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const normalizeMoneyPayload = ({ otherEmis }) => ({
      otherEmis: otherEmis && normalizeMoney(otherEmis),
    });
    const { App: { reviewPage } } = this.props;

    return (
      <LoanStep
        simulateCurrentRoute={reviewPage}
        nextButtonName={reviewPage && 'Go to review'}
        noBack={reviewPage !== undefined}
        payloadGenerator={normalizeMoneyPayload}
        name={'other-expense-details'}
        title={messages.header}
        ShortDescription={shortDescription}
        noPadding
      >
        <ExpenseDetailsTabs tab={3} />
        <div style={{ padding: '25px' }}>
          <div className={'row'}>
            <div className={'amount-label-div'}>
              <label
                htmlFor="otherEmis"
                className="prefix"
                style={{ paddingRight: '4px' }}
              >₹</label>
            </div>
            <div className={'amount-textfield-div'}>
              <TextField
                name="otherEmis"
                fullWidth
                hintText=""
                // hintStyle={{ marginLeft: '46%' }}
                // inputStyle={{ textAlign: 'center' }}
                floatingLabelText="Other EMIs (optional)"
                optional
                mask={moneyMask}
                extraValidators={[(value) => {
                  if (!value || value.length === 0) {
                    return undefined;
                  }
                  if (/[^\d]/.test(value)) {
                    return 'Please enter a valid number';
                  }
                  if (value.length > 6) {
                    return 'Entered amount is out of range';
                  }
                  return undefined;
                }]}

              />
            </div>

          </div>
          {/* <TextField
          name="monthlyExpenses"
          fullWidth
          hintText="₹"
          floatingLabelText="Monthly expenses (Excluding rent & EMIs )"
          optional
          mask={'999999'}
        />*/}
        </div>
      </LoanStep>
    );
  }
}

OtherExpenseDetailsPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  App: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  OtherExpenseDetailsPage: makeSelectOtherExpenseDetailsPage(),
  App: makeAppSelect(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OtherExpenseDetailsPage);
