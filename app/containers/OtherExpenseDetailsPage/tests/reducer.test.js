
import { fromJS } from 'immutable';
import otherExpenseDetailsPageReducer from '../reducer';

describe('otherExpenseDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(otherExpenseDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
