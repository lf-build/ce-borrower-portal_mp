/*
 * ConsentDeclined Messages
 *
 * This contains all the text for the ConsentDeclined component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ConsentDeclined.header',
    defaultMessage: 'Consent Declined',
  },
});
