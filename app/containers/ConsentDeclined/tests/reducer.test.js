
import { fromJS } from 'immutable';
import consentDeclinedReducer from '../reducer';

describe('consentDeclinedReducer', () => {
  it('returns the initial state', () => {
    expect(consentDeclinedReducer(undefined, {})).toEqual(fromJS({}));
  });
});
