import { createSelector } from 'reselect';

/**
 * Direct selector to the consentDeclined state domain
 */
const selectConsentDeclinedDomain = () => (state) => state.get('consentDeclined');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ConsentDeclined
 */

const makeSelectConsentDeclined = () => createSelector(
  selectConsentDeclinedDomain(),
  (substate) => substate.toJS()
);

export default makeSelectConsentDeclined;
export {
  selectConsentDeclinedDomain,
};
