/*
 * ApplicationRejected Messages
 *
 * This contains all the text for the ApplicationRejected component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ApplicationRejected.header',
    defaultMessage: 'Sorry!',
  },
});
