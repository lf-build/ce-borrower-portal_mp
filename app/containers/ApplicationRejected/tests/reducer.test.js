
import { fromJS } from 'immutable';
import applicationRejectedReducer from '../reducer';

describe('applicationRejectedReducer', () => {
  it('returns the initial state', () => {
    expect(applicationRejectedReducer(undefined, {})).toEqual(fromJS({}));
  });
});
