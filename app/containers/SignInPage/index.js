/*
 *
 * SignInPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';
// import { Link } from 'react-router';
import makeSelectSignInPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';
import { mobileMask, normalizeMobile } from '../../components/MaskedTextField/masks';
import ReApply from '../../components/ReApply';
import {
  acceptReapply as acceptReapplyActionCreator,
  denyReapply as denyReapplyActionCreator,
  sendLoginOtp as sendLoginOtpActionCreator,
  usernameNotFound as usernameNotFoundActionCreator,
} from './actions';

// const ForgotPasswordBlock = styled.div`
//     font-size: 14px;
//     font-weight: 500;
//     text-align: center;
//     margin-bottom: 0;
//     marginTop: 10px;
// `;

// const ForgotPasswordLink = styled(Link)`
//     // color: #79cdd5;
//     // cursor: pointer;
//     font-size: 13px;
//     color: #afafaf;
//     text-decoration: none;
//     letter-spacing: 0.4px;

// `;

const SignInBlock = styled.div`
  color: #79cdd5;
  font-size: 24px;
  font-weight: 100;
  letter-spacing: 0.03em;  
  // padding: 0 70px;
  line-height: normal;
  text-align: center;
`;

const AnchorLink = styled.span`
  color: #79cdd5;
  cursor: pointer;
  text-align: center;
  z-index: 999;
  margin-top: 100;
`;

const SendOtp = styled.span`
  float: right;
  color: #9da0a0;
  font-size: 14px;
  font-weight: 500;
  text-align: right;
  margin-bottom: 0;
  padding-right: 25px;
  margin-top: 10px;
`;

const styles = {
  loaderStyle: {
    backgroundImage: 'url(data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==)',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'right center',
    textTransform: 'uppercase',
  },
  errorMsgStyle: {
    position: 'relative',
    bottom: '15px',
    fontSize: '12px',
    lineHeight: '15px',
    color: 'rgb(255, 117, 106)',
    transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
    marginTop: '6px',
  },
};

export class SignInPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { usernameNotFound } = this.props;
    // when a Username is not found in our database, so we need to again disable the error message and change the button
    // from Apply to Sign In.
    usernameNotFound(false);
  }
  render() {
    const maskNumber = ({ username }) => username ? { username: normalizeMobile(username) } : {};
    const { SignInPage: { promptForReapply, showSendOtpLoader, notFound }, acceptReapply, denyReapply, sendOtp } = this.props;

    // check if state has promptForReapply flag true.
    if (promptForReapply) {
      return (
        <ReApply
          openDialog={promptForReapply}
          acceptReapply={acceptReapply}
          denyReapply={denyReapply}
        />
      );
    }

    return (
      <div style={{ paddingTop: '10%' }}>
        <LoanStep
          payloadGenerator={maskNumber}
          saveErrorClass={'login'}
          name={'sign-in'}
          title={messages.empty}
          reduceWidth
          navigationPanel={(props) =>
            <div style={{ textAlign: 'center' }}>
              <div className={notFound ? '' : 'hidden'} style={{ ...styles.errorMsgStyle }}>
                No account found. Please Apply here
            </div>
              <RaisedButton
                className={notFound ? 'hidden' : ''}
                disabled={props.isLoadingOrSubmiting}
                onTouchTap={props.handleGoNext}
                primary
                label="Sign In"
              />
              <RaisedButton
                className={notFound ? '' : 'hidden'}
                disabled={props.isLoadingOrSubmiting}
                onTouchTap={() => (window.location.href = 'https://www.qbera.com/personal-loan-details.html')}
                primary
                label="Apply"
              />
              {/* <ForgotPasswordBlock>
                <ForgotPasswordLink to={'/user/forgot-password'}> Forgot the password? </ForgotPasswordLink>
              </ForgotPasswordBlock> */}
            </div>
          }
        >

          <SignInBlock>
            {messages.header.defaultMessage}
          </SignInBlock>
          <span style={{ textAlign: 'center', color: '#737376', fontSize: '12px', float: 'left' }}>Sign-in to your Qbera account to complete your loan application and get funds in 24 hours!</span>

          <div className="row">
            <div className={'mobile-label-div'}>
              <label
                htmlFor="username"
                className="prefix"
                style={{ paddingRight: '5px' }}
              >+91</label>
            </div>
            <div className={'mobile-textfield-div'}>
              <TextField
                name={'username'}
                hintText=""
                hintStyle={{ fontWeight: 500, color: 'rgba(0, 0, 0, 0.870588)' }}
                floatingLabelText="Mobile Number"
                fullWidth
                mask={mobileMask}
                extraValidators={[(value) => value && normalizeMobile(value.toString()).match(/^[6-9][0-9]{9}$/) ? undefined : 'Mobile number is invalid']}
                inputStyle={showSendOtpLoader ? { ...styles.loaderStyle } : {}}
              />
            </div>
            <SendOtp><AnchorLink onClick={() => sendOtp()}>Send OTP</AnchorLink>
            </SendOtp>
          </div>

          <div className="row">
            <div className={'mobile-label-div'}>
              <label
                htmlFor="OTP"
                className="prefix"
                style={{ paddingRight: '5px' }}
              ></label>
            </div>
            <div className={'mobile-textfield-div'}>
              <TextField
                name={'otp'}
                hintText="OTP"
                floatingLabelText="OTP"
                type={'password'}
                minLen={6}
                maxLen={15}
                fullWidth
              />
            </div>
          </div>

          <div style={{ marginTop: '25px', marginBottom: '25px' }} />

        </LoanStep>
      </div >
    );
  }
}

SignInPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  SignInPage: React.PropTypes.object,
  acceptReapply: React.PropTypes.func,
  denyReapply: React.PropTypes.func,
  sendOtp: React.PropTypes.func,
  usernameNotFound: React.PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  SignInPage: makeSelectSignInPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    acceptReapply: () => dispatch(acceptReapplyActionCreator()),
    denyReapply: () => dispatch(denyReapplyActionCreator()),
    sendOtp: () => dispatch(sendLoginOtpActionCreator()),
    usernameNotFound: (notFound) => dispatch(usernameNotFoundActionCreator(notFound)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInPage);
