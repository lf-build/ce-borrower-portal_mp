// import { takeEvery, call, put } from 'redux-saga/effects';

// const fakeCall = ({ stage, fact, action, payload }) => new Promise((resolve, reject) => setTimeout(() => reject({ stage, fact, action, payload }), 2000));

// function* executeWorkflow(action) {
//   const { tag } = action.meta;
//   if (!tag) {
//     yield put({
//       type: 'EXECUTE_WORKFLOW_REQUEST_EXECUTION_DENIED',
//       meta: {
//         ...action.meta,
//         reason: 'Untagged workflow execution is not supported, Please provide a property named *tag*',
//       },
//     });
//   }
//   yield put({ type: `EXECUTE_WORKFLOW_REQUEST_STARTED_${tag}`, meta: action.meta });
//   try {
//     const response = yield call(fakeCall, action.meta);
//     yield put({ type: `EXECUTE_WORKFLOW_REQUEST_FULFILLED_${tag}`, meta: { response } });
//   } catch (error) {
//     yield put({ type: `EXECUTE_WORKFLOW_REQUEST_FAILED_${tag}`, meta: { ...action.meta, error } });
//   }
//   yield put({ type: `EXECUTE_WORKFLOW_REQUEST_ENDED_${tag}`, meta: action.meta });
// }

// Individual exports for testing
export function* workflowSaga() {
  // See example in containers/HomePage/sagas.js
  // yield takeEvery('EXECUTE_WORKFLOW_REQUEST', executeWorkflow);
}

// All sagas to be loaded
export default [
  workflowSaga,
];
