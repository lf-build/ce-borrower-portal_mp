/*
 *
 * SocialPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { createStructuredSelector } from 'reselect';

import makeSelectSocialPage from './selectors';
import messages from './messages';
import LoanStep from '../LoanStep';
import VerificationProcess from '../../components/VerificationProcess';
import makeSelectApp from '../App/selectors';
import NavigationPanel from './NavigationPanel';
import { lendoCallRequest, getInitialOfferInfoRequest } from './actions';
import { getBasicAppInfoRequest } from '../InitialOfferPage/actions';

const SocialInfo = styled.p`
    // color: #79cdd5;
    // font-size: 13px;
    // font-weight: 400;
    // letter-spacing: 0.025em;
    // margin: 20px 0;
    // line-height: normal;
    // text-align: center;
    font-size: 12px;
    color: #000;
    line-height: 22px;
    text-align: left;
`;

export class SocialPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const {
      App: {
        application,
        initialOffer,
      },
      dispatch,
    } = this.props;

    if (!application) {
      dispatch(getBasicAppInfoRequest());
    }

    if (!initialOffer) {
      setTimeout(() => dispatch(getInitialOfferInfoRequest()), 1000);
    }
  }
  loadLendoScript() {
    const {
      App: {
        application: {
          applicationNumber,
          firstName,
          middleName,
          lastName,
          dateOfBirth,
          personalEmail,
          mobileNumber,
          employerName,
        },
      },
      dispatch,
    } = this.props;

    dispatch(lendoCallRequest());
    const LenddoVerifyConfig = {
      scriptId: process.env.LENDO_SCRIPT_ID,
      application_id: applicationNumber,
      verificationFields: {
        /* sample verification field values */
        firstname: firstName,
        middlename: middleName === undefined ? '' : middleName,
        lastname: lastName,
        birthdate: dateOfBirth,
        email: personalEmail,
        employer: employerName,
        mobilephone: mobileNumber,
      },
      onSubmit(cb) {
        const errors = false;
        if (errors === false) {
          cb();
        }
      },
    };
    window.LenddoVerifyConfig = LenddoVerifyConfig;
    function initLendo() {
      const la = document.createElement('script');
      la.type = 'text/javascript';
      la.async = true;
      la.src = `https://authorize.partner-service.link/verify.js?v=${Date.now()}`;
      const s = document.getElementById('lendoScript');
      s.parentNode.insertBefore(la, s);
      const hideLendoSpinner = () => {
        if (document.getElementById('VERIFI_ME')) {
          document.getElementById('lenddo_verify').removeChild(document.getElementById('lenddoSpinner'));
        } else {
          setTimeout(hideLendoSpinner, 10);
        }
      };
      hideLendoSpinner();
    }

    function ensureScriptTag() {
      if (!document.getElementById('lendoScript')) {
        setTimeout(ensureScriptTag, 10);
      } else {
        initLendo();
      }
    }
    setTimeout(ensureScriptTag, 10);
  }
  render() {
    const { App, SocialPage: domain } = this.props;
    if (!App || !App.initialOffer) {
      // return <h1> Offer Not yet generated </h1>;
      return (
        <LoanStep
          title={messages.header}
          navigationPanel={() => <span />}
          noPadding
        >
          <VerificationProcess step={1} />
          <div style={{ display: (!App || !App.initialOffer) || (!App || !App.application) ? 'block' : 'none', marginTop: '20%' }} className="spinner">
            <div className="bounce1" style={{ backgroundColor: '#79cdd5' }}></div>
            <div className="bounce2" style={{ backgroundColor: '#79cdd5' }}></div>
            <div className="bounce3" style={{ backgroundColor: '#79cdd5' }}></div>
          </div>
        </LoanStep>
      );
    }

    if (App && App.application && App.initialOffer) {
      this.loadLendoScript();
    }

    const { initialOffer: { fileType } } = App;
    const showMessage = (!domain.enableLendo) && (!domain.pending);
    const showLendo = domain.enableLendo && !domain.pending;
    const nextButtonName = fileType === 'Thick' && domain.enableLendo ? 'Skip' : 'Next';
    const disableButton = fileType !== 'Thick' && domain.enableLendo;

    return (
      <LoanStep
        title={messages.header}
        navigationPanel={(props) => <NavigationPanel showSkip={!disableButton} buttonLabel={nextButtonName} {...props} />}
        noPadding
      >
        <script id="lendoScript"> </script>
        <VerificationProcess step={1} />
        <div style={{ padding: 25 }}>
          <SocialInfo>
            Please connect your facebook and e-mail accounts with our secure connection for us to calculate your social credit score.
        </SocialInfo>
          <br />
          <SocialInfo style={{ display: showMessage ? 'block' : 'none' }}>Verification Done. Please click next to move ahead.</SocialInfo>

          <div className={'manual-label'} style={{ display: showLendo ? 'block' : 'none' }}>Verification using Verifi.me</div>

          <div style={{ display: domain.pending ? 'block' : 'none' }} className="spinner">
            <div className="bounce1" style={{ backgroundColor: '#79cdd5' }}></div>
            <div className="bounce2" style={{ backgroundColor: '#79cdd5' }}></div>
            <div className="bounce3" style={{ backgroundColor: '#79cdd5' }}></div>
          </div>

          <div style={{ textAlign: 'left', marginTop: '10px', marginBottom: '30px', width: '100%', display: showLendo ? 'block' : 'none' }}>
            <span className={'col-sm-3'} />
            <div id="lenddo_verify">
              <div id="lenddoSpinner" className="spinner">
                <div className="bounce1" style={{ backgroundColor: '#79cdd5' }}></div>
                <div className="bounce2" style={{ backgroundColor: '#79cdd5' }}></div>
                <div className="bounce3" style={{ backgroundColor: '#79cdd5' }}></div>
              </div>
            </div>
            <span className={'col-sm-3'} />
          </div>
        </div>
      </LoanStep>
    );
  }
}

SocialPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  App: PropTypes.object,
  SocialPage: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  SocialPage: makeSelectSocialPage(),
  App: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SocialPage);
