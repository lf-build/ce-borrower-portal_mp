import { createSelector } from 'reselect';

/**
 * Direct selector to the socialPage state domain
 */
const selectSocialPageDomain = () => (state) => state.get('socialPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by SocialPage
 */

const makeSelectSocialPage = () => createSelector(
  selectSocialPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectSocialPage;
export {
  selectSocialPageDomain,
};
