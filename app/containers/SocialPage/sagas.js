import { takeLatest, put } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import {
  LENDO_CALL_REQUEST,
  GET_INITIAL_OFFER_INFO_REQUEST } from './constants';
import { GET_BASIC_APP_INFO_REQUEST } from '../InitialOfferPage/constants';
import * as actions from '../InitialOfferPage/actions';
import * as socialActions from './actions';

export function* requestLendo() {
  const tag = 'lendo';
  // Select username from store
  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'verify',
    command: 'set-social',
  }, { tag });

  if (executionRequestAccepted) {
    // yield put(actions.generateInitialOfferStarted());

    // Wait for workflow to either fail or succeed.
    yield uplink.waitForUplinkExecutionSuccessOrFailure(tag);
  } else {
    // yield put(actions.generateInitialOfferDenied({ message: 'Workflow did not accept the request.' }));
    // navigationHelper.goTo({ name: 'initialOffer' }, 'internalServerError', false);
  }

  // yield put(actions.generateInitialOfferEnded());
}

export function* getBasicAppInfo() {
  const form = 'get-basic-app-info';

  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'application',
    command: 'get-basic-app-info',
  }, { tag: form });

  if (executionRequestAccepted) {
    yield put(actions.getBasicAppInfoRequestStarted());

    const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);

    if (valid) {
      yield put(actions.getBasicAppInfoRequestFulfilled(valid.meta.body));
    } else {
      yield put(actions.getBasicAppInfoRequestFailed('error while fetching basic app information'));
    }

    yield put(actions.getBasicAppInfoRequestEnded());
  }
}

export function* getInitialOfferInfo() {
  const form = 'get-initial-offer-details';

  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'offer',
    command: 'get-initial-offer-details',
  }, { tag: form });

  if (executionRequestAccepted) {
    yield put(socialActions.getInitialOfferInfoRequestStarted());

    const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);

    if (valid) {
      yield put(socialActions.getInitialOfferInfoRequestFulfilled(valid.meta.body));
    } else {
      yield put(socialActions.getInitialOfferInfoRequestFailed('error while fetching basic app information'));
    }

    yield put(socialActions.getInitialOfferInfoRequestEnded());
  }
}

export function* defaultSaga() {
  yield takeLatest(LENDO_CALL_REQUEST, requestLendo);
  yield takeLatest(GET_BASIC_APP_INFO_REQUEST, getBasicAppInfo);
  yield takeLatest(GET_INITIAL_OFFER_INFO_REQUEST, getInitialOfferInfo);
}
// All sagas to be loaded
export default [
  defaultSaga,
];
