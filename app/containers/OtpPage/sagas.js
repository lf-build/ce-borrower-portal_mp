import { takeEvery, put, select } from 'redux-saga/effects';
import { browserHistory } from 'react-router';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { SAVE_LOANSTEP_FULFILLED } from '../../containers/LoanStep/constants';
import { userSignedIn, loadUserProfile } from '../../sagas/auth/actions';
import { RESEND_OTP_REQUEST } from './constants';
import * as resendOtpActions from './actions';

function* handleOtpFailed(action) {
  try {
    if (action.meta.response.meta.error.body.maxAttemptsReached) {
      browserHistory.replace('/application/verification-failed');
    }
  } catch (error) { // eslint-disable-line no-empty

  }
}

function* resendOtp() {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'sign-up',
    command: 'send-otp',
  }, {
    tag: 'resendOtp',
  }))) {
    return;
  }
  yield put(resendOtpActions.resendOtpRequestStarted());
  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('resendOtp');

  if (valid) {
    yield put(resendOtpActions.resendOtpRequestFulfilled(valid.meta.body));
  } else {
    yield put(resendOtpActions.resendOtpRequestFailed(''));
  }

  yield put(resendOtpActions.resendOtpRequestEnded());

  // if the request gives a valid response then we will show an alert
  if (valid) {
    const mobileForm = yield select();
    const mobileState = mobileForm.toJS().form.mobile;

    if (mobileState && mobileState.values) {
      const { values: { mobileNumber } } = mobileState;
      alert(`OTP sent to +91 ${mobileNumber}`); // eslint-disable-line no-alert
    }
  }
}

function* handleResenOtpFailed(action) {
  const { meta: { error: { body } } } = action;

  if (body) {
    const { maxAttemptsReached } = body;
    if (maxAttemptsReached) {
      browserHistory.replace('/application/verification-failed');
    }
  }
}

function* loginSaga(action) {
  yield put(userSignedIn(action.meta.response.meta.body.token));
  yield put(loadUserProfile({
    flow: 'RETURN-USER',
    token: action.meta.response.meta.body.token,
    mobileNumber: action.meta.response.meta.body.application.personalMobile,
  }));
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeEvery('app/LoanStep/SAVE_LOANSTEP_FAILED_otp', handleOtpFailed);
  yield takeEvery(RESEND_OTP_REQUEST, resendOtp);
  yield takeEvery(`${SAVE_LOANSTEP_FULFILLED}_otp`, loginSaga);
  yield takeEvery('app/uplink/EXECUTE_UPLINK_REQUEST_FAILED_resendOtp', handleResenOtpFailed);
  // See example in containers/HomePage/sagas.js
}

// All sagas to be loaded
export default [
  defaultSaga,
];
