import { createSelector } from 'reselect';

/**
 * Direct selector to the otpPage state domain
 */
const selectOtpPageDomain = () => (state) => state.get('otpPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by OtpPage
 */

const makeSelectOtpPage = () => createSelector(
  selectOtpPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectOtpPage;
export {
  selectOtpPageDomain,
};
