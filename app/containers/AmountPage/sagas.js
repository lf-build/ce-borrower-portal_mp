import { takeEvery, put } from 'redux-saga/effects';

function* syncSliderValueToAmountTextField(action) {
  if (action.meta && action.meta.form === 'amount') {
    if (action.meta.field === 'amountField') {
      const amountToSync = (action.payload && parseInt(action.payload.toString().replace(/[^\d]/g, ''), 10)) || 0;
      if (amountToSync.toString() !== action.payload) {
        yield put({
          type: '@@redux-form/BLUR',
          meta: {
            form: 'amount',
            field: 'amountField',
            touch: false,
            persistentSubmitErrors: false,
          },
          payload: amountToSync.toString(),
        });
        return;
      }
      if (amountToSync >= 25000 && amountToSync <= 1000000) {
        yield put({
          type: '@@redux-form/BLUR',
          meta: {
            form: 'amount',
            field: 'amount',
            touch: false,
            persistentSubmitErrors: false,
          },
          payload: amountToSync,
        });
      }

      if (amountToSync < 25000) {
        yield put({
          type: '@@redux-form/BLUR',
          meta: {
            form: 'amount',
            field: 'amountField',
            touch: false,
            persistentSubmitErrors: false,
          },
          payload: '25000',
        });
      } else if (amountToSync > 1000000) {
        yield put({
          type: '@@redux-form/BLUR',
          meta: {
            form: 'amount',
            field: 'amountField',
            touch: false,
            persistentSubmitErrors: false,
          },
          payload: '1000000',
        });
      }
    }
  }
}

function* syncTextfieldValueToSliderField(action) {
  if (action.meta && action.meta.form === 'amount') {
    if (action.meta.field === 'amount') {
      yield put({
        type: '@@redux-form/BLUR',
        meta: {
          form: 'amount',
          field: 'amountField',
          touch: false,
          persistentSubmitErrors: false,
        },
        payload: action.payload,
      });
    } else if (action.meta.field === 'amountField') {
      const amountToSync = (action.payload && parseInt(action.payload.replace(/[^\d]/g, ''), 10)) || 0;
      if (amountToSync >= 25000 && amountToSync <= 1000000) {
        yield put({
          type: '@@redux-form/BLUR',
          meta: {
            form: 'amount',
            field: 'amount',
            touch: false,
            persistentSubmitErrors: false,
          },
          payload: amountToSync,
        });
      }

      if (amountToSync <= 25000) {
        yield put({
          type: '@@redux-form/BLUR',
          meta: {
            form: 'amount',
            field: 'amount',
            touch: false,
            persistentSubmitErrors: false,
          },
          payload: 25000,
        });
      }

      // if (amountToSync === 0) {
      //   yield put({
      //     type: '@@redux-form/BLUR',
      //     meta: {
      //       form: 'amount',
      //       field: 'amountField',
      //       touch: false,
      //       persistentSubmitErrors: false,
      //     },
      //     payload: '',
      //   });
      // }
    }
  }
}
// function* setTextAmountWithinOffset() {
  // if (action.meta && action.meta.form === 'amount' && action.meta.field === 'amountField') {
  //   const amountToSync = (action.payload && parseInt(action.payload.toString().replace('₹', '').replace(/,/g, ''), 10)) || 0;
  //   if (amountToSync < 50000) {
  //     yield put({
  //       type: '@@redux-form/CHANGE',
  //       meta: {
  //         form: 'amount',
  //         field: 'amountField',
  //         touch: false,
  //         persistentSubmitErrors: false,
  //       },
  //       payload: '50000',
  //     });
  //   } else if (amountToSync > 500000) {
  //     yield put({
  //       type: '@@redux-form/CHANGE',
  //       meta: {
  //         form: 'amount',
  //         field: 'amountField',
  //         touch: false,
  //         persistentSubmitErrors: false,
  //       },
  //       payload: '500000',
  //     });
  //   }
  // }
// }


// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery('@@redux-form/BLUR', syncSliderValueToAmountTextField);
  yield takeEvery('@@redux-form/CHANGE', syncTextfieldValueToSliderField);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
