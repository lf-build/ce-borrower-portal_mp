
import { fromJS } from 'immutable';
import amountPageReducer from '../reducer';

describe('amountPageReducer', () => {
  it('returns the initial state', () => {
    expect(amountPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
