/*
 * AmountPage Messages
 *
 * This contains all the text for the AmountPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AmountPage.header',
    defaultMessage: 'How much do you need?',
  },
});
