/*
 *
 * AmountPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
// import Slider from 'material-ui/Slider';
// import { FormattedNumber } from 'react-intl';

import { createStructuredSelector } from 'reselect';
import makeSelectAmountPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';
import Slider from '../../components/RequiredSlider';
import makeAppSelect from '../App/selectors';
import { LoanAmountIcon } from '../../components/Icons';
import { moneyMask } from '../../components/MaskedTextField/masks';

export class AmountPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    document.getElementById(Array.from(document.getElementsByName('amountField')[0].children).filter((c) => c.tagName === 'INPUT')[0].id).blur();
  }
  render() {
    const { App: { reviewPage } } = this.props;
    const removeAmountField = () => ({ amountField: undefined });
    return (
      <LoanStep simulateCurrentRoute={reviewPage} nextButtonName={reviewPage && 'Go to review'} noBack payloadGenerator={removeAmountField} name={'amount'} title={messages.header}>
        <div >
          <LoanAmountIcon style={{ marginBottom: '50px' }} />
          <Slider name="amount" sliderStyle={{ marginBottom: '0px' }} />
          <span style={{ color: '#A9A0A5', float: 'left', marginTop: '-50px' }} > ₹ 25,000 </span>
          <span style={{ color: '#A9A0A5', float: 'right', marginTop: '-50px' }} > ₹ 10,00,000 </span>
          {/* <FormattedNumber {...{ value: 123555, style: 'currency', currency: 'INR', minimumFractionDigits: 0 }} />*/}
          <div className={'row'}>
            <div className={'amount-label-div'}>
              <label
                htmlFor="amountField"
                className="prefix"
                style={{ paddingRight: '4px' }}
              >₹</label>
            </div>
            <div className={'amount-textfield-div'}>
              <TextField
                focus
                name="amountField"
                hintText="Enter Amount"
                inputStyle={{ textAlign: 'center' }}
                fullWidth
                defaultValue={25000}
                extraValidators={[(amt) => {
                  const amount = parseInt(amt.toString().replace('₹', '').replace(/,/g, ''), 10) || 0;

                  if (amount >= 25000 && amount <= 1000000) {
                    return undefined;
                  }
                  return 'Please enter an amount between ₹25,000 and ₹10,00,000';
                }]}
                mask={moneyMask}
                floatingLabelText="Amount"
                floatingLabelStyle={{ display: 'none' }}
              />
            </div>

          </div>
        </div>
      </LoanStep>
    );
  }
}

AmountPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  App: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  AmountPage: makeSelectAmountPage(),
  App: makeAppSelect(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AmountPage);
