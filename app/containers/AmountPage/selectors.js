import { createSelector } from 'reselect';

/**
 * Direct selector to the amountPage state domain
 */
const selectAmountPageDomain = () => (state) => state.get('amountPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AmountPage
 */

const makeSelectAmountPage = () => createSelector(
  selectAmountPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectAmountPage;
export {
  selectAmountPageDomain,
};
