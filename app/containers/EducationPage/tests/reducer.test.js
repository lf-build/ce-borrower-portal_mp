
import { fromJS } from 'immutable';
import educationPageReducer from '../reducer';

describe('educationPageReducer', () => {
  it('returns the initial state', () => {
    expect(educationPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
