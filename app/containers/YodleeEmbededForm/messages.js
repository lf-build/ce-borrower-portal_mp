/*
 * YodleeEmbededForm Messages
 *
 * This contains all the text for the YodleeEmbededForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.YodleeEmbededForm.header',
    defaultMessage: 'This is YodleeEmbededForm container !',
  },
});
