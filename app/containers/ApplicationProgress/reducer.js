/*
 *
 * ApplicationProgress reducer
 *
 */
import { LOCATION_CHANGE } from 'react-router-redux';
import { fromJS } from 'immutable';
// import {
//   DEFAULT_ACTION,
// } from './constants';

const initialState = fromJS({
  mlevel1: 0,
  mlevel2: 0,
  level1: 0,
  level2: 0,
  level3: 0,
});

const maxLevel = (c, m) => {
  if (c.level1 > m.level1) {
    return c;
  } else if (c.level1 === m.level1) {
    if (c.level2 >= m.level2) {
      return c;
    }
  }
  return m;
};

const setLv = (state, l1, l2, l3) => {
  const m = maxLevel({ level1: l1, level2: l2, level3: l3 }, {
    level1: state.get('mlevel1'),
    level2: state.get('mlevel2'),
  });
  return state.set('mlevel1', m.level1)
          .set('mlevel2', m.level2)
          .set('level1', l1)
          .set('level2', l2)
          .set('level3', l3);
};
function applicationProgressReducer(state = initialState, action) {
  if (action.type === LOCATION_CHANGE) {
    if (action.payload.action === 'POP') {
      return state;
    }
    const [id, route] = action.payload.pathname.split('/').filter((entry) => entry);
  // If no route, we dont need to track the stapper state.
    if (!(id && route)) {
      return state;
    }


    switch (route) {
      case 'amount':
        return setLv(state, 0, 0, 0);
      case 'reason':
        return setLv(state, 0, 1, 0);
      case 'basic-information':
        return setLv(state, 1, 0, 0);
      case 'mobile':
        return setLv(state, 1, 1, 0);
      case 'otp':
        return setLv(state, 1, 1, 0);
      case 'create-account':
        return setLv(state, 1, 1, 0);
      case 'work-details':
        return setLv(state, 1, 2, 0);
      case 'residence-expense-details':
        return setLv(state, 1, 3, 0);
      case 'credit-card-expense-details':
        return setLv(state, 1, 3, 1);
      case 'other-expense-details':
        return setLv(state, 1, 3, 2);
      case 'personal-details':
        return setLv(state, 1, 4, 0);
      case 'review-application':
        return setLv(state, 1, 5, 0);
      case 'initial-offer':
        return setLv(state, 1, 8, 0);
      case 'application-rejected':
        return setLv(state, 1, 9, 0);
      case 'banking':
        return setLv(state, 2, 0, 0);
      case 'social':
        return setLv(state, 2, 1, 0);
      case 'education':
        return setLv(state, 2, 2, 0);
      case 'employment':
        return setLv(state, 2, 3, 0);
      case 'final-offer':
        return setLv(state, 3, 0, 0);
      case 'kyc':
        return setLv(state, 3, 0, 0);
      case 'application-loan-agreement':
        return setLv(state, 3, 1, 0);
      default:
        return setLv(state, 0, 0, 0);
    }
  }
  return state;
}

export default applicationProgressReducer;
