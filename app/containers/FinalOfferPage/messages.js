/*
 * FinalOfferPage Messages
 *
 * This contains all the text for the FinalOfferPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.FinalOfferPage.header',
    defaultMessage: 'Your loan is ready for disbursement!',
  },
  loadingHeader: {
    id: 'app.containers.ReviewApplicationPage.loadingHeader',
    defaultMessage: 'Determining your credit eligibility',
  },
  loadingSubHeader: {
    id: 'app.containers.ReviewApplicationPage.loadingSubHeader',
    defaultMessage: 'Creating your loan offer! This may take a few minutes.',
  },
});
