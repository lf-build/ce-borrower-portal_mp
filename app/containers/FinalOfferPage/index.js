/*
 *
 * FinalOfferPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Checkbox from 'material-ui/Checkbox';
import RadioButtonChecked from 'material-ui/svg-icons/toggle/radio-button-checked';
import RadioButtonUnChecked from 'material-ui/svg-icons/toggle/radio-button-unchecked';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';
import styled from 'styled-components';
import InitialOfferNavigationPanel from '../../components/InitialOfferNavigationPanel';

import {
  offerSelected as offerSelectedActionCreator,
  generateFinalOfferRequest as initiateGenerateFinalOfferActionCreator,
} from './actions';
import makeSelectFinalOfferPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import { HourglassIcon } from '../../components/Icons';

const CongratulationText = styled.p`
    // font-size: 18px;
    // font-weight: 400;
    margin: 20px 0 20px;
    text-align: center;
    font-size: 19px;
    line-height: 27px;
    font-weight: 100;
    letter-spacing: 0.025em;
`;

const shortDescription = () => <p> Please select a loan option </p>;

export class FinalOfferPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    const { FinalOfferPage: FinalOfferPageDomain } = this.props;
    const { offerPending, loading } = FinalOfferPageDomain;

    if (offerPending) {
      if (!loading) {
        this.props.initiateGenerateFinalOffer();
      }
    }
  }

  render() {
    const { FinalOfferPage: FinalOfferPageDomain, offerSelected } = this.props;

    const styles = {
      RadioButtonStyle: {
        marginRight: 0,
        marginTop: '15px',
      },
    };

    const formatNumber = (text) => this.props.intl.formatNumber(text, {
      style: 'currency',
      currency: 'INR',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    });

    const { offer, offerPending, loading, error } = FinalOfferPageDomain;
    if (error || (offer && (offer.status === 'rejected' || offer.status === 'failed'))) {
      return <h1> Offer generation Failed </h1>;
    }
    if (offerPending) {
      if (!loading) {
        return <h1> Need to initiate offer generation </h1>;
      }
      return (<LoanStep name={'final-offer'} title={messages.loadingHeader} navigationPanel={() => <span />}>
        <HourglassIcon style={{ transform: 'scale(1.5)' }} />
        <div className="spinner">
          <div className="bounce1" style={{ backgroundColor: '#79cdd5' }}></div>
          <div className="bounce2" style={{ backgroundColor: '#79cdd5' }}></div>
          <div className="bounce3" style={{ backgroundColor: '#79cdd5' }}></div>
        </div>
        <CongratulationText>
          <FormattedMessage {...messages.loadingSubHeader} />
        </CongratulationText>
      </LoanStep>);
    }

    const { finalOffers } = offer;
    // const [firstOffer] = finalOffers;
    const [selectedOffer] = finalOffers.filter((o) => o.isSelected);
    const handleOfferSelected = (event) => {
      if (finalOffers) {
        const index = finalOffers.findIndex((x) => x.offerId === event.target.value);
        offerSelected(index);
      }
    };
    const buildPayload = () => ({ selectedOfferId: selectedOffer.offerId });
    if (finalOffers[0].manualReview) {
      return <span />;
    }

    const offerFilter = (o) => (process.env.OFFER_MODE && process.env.OFFER_MODE.toLowerCase() === 'manual') ? o.isManualOffer : true;
    return (
      <LoanStep
        noBorder
        name={'final-offer'}
        title={messages.header}
        navigationPanel={(props) => <InitialOfferNavigationPanel offerType={'final'} nextDisabled={selectedOffer === undefined} {...props} />}
        noBack
        payloadGenerator={buildPayload}
        ShortDescription={shortDescription}
        noPadding
      >
        { /* <MoneyChangeIcon />

        <LoanAmountLabel>Loan Amount</LoanAmountLabel>
        <LoanAmountFigure>{formatNumber(firstOffer.finalOfferAmount)}</LoanAmountFigure>

        <div style={{ textAlign: 'center', fontSize: 14, fontWeight: 300, marginBottom: 30 }}>Please select a loan option</div> */}

        <div className={'col-sm-12 res-pad'} style={{ padding: '5px' }}>

          {finalOffers.filter(offerFilter).map((row, index) => (

            <div key={`div-${index}`} className="radio-loan">
              <div className="rad-bx">
                { /* <input type="radio" className="rad" name="radio1" onChange={handleOfferSelected} /> */}
                <Checkbox
                  checkedIcon={<RadioButtonChecked />}
                  uncheckedIcon={<RadioButtonUnChecked />}
                  label=""
                  value={row.offerId}
                  name={'loanOptions'}
                  className={'rad'}
                  onCheck={handleOfferSelected}
                  style={styles.RadioButtonStyle}
                  checked={row.isSelected}
                  // style={styles.checkbox}
                />
              </div>
              <div className="loan-box" key={row.offerId}>
                <div className="main-head">
                  <h6>Loan {index + 1}</h6>
                  <span>{formatNumber(row.finalOfferAmount)}</span>
                </div>
                <ul className="loan-items">
                  <li><h6>DURATION</h6> <span>{row.loantenure} Months</span> </li>
                  <li><h6>EMI</h6> <span>{formatNumber(row.emi)}</span> </li>
                  <li><h6>PROCESSING FEE <br />(Incl. Service Tax)</h6> <span>{formatNumber(row.processingFee)}</span> </li>
                </ul>
              </div>
            </div>
                ))}

        </div>

      </LoanStep>
    );
  }
}

FinalOfferPage.propTypes = {
  offerSelected: React.PropTypes.func.isRequired,
  initiateGenerateFinalOffer: React.PropTypes.func.isRequired,
  intl: intlShape.isRequired,
  FinalOfferPage: React.PropTypes.object,

};

const mapStateToProps = createStructuredSelector({
  FinalOfferPage: makeSelectFinalOfferPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    offerSelected: (id) => dispatch(offerSelectedActionCreator(id)),
    initiateGenerateFinalOffer: () => dispatch(initiateGenerateFinalOfferActionCreator()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(FinalOfferPage));
