/**
*
* FinalOfferPageNavigationPanel
*
*/

import React from 'react';
// import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';

export class FinalOfferPageNavigationPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { isLoadingOrSubmiting, handleGoBack, handleGoNext, nextDisabled } = this.props;
    return (
      <div className="col-xs-9 col-xs-offset-2">
        <div className="col-xs-6">
          <RaisedButton style={{ width: '70%' }} disabled={isLoadingOrSubmiting} onTouchTap={handleGoBack} primary label="Back" />
        </div>
        <div className={'col-xs-6'}>
          <RaisedButton style={{ width: '70%' }} disabled={isLoadingOrSubmiting || nextDisabled} onTouchTap={handleGoNext} primary label="Next" />
        </div>
      </div>);
  }
}

FinalOfferPageNavigationPanel.propTypes = {
  isLoadingOrSubmiting: React.PropTypes.bool.isRequired,
  handleGoBack: React.PropTypes.func.isRequired,
  handleGoNext: React.PropTypes.func.isRequired,
  nextDisabled: React.PropTypes.bool.isRequired,
};

export default FinalOfferPageNavigationPanel;
