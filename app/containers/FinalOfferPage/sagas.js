import { takeEvery, put } from 'redux-saga/effects';
// import { makeSelectLocationState } from 'containers/App/selectors';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { navigationHelper } from '../NavigationHelper/helper';
import {
  FINAL_OFFER_GENERATE_REQUEST,
  FINAL_OFFER_GENERATE_REQUEST_FULFILLED,
} from './constants';
import * as offerActions from './actions';
function* generateOffer() {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'offer',
    command: 'generate-final',
  }, {
    tag: 'finalOffer',
  }))) {
    return;
  }
  yield put(offerActions.generateFinalOfferRequestStarted());
  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('finalOffer');
  if (valid) {
    // if (valid.meta.body.status && valid.meta.body.status === 'rejected') {
    //   yield put(offerActions.generateFinalOfferRequestFailed(valid.meta.body));
    // } else {
    yield put(offerActions.generateFinalOfferRequestFulfilled(valid.meta.body));
    // }
  } else {
    yield put(offerActions.generateFinalOfferRequestFailed(valid.meta.body));
  }

  yield put(offerActions.generateFinalOfferRequestEnded());
}

export function* navigateToProperPage(action) {
  if (action.meta.offer && action.meta.offer.status && action.meta.offer.status === 'failed') {
    yield navigationHelper.goTo({
      name: 'finalOfferPage',
      params: {
      },
    }, 'internalServerError');
    return;
  }
  if (action.meta.offer && action.meta.offer.status && action.meta.offer.status === 'rejected') {
    yield navigationHelper.goTo({
      name: 'finalOfferPage',
      params: {
      },
    }, 'applicationRejectedPage');
    return;
  }
  if (action.meta.offer.finalOffers && action.meta.offer.finalOffers[0].manualReview === true) {
    yield navigationHelper.goTo({
      name: 'finalOfferPage',
      params: {
      },
    }, 'applicationUnderReviewPage');
  }
}
// Individual exports for testing
export function* defaultSaga() {
  yield takeEvery(FINAL_OFFER_GENERATE_REQUEST, generateOffer);
  yield takeEvery(FINAL_OFFER_GENERATE_REQUEST_FULFILLED, navigateToProperPage);
  // See example in containers/HomePage/sagas.js
}

// All sagas to be loaded
export default [
  defaultSaga,
];
