
import { fromJS } from 'immutable';
import creditCardExpenseDetailsPageReducer from '../reducer';

describe('creditCardExpenseDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(creditCardExpenseDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
