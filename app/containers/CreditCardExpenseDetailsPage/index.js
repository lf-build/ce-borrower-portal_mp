/*
 *
 * CreditCardExpenseDetailsPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
// import { createStructuredSelector } from 'reselect';
import { RadioButton } from 'material-ui/RadioButton';
import { Field, formValueSelector } from 'redux-form/immutable';
import RadioButtonGroup from 'redux-form-material-ui/lib/RadioButtonGroup';
import styled from 'styled-components';

// import makeSelectCreditCardExpenseDetailsPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';
import ExpenseDetailsTabs from '../../components/ExpenseDetailsTabs';
import { moneyMask, normalizeMoney } from '../../components/MaskedTextField/masks';
import makeAppSelect from '../App/selectors';

const shortDescription = () => <p> Let us quickly calculate your monthly expenses to find the most comfortable EMI for you. </p>;
const CreditCardExpensePageFormSelector = formValueSelector('credit-card-expense-details');

export class CreditCardExpenseDetailsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const styles = {
      row: {
        marginBottom: '20px',
        color: '#4e4e4e',
      },
      row2: {
        marginTop: '10px',
        marginBottom: '12px',
        color: '#4e4e4e',
        paddingTop: '25px',
      },
      radioButton: {
        marginTop: '1px',
        display: 'block',
        height: 40,
        borderRight: '1px solid #dadada',
        background: '#fff none repeat scroll 0 0',
        border: '1px solid #dadada',
      },
      radioButtonSelected: {
        marginTop: '1px',
        display: 'block',
        height: 40,
        background: 'rgba(121, 205, 213, 0.15) none repeat scroll 0 0',
        border: '1px solid #79cdd5',
        boxShadow: '0 0',
        color: '#79cdd5',
      },
      iconStyle: {
        marginRight: '2px',
        display: 'none',
      },
      labelStyle: {
        color: '#303030',
        fontSize: '14px',
        lineHeight: 3,
        textAlign: 'center',
        width: 'calc(100% - 0px)',
      },
      labelStyleSelected: {
        color: '#79cdd5',
        fontSize: '14px',
        lineHeight: 3,
        textAlign: 'center',
        width: 'calc(100% - 0px)',
      },
      borderRadiusLeft: {
        borderBottomLeftRadius: 4,
        borderTopLeftRadius: 4,
      },
      borderRadiusRight: {
        borderBottomRightRadius: 4,
        borderTopRightRadius: 4,
      },
      spanLabel: {
        lineHeight: 'normal',
        transition: 'all 240ms ease 0s',
        zIndex: 10,
        opacity: 1,
        color: 'rgba(0, 0, 0, 0.298039)',
        fontSize: 10,
        fontWeight: 400,
        letterSpacing: '0.025em',
        marginBottom: 0,
        textTransform: 'capitalize',
      },
    };


    const { hasCreditCard, reviewPage } = this.props;

    const normalizeMoneyPayload = ({ creditCardOutstanding }) => ({
      creditCardOutstanding: creditCardOutstanding && normalizeMoney(creditCardOutstanding),
    });
    const CreditCardIconStyle = styled.label``;

    return (
      <LoanStep
        simulateCurrentRoute={reviewPage}
        nextButtonName={reviewPage && 'Go to review'}
        noBack={reviewPage !== undefined}
        payloadGenerator={normalizeMoneyPayload}
        name={'credit-card-expense-details'}
        title={messages.header}
        ShortDescription={shortDescription}
        noPadding
      >
        <ExpenseDetailsTabs tab={2} />

        <div style={{ padding: '25px' }}>
          <div style={styles.row2}>
            <span style={styles.spanLabel}> Do you have a credit card?</span>
            <Field name="hasCreditCard" style={{ display: 'flex' }} validate={(value) => value ? undefined : 'Please let us know if you have Credit Card'} component={RadioButtonGroup}>
              <RadioButton
                value={'true'}
                label="Yes"
                checkedIcon={<CreditCardIconStyle />}
                uncheckedIcon={<CreditCardIconStyle />}
                labelStyle={hasCreditCard === 'true' ? { ...styles.labelStyleSelected, ...styles.borderRadiusLeft } : { ...styles.labelStyle, ...styles.borderRadiusLeft }}
                iconStyle={styles.iconStyle}
                style={hasCreditCard === 'true' ? { ...styles.radioButtonSelected, ...styles.borderRadiusLeft } : { ...styles.radioButton, ...styles.borderRadiusLeft }}
              />
              <RadioButton
                value={'false'}
                labelStyle={hasCreditCard === 'false' ? { ...styles.labelStyleSelected, ...styles.borderRadiusRight } : { ...styles.labelStyle, ...styles.borderRadiusRight }}
                checkedIcon={<CreditCardIconStyle />}
                uncheckedIcon={<CreditCardIconStyle />}
                label="No"
                iconStyle={styles.iconStyle}
                style={hasCreditCard === 'false' ? { ...styles.radioButtonSelected, ...styles.borderRadiusRight } : { ...styles.radioButton, ...styles.borderRadiusRight }}
              />
            </Field>
          </div>

          <div className={hasCreditCard === 'true' ? 'row' : 'hidden'}>
            <div className={'amount-label-div'}>
              <label
                htmlFor="creditCardOutstanding"
                className="prefix"
                style={{ paddingRight: '4px' }}
              >₹</label>
            </div>
            <div className={'amount-textfield-div'}>
              <TextField
                className={hasCreditCard === 'true' ? '' : 'hidden'}
                name="creditCardOutstanding"
                fullWidth
                minLen={1}
                hintText=""
                floatingLabelText="Credit Card Balances"
                mask={moneyMask}
                optional={hasCreditCard === 'false'}
                floatingLabelFocusStyle={{ width: '321px' }}
                floatingLabelShrinkStyle={{ width: '321px' }}
                extraValidators={[(value) => {
                  if (!(hasCreditCard === 'true')) {
                    return undefined;
                  }
                  if (/[^\d]/.test(value)) {
                    return 'Please enter a valid number';
                  }
                  if (value.length > 6) {
                    return 'Entered amount is out of range';
                  }
                  return undefined;
                }]}
              />
            </div>

          </div>
        </div>

      </LoanStep>
    );
  }
}

CreditCardExpenseDetailsPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  hasCreditCard: PropTypes.string,
  reviewPage: PropTypes.string,
};

// const mapStateToProps = createStructuredSelector({
//   CreditCardExpenseDetailsPage: makeSelectCreditCardExpenseDetailsPage(),
// });

const mapStateToProps = (state) => ({
  ...({ hasCreditCard: CreditCardExpensePageFormSelector(state, 'hasCreditCard') }),
  ...makeAppSelect()(state),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreditCardExpenseDetailsPage);
