import { takeEvery, put, select } from 'redux-saga/effects';

function* resetCCOutstanding(action) {
  if (action.meta && action.meta.form === 'credit-card-expense-details') {
    // query the state using the exported selector
    const creditCard = yield select();
    const { 'credit-card-expense-details': creditCardState } = creditCard.toJS().form;

    // when page is loaded for the first time
    if (!creditCardState || !creditCardState.values) {
      return;
    }

    const { values: { hasCreditCard, creditCardOutstanding } } = creditCardState;

    if (hasCreditCard && hasCreditCard === 'false') {
      if (creditCardOutstanding && (/[^\d]/.test(creditCardOutstanding) || (creditCardOutstanding.match(/^[0-9]+$/i) && creditCardOutstanding.length > 6))) {
        if (action.meta.field === 'hasCreditCard') {
          yield put({
            type: '@@redux-form/CHANGE',
            meta: {
              form: 'credit-card-expense-details',
              field: 'creditCardOutstanding',
              touch: false,
              persistentSubmitErrors: false,
            },
            payload: '',
          });
        }
      }
    }
  }
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery('@@redux-form/CHANGE', resetCCOutstanding);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
