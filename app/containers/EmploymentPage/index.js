/*
 *
 * EmploymentPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
// import { Field, getFormValues } from 'redux-form/immutable';
import { Field } from 'redux-form/immutable';
import SelectField from 'redux-form-material-ui/lib/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { createStructuredSelector } from 'reselect';
import MaterialTextField from 'material-ui/TextField';

import makeSelectEmploymentPage from './selectors';
import messages from './messages';
import TextField from '../../components/RequiredTextField';
import LoanStep from '../../components/LoanStepForm';
import VerificationProcess from '../../components/VerificationProcess';
import AddressDetails from '../../components/AddressDetails';
// import FileDropZone from '../../components/FileDropZone';
import {
  fileRecieved as fileRecievedActionCreator,
  fileRemoved as fileRemovedActionCreator,
  fetchPendingDocumentListRequest as fetchPendingDocumentListRequestActionCreator,
} from '../KycPage/actions';
// import { getForm26ASStatusRequest as getForm26ASStatusRequestActionCreator } from './actions';
import { getVerificationEmailSentStatusRequest as getVerificationEmailSentStatusRequestActionCreator } from './actions';
import { inputMask } from '../../components/MaskedTextField/masks';
import makeSelectApp from '../App/selectors';
import { getBasicAppInfoRequest } from '../InitialOfferPage/actions';
import YearMonth from '../../components/YearMonth';
// validation functions
const required = (value) => value == null ? 'Please tell us what your Total Work Experience is' : undefined;
const BlockLabel = styled.h3`
`;
// const employerName = 'Sigma Info';

const EmploymentInfo = styled.p`  
    font-size: 12px;
    color: #000;
    line-height: 22px;
    text-align: left;
`;

export class EmploymentPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const {
      fetchPendingDocuments,
      verificationEmailSentStatus,
      App,
      getBasicAppInfo } = this.props;
    // const { form26ASStatus } = this.props;
    if (fetchPendingDocuments) {
      fetchPendingDocuments();
    }

    // here if Form 26AS is available the Form 26AS upload option will not be shown.
    // form26ASStatus();

    // here we check if verification email is sent on the official mail id or not.
    verificationEmailSentStatus();

    // get basic app info
    if (!App || !App.application) {
      getBasicAppInfo();
    }
  }
  render() {
    const styles = {
      row: {
        marginBottom: '20px',
        color: '#4e4e4e',
      },
      row2: {
        marginTop: '10px',
        // marginBottom: '12px',
        color: '#4e4e4e',
      },
      radioButton: {
        marginTop: '1px',
        display: 'block',
        height: 40,
        // borderRight: '1px solid #dadada',
        background: '#fff none repeat scroll 0 0',
        border: '1px solid #dadada',
      },
      radioButtonSelected: {
        marginTop: '1px',
        display: 'block',
        height: 40,
        background: 'rgba(121, 205, 213, 0.15) none repeat scroll 0 0',
        border: '1px solid #79cdd5',
        boxShadow: '0 0',
        color: '#79cdd5',
      },
      iconStyle: {
        marginRight: '2px',
        color: '#4e4e4e',
        display: 'none',
      },
      labelStyle: {
        color: '#303030',
        fontSize: '14px',
        lineHeight: 3,
        textAlign: 'center',
        width: 'calc(100% - 0px)',
      },
      labelStyleSelected: {
        color: '#79cdd5',
        fontSize: '14px',
        lineHeight: 3,
        textAlign: 'center',
        width: 'calc(100% - 0px)',
      },
      borderRadiusLeft: {
        borderBottomLeftRadius: 4,
        borderTopLeftRadius: 4,
      },
      borderRadiusRight: {
        borderBottomRightRadius: 4,
        borderTopRightRadius: 4,
      },
      spanLabel: {
        lineHeight: 'normal',
        transition: 'all 240ms ease 0s',
        zIndex: 10,
        opacity: 1,
        color: 'rgba(0, 0, 0, 0.298039)',
        fontSize: 10,
        fontWeight: 400,
        letterSpacing: '0.025em',
        marginBottom: 0,
        textTransform: 'capitalize',
      },
      ymError: {
        position: 'relative',
        bottom: '15px',
        fontSize: '12px',
        lineHeight: '15px',
        color: 'rgb(255, 117, 106)',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        textAlign: 'left',
        marginTop: '6px',
      },
    };
    const { App } = this.props;
    if (!App) {
      // return <h1> Not Ready </h1>;
      return (
        <LoanStep
          name={'employment'}
          title={messages.header}
          navigationPanel={() => <span />}
          noPadding
        >
          <VerificationProcess step={1} />
          <div style={{ display: !App ? 'block' : 'none', marginTop: '10%', marginBottom: '10%' }} className="spinner">
            <div className="bounce1" style={{ backgroundColor: '#79cdd5' }}></div>
            <div className="bounce2" style={{ backgroundColor: '#79cdd5' }}></div>
            <div className="bounce3" style={{ backgroundColor: '#79cdd5' }}></div>
          </div>
        </LoanStep>
      );
    }

    const { application: { employerName } } = App;


    const employer = employerName;

    // const { fileRecieved, fileRemoved } = this.props;
    const { files, pendingDocuments, isEmailSent, errorYMMsg, employementYear } = this.props.EmploymentPage;
    // const { isForm26ASAvailable } = this.props.EmploymentPage;
    const pendingDocumentNames = (pendingDocuments || []).map((d) => d.documentName[0]);
    // const handleFileDropped = (stipulation) => (e) => fileRecieved({ ...e, ...stipulation, ...{ stipulationType: stipulation.documentName[0], documentName: undefined } });
    // const handleFileRemoved = (stipulation) => () => fileRemoved({ ...{ stipulationType: stipulation.documentName[0], documentName: undefined } });

    const buildPayload = () => ({
      documents: files && files.Form26AS ? Object.keys(files)
        .filter((doc) => pendingDocumentNames.includes(doc))
        .map((stipulationType) => files[stipulationType])
        .filter((doc) => doc)
        .map((doc) => doc[0]) : undefined,
    });
    return (
      <LoanStep payloadGenerator={buildPayload} name={'employment'} title={messages.header} disableNext={errorYMMsg && true} noPadding>
        <VerificationProcess step={3} />
        <div style={{ padding: 25 }}>
          <MaterialTextField
            fullWidth
            hintText="Employer"
            floatingLabelText="Employer"
            value={typeof employer === 'string' ? employer : employer.name}
            readOnly
          />
          <TextField
            name="designation"
            fullWidth
            hintText="Designation/Title"
            floatingLabelText="Designation/Title"
            mask={inputMask}
            enforceMasking
          />

          <Field
            name="workExperience"
            fullWidth
            floatingLabelText={'Total Work Experience'}
            floatingLabelStyle={{ fontSize: '12px' }}
            floatingLabelFocusStyle={{ fontSize: '14px' }}
            floatingLabelShrinkStyle={{ fontSize: '14px' }}
            component={SelectField}
            hintText="Total Work Experience"
            validate={required}
            selectedMenuItemStyle={{ color: '#79cdd5' }}
            errorStyle={{ fontSize: 12, color: '#ff756a', textAlign: 'left', lineHeight: '15px', marginTop: '6px' }}
          >
            <MenuItem value={1} primaryText="Less than 1 year" />
            <MenuItem value={2} primaryText="2 years" />
            <MenuItem value={3} primaryText="3 years" />
            <MenuItem value={4} primaryText="4 years" />
            <MenuItem value={5} primaryText="5 years" />
            <MenuItem value={6} primaryText="6 years" />
            <MenuItem value={7} primaryText="7 years" />
            <MenuItem value={8} primaryText="8 years" />
            <MenuItem value={9} primaryText="9 years" />
            <MenuItem value={10} primaryText="More than 9 years" />
          </Field>

          <BlockLabel>Employer Address</BlockLabel>
          <AddressDetails
            addressLine1="employerAddress.addressLine1"
            addressLine2="employerAddress.addressLine2"
            locality="employerAddress.locality"
            pincode="employerAddress.pinCode"
            city="employerAddress.city"
            state="employerAddress.state"
          />

          <BlockLabel>How many years have you worked at your current company?</BlockLabel>
          <YearMonth addYear={employementYear} name="employementYear" />
          <div className={errorYMMsg ? '' : 'hidden'} style={styles.ymError}>
            {errorYMMsg}
          </div>

          {/* {pendingDocuments && pendingDocuments.filter((doc) => doc.documentName.length && doc.documentName[0] === 'Form26AS').length ? */}
          {isEmailSent ?
            <div>
              <div className={'manual-label'}>Employer Verification via Email</div>
              <EmploymentInfo>
                A verification email has been sent to your official email ID. Please click on verification link in it.
                {/* <span className={isForm26ASAvailable ? 'hidden' : ''}> Alternatively, you can upload your Form 26AS to complete verification.</span>*/}
              </EmploymentInfo>
              {/* <div style={{ marginTop: '40px', marginBottom: '0px' }} className={isForm26ASAvailable ? 'hidden' : 'manual-label'}>Form-26 AS File Upload</div>*/}
            </div>
            : <span />}
          {/* <div>
            { pendingDocuments && !isForm26ASAvailable ? pendingDocuments.filter((doc) => doc.documentName.length && doc.documentName[0] === 'Form26AS')
                            .map((doc) => (
                              <FileDropZone
                                key={doc.documentName[0]}
                                onDrop={handleFileDropped(doc)}
                                onRemove={handleFileRemoved(doc)}
                                files={files[doc.documentName[0]] || []}
                                message={
                                  <span style={{ fontSize: '13px', color: 'black' }}>
                                    <strong style={{ fontWeight: 600 }}>Choose a file </strong>or Drag it here
                                  </span>}
                                accept={'application/pdf,image/*'}
                              />
                            ))

                          : <span />
          }


          </div>*/}
        </div>
      </LoanStep>
    );
  }
}

EmploymentPage.propTypes = {
  // form: PropTypes.object,
  EmploymentPage: PropTypes.object,
  // fileRecieved: PropTypes.func,
  // fileRemoved: PropTypes.func,
  fetchPendingDocuments: PropTypes.func,
  // form26ASStatus: PropTypes.func,
  verificationEmailSentStatus: PropTypes.func,
  App: PropTypes.object,
  getBasicAppInfo: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  // form: getFormValues('work-details'),
  EmploymentPage: makeSelectEmploymentPage(),
  App: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    fileRecieved: ({ file, dataUrl, type, factName, methodName, stipulationType }) => dispatch(fileRecievedActionCreator({ file, dataUrl, type, factName, methodName, stipulationType })),
    fileRemoved: ({ stipulationType }) => dispatch(fileRemovedActionCreator({ stipulationType })),
    fetchPendingDocuments: () => dispatch(fetchPendingDocumentListRequestActionCreator()),
    // form26ASStatus: () => dispatch(getForm26ASStatusRequestActionCreator()),
    verificationEmailSentStatus: () => dispatch(getVerificationEmailSentStatusRequestActionCreator()),
    getBasicAppInfo: () => dispatch(getBasicAppInfoRequest()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmploymentPage);
