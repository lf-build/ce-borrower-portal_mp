
import { fromJS } from 'immutable';
import employmentPageReducer from '../reducer';

describe('employmentPageReducer', () => {
  it('returns the initial state', () => {
    expect(employmentPageReducer(undefined, {})).toEqual(fromJS({
      files: {},
      pendingDocs: [],
    }));
  });
});
