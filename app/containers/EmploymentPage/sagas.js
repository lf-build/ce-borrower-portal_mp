import { takeEvery, put, select } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import {
  GET_FORM26AS_STATUS_REQUEST,
  GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST,
  SET_EMP_YM_ERROR_MSG,
} from './constants';
import * as employmentActions from './actions';

function* fetchCityStateFromZip(action) {
  let section = '';
  if (action.meta.form === 'employment' && action.meta.field === 'employerAddress.pinCode') {
    section = 'employerAddress';
  } else {
    return;
  }

  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'lookup',
    command: 'pincode-details',
  },
    {
      tag: action.payload,
      payload: {
        pincode: action.payload,
      },
    }))) {
    return;
  }

  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(action.payload);
  if (valid) {
    yield put({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'employment',
        field: `${section}.city`,
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: valid.meta.body.cities[0],
    });
    yield put({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'employment',
        field: `${section}.state`,
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: valid.meta.body.state,
    });
  } else {
    yield put({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'employment',
        field: `${section}.city`,
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: '',
    });
    yield put({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'employment',
        field: `${section}.state`,
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: '',
    });

    yield put({
      type: '@@redux-form/UPDATE_SYNC_ERRORS',
      meta: {
        form: 'employment',
      },
      payload: {
        syncErrors: {
          employerAddress: {
            pinCode: 'This Pin code is currently not serviceable.',
          },
        },
      },
    });
  }
}

function* getForm26ASStatus() {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'verify',
    command: 'get-form26as-status',
  },
    {
      tag: 'form26as',
    }))) {
    return;
  }

  yield put(employmentActions.getForm26ASStatusRequestStarted());

  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('form26as');

  if (valid) {
    yield put(employmentActions.getForm26ASStatusRequestFulfilled(valid.meta.body.body));
  } else {
    yield put(employmentActions.getForm26ASStatusRequestFailed('error occurred while fetching Form26 AS status'));
  }

  yield put(employmentActions.getForm26ASStatusRequestEnded());
}

function* getVerificationEmailSentStatus() {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'verify',
    command: 'get-verification-email-status',
  },
    {
      tag: 'verification-email',
    }))) {
    return;
  }

  yield put(employmentActions.getVerificationEmailSentStatusRequestStarted());

  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('verification-email');

  if (valid) {
    yield put(employmentActions.getVerificationEmailSentStatusRequestFulfilled(valid.meta.body));
  } else {
    yield put(employmentActions.getVerificationEmailSentStatusRequestFailed('error occurred while fetching Verification Email Sent status'));
  }

  yield put(employmentActions.getVerificationEmailSentStatusRequestEnded());
}

function checkIfValidYM(year) {
  const errorMsg = (year === '0')
    ? 'year can not be zero'
    : undefined;

  if (errorMsg !== undefined) {
    return errorMsg;
  }

  return errorMsg;
}

function* validateYearMonth(action) {

  const persDetails = yield select();
  const { employment } = persDetails.toJS().form;
  const { values: { employementYear } } = employment;
  if (employementYear) {
    const errorMsg = checkIfValidYM(employementYear);
    if (errorMsg) {
      yield put({
        type: SET_EMP_YM_ERROR_MSG,
        meta: {
          form: 'employment',
        },
        payload: {
          errorMsg,
        },
      });
    } else {
      yield put({
        type: SET_EMP_YM_ERROR_MSG,
        meta: {
          form: 'employment',
        },
        payload: {
          errorMsg: undefined,
        },
      });
    }
  } else {
    yield put({
      type: SET_EMP_YM_ERROR_MSG,
      meta: {
        form: 'employment',
      },
      payload: {
        errorMsg: ' ',
      },
    });
  }
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery('@@redux-form/BLUR', fetchCityStateFromZip);
  yield takeEvery(GET_FORM26AS_STATUS_REQUEST, getForm26ASStatus);
  yield takeEvery(GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST, getVerificationEmailSentStatus);
  yield takeEvery('@@redux-form/CHANGE', validateYearMonth);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
