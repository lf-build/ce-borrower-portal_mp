import { createSelector } from 'reselect';

/**
 * Direct selector to the applicationUnderReview state domain
 */
const selectApplicationUnderReviewDomain = () => (state) => state.get('applicationUnderReview');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ApplicationUnderReview
 */

const makeSelectApplicationUnderReview = () => createSelector(
  selectApplicationUnderReviewDomain(),
  (substate) => substate.toJS()
);

export default makeSelectApplicationUnderReview;
export {
  selectApplicationUnderReviewDomain,
};
