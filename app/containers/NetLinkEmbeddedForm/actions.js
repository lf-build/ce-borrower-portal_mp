/*
 *
 * NetLinkEmbeddedForm actions
 *
 */

import {
  DEFAULT_ACTION,
  GET_NET_LINK_STATUS_REQUEST,
  GET_NET_LINK_STATUS_REQUEST_STARTED,
  GET_NET_LINK_STATUS_REQUEST_FULFILLED,
  GET_NET_LINK_STATUS_REQUEST_FAILED,
  GET_NET_LINK_STATUS_REQUEST_ENDED,
  NET_LINK_FORM_REQUEST,
  NET_LINK_FORM_REQUEST_STARTED,
  NET_LINK_FORM_REQUEST_FULFILLED,
  NET_LINK_FORM_REQUEST_FAILED,
  NET_LINK_FORM_REQUEST_ENDED,
  UPDATE_STATUS_CALLED_FIRST_TIME,
  SHOW_HIDE_NET_BANKING_LOADER,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function netlinkStatusRequest() {
  return {
    type: GET_NET_LINK_STATUS_REQUEST,
  };
}

export function netlinkStatusRequestStarted() {
  return {
    type: GET_NET_LINK_STATUS_REQUEST_STARTED,
  };
}

export function netlinkStatusRequestFulfilled(form) {
  return {
    type: GET_NET_LINK_STATUS_REQUEST_FULFILLED,
    meta: {
      form,
    },
  };
}

export function netlinkStatusRequestFailed(error) {
  return {
    type: GET_NET_LINK_STATUS_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function netlinkStatusRequestEnded() {
  return {
    type: GET_NET_LINK_STATUS_REQUEST_ENDED,
  };
}

export function netlinkFormRequest() {
  return {
    type: NET_LINK_FORM_REQUEST,
  };
}

export function netlinkFormRequestStarted() {
  return {
    type: NET_LINK_FORM_REQUEST_STARTED,
  };
}

export function netlinkFormRequestFulfilled(form) {
  return {
    type: NET_LINK_FORM_REQUEST_FULFILLED,
    meta: {
      form,
    },
  };
}

export function netlinkFormRequestFailed(error) {
  return {
    type: NET_LINK_FORM_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function netlinkFormRequestEnded() {
  return {
    type: NET_LINK_FORM_REQUEST_ENDED,
  };
}

export function updateStatusCalledFirstTime(status) {
  return {
    type: UPDATE_STATUS_CALLED_FIRST_TIME,
    payload: {
      status,
    },
  };
}

export function netBankingLoader(value) {
  return {
    type: SHOW_HIDE_NET_BANKING_LOADER,
    payload: {
      value,
    },
  };
}
