
import { fromJS } from 'immutable';
import netLinkEmbeddedFormReducer from '../reducer';

describe('netLinkEmbeddedFormReducer', () => {
  it('returns the initial state', () => {
    expect(netLinkEmbeddedFormReducer(undefined, {})).toEqual(fromJS({
      // statusCalledFirstTime: true,
    }));
  });
});
