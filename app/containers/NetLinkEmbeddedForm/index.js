/*
 *
 * NetLinkEmbeddedForm
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import makeSelectNetLinkEmbeddedForm from './selectors';
// import messages from './messages';
import {
  netlinkStatusRequest as netlinkStatusRequestActionCreator,
  netBankingLoader as netBankingLoaderActionCreator } from './actions';
import makeSelectApp from '../App/selectors';
import netBankingLoader from './net-banking-loader.gif';

export class NetLinkEmbeddedForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { getNetlinkStatus } = this.props;
    getNetlinkStatus();
  }
  render() {
    const styles = {
      loaderDiv: {
        height: '100vh', textAlign: 'center', display: 'table-cell', width: '100vw', verticalAlign: 'middle',
      },
      netBankingImg: {
        width: '80%',
        maxWidth: '650px',
      },
      loaderImg: {
        maxWidth: '450px', width: '100%',
      },
      h2Style: {
        fontSize: '24px',
        lineHeight: '42px',
        backgroundColor: 'rgba(0,0,0,0)',
        color: '#444!important',
        fontWeight: 'lighter',
      },
      bStyle: {
        fontWeight: 600,
      },
    };

    // from state getting value of statusCalledFirstTime, which will help in deciding which loader to show.
    const { App, setLoader } = this.props;
    let showLoader = false;
    if (App) {
      const { showNetbankingLoader } = App;

      // doing this so a delay of 7 seconds can show the Net Banking loader.
      if (showNetbankingLoader) {
        showLoader = showNetbankingLoader;
        setTimeout(() => setLoader(false), 7000);
      } else {
        showLoader = showNetbankingLoader;
      }
    }

    return (
      <div>
        <div id="net-banking-loader" className={showLoader ? '' : 'hidden'} style={styles.loaderDiv}>
          <h2 style={styles.h2Style}>You will now be redirected to a <b style={styles.bStyle}>secure gateway</b> to submit your bank statement via your <b style={styles.bStyle}>netbanking account</b>.</h2>
          <img alt="Loading animation" src={netBankingLoader} style={styles.netBankingImg} />
        </div>
        <div id="cancel-loader" className={showLoader ? 'hidden' : ''} style={styles.loaderDiv}>
          <img alt="Loading animation" src="https://crex-cdn.s3.amazonaws.com/loaderqbera.gif" style={styles.loaderImg} />
        </div>
        <div id="netLinkPlaceHolder">
        </div>
      </div>
    );
  }
}

NetLinkEmbeddedForm.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  getNetlinkStatus: PropTypes.func,
  App: PropTypes.object,
  setLoader: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  NetLinkEmbeddedForm: makeSelectNetLinkEmbeddedForm(),
  App: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getNetlinkStatus: () => dispatch(netlinkStatusRequestActionCreator()),
    setLoader: (value) => dispatch(netBankingLoaderActionCreator(value)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NetLinkEmbeddedForm);
