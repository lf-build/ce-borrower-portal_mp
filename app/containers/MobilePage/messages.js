/*
 * MobilePage Messages
 *
 * This contains all the text for the MobilePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.MobilePage.header',
    defaultMessage: 'Mobile number verification',
  },
});
