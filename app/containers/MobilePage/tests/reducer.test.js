
import { fromJS } from 'immutable';
import mobilePageReducer from '../reducer';

describe('mobilePageReducer', () => {
  it('returns the initial state', () => {
    expect(mobilePageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
