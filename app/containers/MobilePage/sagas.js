import { takeEvery, put } from 'redux-saga/effects';
import { showLoginButton } from './actions';

function* showLoginBtn(action) {
  const { meta: { error: { status: { code } } } } = action;
  if (code) {
    yield put(showLoginButton(true));
  }
}

function* hideLoginBtn(action) {
  const { payload: { syncErrors: { mobileNumber } } } = action;
  if (!mobileNumber) {
    yield put(showLoginButton(false));
  }
}
// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery('app/uplink/EXECUTE_UPLINK_REQUEST_FAILED_mobile', showLoginBtn);
  yield takeEvery('@@redux-form/UPDATE_SYNC_ERRORS', hideLoginBtn);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
