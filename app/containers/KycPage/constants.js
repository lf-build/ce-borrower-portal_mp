/*
 *
 * KycPage constants
 *
 */

export const DEFAULT_ACTION = 'app/KycPage/DEFAULT_ACTION';
export const FILE_RECIEVED = 'app/KycPage/FILE_RECIEVED';
export const FILE_REMOVED = 'app/KycPage/FILE_REMOVED';
export const FETCH_PENDING_DOCUMENT_LIST_REQUEST = 'app/KycPage/FETCH_PENDING_DOCUMENT_LIST_REQUEST';
export const FETCH_PENDING_DOCUMENT_LIST_REQUEST_STARTED = 'app/KycPage/FETCH_PENDING_DOCUMENT_LIST_REQUEST_STARTED';
export const FETCH_PENDING_DOCUMENT_LIST_REQUEST_FUILFILLED = 'app/KycPage/FETCH_PENDING_DOCUMENT_LIST_REQUEST_FUILFILLED';
export const FETCH_PENDING_DOCUMENT_LIST_REQUEST_FAILED = 'app/KycPage/FETCH_PENDING_DOCUMENT_LIST_REQUEST_FAILED';
export const FETCH_PENDING_DOCUMENT_LIST_REQUEST_ENDED = 'app/KycPage/FETCH_PENDING_DOCUMENT_LIST_REQUEST_ENDED';
