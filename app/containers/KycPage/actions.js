/*
 *
 * KycPage actions
 *
 */

import {
  FILE_RECIEVED,
  FILE_REMOVED,
  DEFAULT_ACTION,
  FETCH_PENDING_DOCUMENT_LIST_REQUEST,
  FETCH_PENDING_DOCUMENT_LIST_REQUEST_STARTED,
  FETCH_PENDING_DOCUMENT_LIST_REQUEST_FUILFILLED,
  FETCH_PENDING_DOCUMENT_LIST_REQUEST_FAILED,
  FETCH_PENDING_DOCUMENT_LIST_REQUEST_ENDED,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function fileRemoved({ stipulationType }) {
  return {
    type: FILE_REMOVED,
    meta: {
      stipulationType,
    },
    payload: {
      stipulationType,
    },
  };
}

export function fileRecieved({ file, dataUrl, type, stipulationType, factName, methodName }) {
  return {
    type: FILE_RECIEVED,
    meta: {
      stipulationType,
    },
    payload: {
      file,
      dataUrl,
      type,
      stipulationType,
      factName,
      methodName,
    },
  };
}

export function fetchPendingDocumentListRequest() {
  return {
    type: FETCH_PENDING_DOCUMENT_LIST_REQUEST,
  };
}
export function fetchPendingDocumentListRequestStarted() {
  return {
    type: FETCH_PENDING_DOCUMENT_LIST_REQUEST_STARTED,
  };
}
export function fetchPendingDocumentListRequestFuilfilled(meta) {
  return {
    type: FETCH_PENDING_DOCUMENT_LIST_REQUEST_FUILFILLED,
    meta,
  };
}
export function fetchPendingDocumentListRequestFailed(meta) {
  return {
    type: FETCH_PENDING_DOCUMENT_LIST_REQUEST_FAILED,
    meta,
  };
}
export function fetchPendingDocumentListRequestEnded() {
  return {
    type: FETCH_PENDING_DOCUMENT_LIST_REQUEST_ENDED,
  };
}
