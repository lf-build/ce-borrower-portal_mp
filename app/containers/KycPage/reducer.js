/*
 *
 * KycPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  FILE_RECIEVED,
  FETCH_PENDING_DOCUMENT_LIST_REQUEST_FUILFILLED,
} from './constants';

const initialState = fromJS({
  files: {},
  pendingDocs: [],
});

function kycPageReducer(state = initialState, action) {
  switch (action.type) {
    case FILE_RECIEVED:
      return state.set('files', { ...state.get('files'), ...{ [action.meta.stipulationType]: [action.payload] } });
    case FETCH_PENDING_DOCUMENT_LIST_REQUEST_FUILFILLED:
      return state.set('pendingDocuments', action.meta);
    default:
      return state;
  }
}

export default kycPageReducer;
