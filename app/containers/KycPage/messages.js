/*
 * KycPage Messages
 *
 * This contains all the text for the KycPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.KycPage.header',
    defaultMessage: 'KYC',
  },
  Form26AS: {
    id: 'app.containers.KycPage.Form26AS',
    defaultMessage: 'Form-26 AS',
  },
  PANCard: {
    id: 'app.containers.KycPage.PANCard',
    defaultMessage: 'PAN Card',
  },
  AadhaarCard: {
    id: 'app.containers.KycPage.AadhaarCard',
    defaultMessage: 'Aadhaar Card',
  },
  SignedDocuments: {
    id: 'app.containers.KycPage.SignedDocuments',
    defaultMessage: 'Signed Documents',
  },
  KYCAddress: {
    id: 'app.containers.KycPage.KYCAddress',
    defaultMessage: 'Voter\'s ID Card / Valid Driving License / Valid Passport / UID (Aadhaar) / Any other document notified by the central government',
  },
});
