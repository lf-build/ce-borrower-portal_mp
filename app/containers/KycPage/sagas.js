import { takeLatest, put } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import * as action from './actions';
import { FETCH_PENDING_DOCUMENT_LIST_REQUEST } from './constants';

function* fetchPendingDocumentList() {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'kyc',
    command: 'pending-documents',
  }, {
    tag: 'pending-documents',
  }))) {
    return;
  }
  yield put(action.fetchPendingDocumentListRequestStarted());
  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('pending-documents');
  if (valid) {
    yield put(action.fetchPendingDocumentListRequestFuilfilled(valid.meta.body));
  } else {
    yield put(action.fetchPendingDocumentListRequestFailed(valid.meta.body));
  }

  yield put(action.fetchPendingDocumentListRequestEnded());
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeLatest(FETCH_PENDING_DOCUMENT_LIST_REQUEST, fetchPendingDocumentList);
  // See example in containers/HomePage/sagas.js
}

// All sagas to be loaded
export default [
  defaultSaga,
];
