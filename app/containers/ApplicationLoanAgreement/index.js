/*
 *
 * ApplicationLoanAgreement
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
// import RaisedButton from 'material-ui/RaisedButton';

import { createStructuredSelector } from 'reselect';
import makeSelectApplicationLoanAgreement from './selectors';
import messages from './messages';
import LoanStep from '../LoanStep';
import { CheckPassIcon } from '../../components/Icons';
import makeSelectApp from '../App/selectors';
import Referral from '../../components/Referral';
import { getBasicAppInfoRequest } from '../InitialOfferPage/actions';

const CongratulationText = styled.p`
    // font-size: 18px;
    // font-weight: 400;
    margin: 0 0 20px;
    text-align: center;
    font-size: 19px;
    line-height: 27px;
    font-weight: 100;
    letter-spacing: 0.025em;
`;

// const style = {
//   textAlign: 'center',
//   borderColor: '#79cdd5',
//   boxShadow: '0 0',
//   fontSize: '14px',
//   color: 'rgb(255, 255, 255)',
// };

export class ApplicationLoanAgreement extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { App: { application } } = this.props;
    if (!application) {
      this.props.dispatch(getBasicAppInfoRequest());
    }
  }

  render() {
    const { App: { application } } = this.props;

    if (!application) {
      return <span />;
    }

    const { firstName, mobileNumber } = application;

    return (
      <LoanStep navigationPanel={() => <Referral name={firstName} mobile={mobileNumber} />} title={messages.header}>
        <CheckPassIcon />
        <CongratulationText />
        <CongratulationText>
          {/* Your loan agreement has been sent to your personal e-mail ID and we will be calling you shortly to schedule an appointment for documentation.*/}
          {/* Great, your loan offer has been selected. We will contact you shortly to complete the documentation for quick disbursal of your loan.*/}
          {/* Congratulations! We have received your application and we will contact you within 4 business hours. */}
          Congratulations! Our executive will contact you shortly to complete the documentation.
        </CongratulationText>

        {/*
          <div className={'col-xs-2'} />
          <RaisedButton style={style} className={'col-xs-8'} primary >
            Click Here To See Loan Agreement
          </RaisedButton>
          <div className={'col-xs-2'} />
        */}
      </LoanStep>
    );
  }
}

ApplicationLoanAgreement.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  App: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ApplicationLoanAgreement: makeSelectApplicationLoanAgreement(),
  App: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationLoanAgreement);
