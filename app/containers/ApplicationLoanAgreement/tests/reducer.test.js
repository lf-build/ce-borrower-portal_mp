
import { fromJS } from 'immutable';
import applicationLoanAgreementReducer from '../reducer';

describe('applicationLoanAgreementReducer', () => {
  it('returns the initial state', () => {
    expect(applicationLoanAgreementReducer(undefined, {})).toEqual(fromJS({}));
  });
});
