import { createSelector } from 'reselect';

/**
 * Direct selector to the applicationLoanAgreement state domain
 */
const selectApplicationLoanAgreementDomain = () => (state) => state.get('applicationLoanAgreement');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ApplicationLoanAgreement
 */

const makeSelectApplicationLoanAgreement = () => createSelector(
  selectApplicationLoanAgreementDomain(),
  (substate) => substate.toJS()
);

export default makeSelectApplicationLoanAgreement;
export {
  selectApplicationLoanAgreementDomain,
};
