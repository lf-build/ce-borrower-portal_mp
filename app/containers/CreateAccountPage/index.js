/*
 *
 * CreateAccountPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import makeSelectCreateAccountPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';
import HintText from '../../components/HintText';
import makeSelectWelcomePage from '../WelcomePage/selectors';

export class CreateAccountPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { WelcomePage: { utmSource } } = this.props;
    return (
      <LoanStep
        noBack
        ShortDescription={() => <p style={{ textAlign: 'center' }}> Please select a password to save and retrieve your application. Your mobile number would be the username </p>}
        name={'create-account'}
        title={messages.header}
        payloadGenerator={() => ({
          utmId: utmSource || undefined,
        })}
      >
        <TextField
          id="password"
          name="password"
          fullWidth
          hintText="Password"
          floatingLabelText="Password"
          type="password"
          minLen={6}
          maxLen={15}
        />
        <HintText message={'Your password must be between 6-15 characters'} />
        <TextField
          id="confirmPassword"
          name="confirmPassword"
          fullWidth
          hintText="Confirm Password"
          floatingLabelText="Confirm Password"
          type="password"
          minLen={6}
          maxLen={15}
          optional
          extraValidators={[() => document.getElementById('password').value === document.getElementById('confirmPassword').value ? undefined : 'Passwords do not match']}
        />

      </LoanStep>
    );
  }
}

CreateAccountPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  WelcomePage: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  CreateAccountPage: makeSelectCreateAccountPage(),
  WelcomePage: makeSelectWelcomePage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateAccountPage);
