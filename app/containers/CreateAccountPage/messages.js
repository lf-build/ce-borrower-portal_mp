/*
 * CreateAccountPage Messages
 *
 * This contains all the text for the CreateAccountPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.CreateAccountPage.header',
    defaultMessage: 'Create new account',
  },
});
