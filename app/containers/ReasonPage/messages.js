/*
 * ReasonPage Messages
 *
 * This contains all the text for the ReasonPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ReasonPage.header',
    defaultMessage: 'What do you need it for?',
  },
});
