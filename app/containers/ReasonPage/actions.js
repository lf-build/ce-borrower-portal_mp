/*
 *
 * ReasonPage actions
 *
 */

import {
  DEFAULT_ACTION,
  SET_PURPOSE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function setPurpose({ purpose }) {
  return {
    type: SET_PURPOSE,
    purpose,
  };
}
