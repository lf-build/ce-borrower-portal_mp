/*
 *
 * ReasonPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ReasonPage/DEFAULT_ACTION';
export const SET_PURPOSE = 'app/ReasonPage/SET_PURPOSE';
export const REASON_NOT_SELECTED = 'app/ReasonPage/REASON_NOT_SELECTED';
export const REASON_SELECTED = 'app/ReasonPage/REASON_SELECTED';
