/*
 *
 * ReasonPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SET_PURPOSE,
  REASON_NOT_SELECTED,
  REASON_SELECTED,
} from './constants';

const initialState = fromJS({});

function reasonPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SET_PURPOSE:
      return state
        .set('selected', action.purpose)
        .set('reasonNotSelected', false);
    case REASON_NOT_SELECTED:
      return state.set('reasonNotSelected', true);
    case REASON_SELECTED:
      return state.set('reasonNotSelected', false);
    default:
      return state;
  }
}

export default reasonPageReducer;
