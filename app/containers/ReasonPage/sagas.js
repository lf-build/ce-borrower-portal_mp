import { takeEvery, put } from 'redux-saga/effects';
import { REASON_NOT_SELECTED, REASON_SELECTED } from './constants';

function* handleSaveFailed() {
  yield put({
    type: REASON_NOT_SELECTED,
  });
}

function* handleSaveSucceed() {
  yield put({
    type: REASON_SELECTED,
  });
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery('@@redux-form/SET_SUBMIT_FAILED_reason', handleSaveFailed);
  yield takeEvery('@@redux-form/SET_SUBMIT_SUCCEEDED_reason', handleSaveSucceed);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
