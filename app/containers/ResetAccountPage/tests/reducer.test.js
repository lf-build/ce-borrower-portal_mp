
import { fromJS } from 'immutable';
import resetAccountPageReducer from '../reducer';

describe('resetAccountPageReducer', () => {
  it('returns the initial state', () => {
    expect(resetAccountPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
