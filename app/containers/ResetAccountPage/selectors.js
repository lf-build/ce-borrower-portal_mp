import { createSelector } from 'reselect';

/**
 * Direct selector to the resetAccountPage state domain
 */
const selectResetAccountPageDomain = () => (state) => state.get('resetAccountPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ResetAccountPage
 */

const makeSelectResetAccountPage = () => createSelector(
  selectResetAccountPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectResetAccountPage;
export {
  selectResetAccountPageDomain,
};
