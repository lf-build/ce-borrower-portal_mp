/*
 * ResetAccountPage Messages
 *
 * This contains all the text for the ResetAccountPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ResetAccountPage.header',
    defaultMessage: 'Reset account',
  },
});
