/*
 *
 * WelcomePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  UTM_PARAM_FOUND,
} from './constants';

const initialState = fromJS({
  utmSource: undefined,
});

function welcomePageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case UTM_PARAM_FOUND:
      return state.set('utmSource', action.payload.utmSource)
                  .set('utmMedium', action.payload.utmMedium)
                  .set('gclid', action.payload.gclid);
    default:
      return state;
  }
}

export default welcomePageReducer;
