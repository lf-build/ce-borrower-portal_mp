/*
 *
 * WelcomePage constants
 *
 */

export const DEFAULT_ACTION = 'app/WelcomePage/DEFAULT_ACTION';
export const UTM_PARAM_FOUND = 'app/WelcomePage/UTM_PARAM_FOUND';
