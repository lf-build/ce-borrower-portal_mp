/*
 *
 * WelcomePage actions
 *
 */

import {
  DEFAULT_ACTION,
  UTM_PARAM_FOUND,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function utmParamFound(payload) {
  return {
    type: UTM_PARAM_FOUND,
    payload,
  };
}
