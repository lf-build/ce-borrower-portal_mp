/*
 *
 * WelcomePage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import uplink from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import makeSelectWelcomePage from './selectors';
// import messages from './messages';
import { navigationHelper } from '../NavigationHelper/helper';
import { utmParamFound as utmParamFoundActionCreator } from './actions';

export class WelcomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    const { params: { utm_source: utmSource, utm_medium: utmMedium, gclid }, setUtmParams, resetState } = this.props;

    const init = () => {
      // when user sign-in from qbera website need to do this logic.
      if (utmSource && utmSource === 'login') {
        navigationHelper.goTo({ name: 'welcome' }, 'signInPage');
        return;
      }
      if (!localStorage.getItem(uplink.config.host)) {
        navigationHelper.goTo({ name: 'welcome' }, 'amountPage');
      }
    };
    // Call api and do regestration.
    const makeCall = () => {
      const loginRequest = utmSource && utmSource === 'login';
      const delay = loginRequest ? 2000 : 6000;

      if (loginRequest) {
        resetState();
        localStorage.clear();
      }
      if (utmSource && utmSource !== 'direct' && utmSource !== 'login') {
        const payload = {
          utmSource,
          utmMedium,
          gclid,
        };
        setUtmParams(payload);
      }
      setTimeout(init, delay);
    };
    makeCall();
  }

  render() {
    return (
      /* <div id="welcomeSpinner" className="spinner">
        <div className="bounce1" style={{ backgroundColor: '#79cdd5' }}></div>
        <div className="bounce2" style={{ backgroundColor: '#79cdd5' }}></div>
        <div className="bounce3" style={{ backgroundColor: '#79cdd5' }}></div>
      </div>*/
      <div style={{ height: '100vh', textAlign: 'center', display: 'table-cell', width: '100vw', verticalAlign: 'middle' }}>
        <img alt="Loading animation" src="https://crex-cdn.s3.amazonaws.com/loaderqbera.gif" style={{ maxWidth: '450px', width: '100%' }} />
      </div>
    );
  }
}

WelcomePage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  params: PropTypes.object,
  setUtmParams: PropTypes.func,
  resetState: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  WelcomePage: makeSelectWelcomePage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    setUtmParams: (payload) => dispatch(utmParamFoundActionCreator(payload)),
    resetState: () => dispatch({
      type: 'RESET',
      state: undefined,
    }),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WelcomePage));
