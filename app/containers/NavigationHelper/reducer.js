/*
 *
 * NavigationHelper reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DISABLE_ROUTE,
} from './constants';

const initialState = fromJS({});

function navigationHelperReducer(state = initialState, action) {
  switch (action.type) {
    case DISABLE_ROUTE:
      return state.set(action.route.name, false);
    default:
      return state;
  }
}

export default navigationHelperReducer;
