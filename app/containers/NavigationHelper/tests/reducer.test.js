
import { fromJS } from 'immutable';
import navigationHelperReducer from '../reducer';

describe('navigationHelperReducer', () => {
  it('returns the initial state', () => {
    expect(navigationHelperReducer(undefined, {})).toEqual(fromJS({}));
  });
});
