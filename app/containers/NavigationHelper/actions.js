/*
 *
 * NavigationHelper actions
 *
 */

import {
  DISABLE_ROUTE,
  NAVIGATE_NEXT_REQUEST,
} from './constants';

export function disableRoute(route) {
  return {
    type: DISABLE_ROUTE,
    route,
  };
}

export function navigateNext(route) {
  return {
    type: NAVIGATE_NEXT_REQUEST,
    meta: { route },
  };
}

