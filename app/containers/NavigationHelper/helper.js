import { browserHistory } from 'react-router';

const next = {
  amountPage: 'reasonPage',
  reasonPage: 'basicInformationPage',
  basicInformationPage: 'mobilePage',
  mobilePage: 'otpPage',
  otpPage: 'workDetailsPage',
  // createAccountPage: 'workDetailsPage',
  workDetailsPage: 'residenceExpenseDetailsPage',
  residenceExpenseDetailsPage: 'personalDetailsPage',
  // creditCardExpenseDetailsPage: 'otherExpenseDetailsPage',
  // otherExpenseDetailsPage: 'personalDetailsPage',
  personalDetailsPage: 'reviewApplicationPage',
  reviewApplicationPage: 'initialOfferPage',
  initialOfferPage: 'bankingPage',
  bankingPage: 'socialPage',
  educationPage: 'employmentPage',
  employmentPage: 'finalOfferPage',
  socialPage: 'educationPage',
  finalOfferPage: 'applicationLoanAgreementPage',
  // kycPage: 'applicationLoanAgreementPage',
  signInPage: 'signInPage',
  forgotPasswordPage: 'signInPage',
  resetAccountPage: 'signInPage',
  verifyEmailPage: 'signInPage',
};

const previous = {
  reasonPage: 'amountPage',
  basicInformationPage: 'reasonPage',
  mobilePage: 'basicInformationPage',
  otpPage: 'mobilePage',
  createAccountPage: 'mobilePage',
  workDetailsPage: undefined,
  residenceExpenseDetailsPage: 'workDetailsPage',
  // creditCardExpenseDetailsPage: 'residenceExpenseDetailsPage',
  // otherExpenseDetailsPage: 'creditCardExpenseDetailsPage',
  personalDetailsPage: 'residenceExpenseDetailsPage',
  reviewApplicationPage: undefined,
  initialOfferPage: undefined,
  bankingPage: undefined,
  educationPage: undefined,
  employmentPage: 'educationPage',
  socialPage: undefined,
  finalOfferPage: undefined,
  kycPage: undefined,
};

const pathByNames = {
  amountPage: 'application/amount',
  reasonPage: 'application/reason',
  basicInformationPage: 'application/basic-information',
  mobilePage: 'application/mobile',
  otpPage: 'application/otp',
  createAccountPage: 'application/create-account',
  workDetailsPage: 'application/work-details',
  residenceExpenseDetailsPage: 'application/residence-expense-details',
  creditCardExpenseDetailsPage: 'application/credit-card-expense-details',
  otherExpenseDetailsPage: 'application/other-expense-details',
  personalDetailsPage: 'application/personal-details',
  reviewApplicationPage: 'application/review-application',
  initialOfferPage: 'application/initial-offer',
  bankingPage: 'application/banking',
  educationPage: 'application/education',
  employmentPage: 'application/employment',
  socialPage: 'application/social',
  finalOfferPage: 'application/final-offer',
  kycPage: 'application/kyc',
  applicationSubmittedPage: 'application/application-submitted',
  applicationUnderReviewPage: 'application/application-under-review',
  verificationFailedPage: 'application/verification-failed',
  applicationLoanAgreementPage: 'application/application-loan-agreement',
  applicationRejectedPage: 'application/application-rejected',
  signInPage: 'user/sign-in',
  forgotPasswordPage: 'user/forgot-password',
  resetAccountPage: 'user/reset-account',
  verifyEmailPage: 'user/verify-email',
  internalServerError: 'user/request-failed',
  notInterestedPage: 'application/not-interested',
  welcomePage: 'user/welcome',
  netlinkPage: 'user/netlink',
  cibilFailed: 'user/cibil-failed',
  applicationExpired: 'application/application-expired',
  consentDeclined: 'application/consent-declined',
};

export const navigationHelper = {
  goNext(route) {
    this.goTo(route, next[route.name]);
  },
  // goTo({ name, params: { id } }, nextRouteName) {
  goTo({ name }, nextRouteName) { // , preserveHistory = true) {
    if (nextRouteName && name !== nextRouteName) {
      // if (preserveHistory && false) {
      //   browserHistory.push(`/${pathByNames[nextRouteName]}`);
      // } else {
      browserHistory.replace(`/${pathByNames[nextRouteName]}`);
      // }
    }
  },
  /*
  * The browserHistory.replace supresses creation of browser history
  * hence we need to go to specific page when user clicks back.
  */
  goBack(currentRoute) {
    this.goTo({ name: currentRoute }, previous[currentRoute]);
    // browserHistory.goBack();
  },
};
