/*
 * ForgotPasswordPage Messages
 *
 * This contains all the text for the ForgotPasswordPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ForgotPasswordPage.header',
    defaultMessage: 'Having trouble signing in?',
  },
});
