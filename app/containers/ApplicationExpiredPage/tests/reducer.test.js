
import { fromJS } from 'immutable';
import applicationExpiredPageReducer from '../reducer';

describe('applicationExpiredPageReducer', () => {
  it('returns the initial state', () => {
    expect(applicationExpiredPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
