import { createSelector } from 'reselect';

/**
 * Direct selector to the applicationExpiredPage state domain
 */
const selectApplicationExpiredPageDomain = () => (state) => state.get('applicationExpiredPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ApplicationExpiredPage
 */

const makeSelectApplicationExpiredPage = () => createSelector(
  selectApplicationExpiredPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectApplicationExpiredPage;
export {
  selectApplicationExpiredPageDomain,
};
