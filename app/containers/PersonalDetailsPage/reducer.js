import { fromJS } from 'immutable';
import {
  // DEFAUL/T_ACTION,
  ADDRESS_NOT_SELECTED,
  ADDRESS_SELECTED,
} from './constants';

const initialState = fromJS({});

function personalDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case ADDRESS_NOT_SELECTED:
      return state.set('addressNotSelected', true);
    case ADDRESS_SELECTED:
      return state.set('addressNotSelected', false);
    default:
      return state;
  }
}

export default personalDetailsPageReducer;