/*
 *
 * PersonalDetailsPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
// import { createStructuredSelector } from 'reselect';
import { RadioButton } from 'material-ui/RadioButton';
import { Field, formValueSelector } from 'redux-form/immutable';
import RadioButtonGroup from 'redux-form-material-ui/lib/RadioButtonGroup';
import styled from 'styled-components';
// import makeSelectPersonalDetailsPage from './selectors';
import makeSelectPersonalDetailsPage from './selectors';
import messages from './messages';
import { addressSelected as addressSelectedActionCreator } from './actions';

import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';

import AddressDetails from '../../components/AddressDetails';
import HintText from '../../components/HintText';
import {
  // dateMask,
  // datePipe,
  aadharMask,
  toUpperPipe,
  // panMask,
  normalizeAadhar,
  alphaNumeric,
} from '../../components/MaskedTextField/masks';

import makeAppSelect from '../App/selectors';
import DateMonthYear from '../../components/DateMonthYear';
import YearMonth from '../../components/YearMonth';

const BlockLabel = styled.h4`
  font-weight: 600;  
  margin-bottom: 1px;
`;
// const required = (value) => value == null ? 'Please select a Residence Type' : undefined;
const errorStyle = {
  bottom: '15px',
  fontSize: '12px',
  lineHeight: '15px',
  color: 'rgb(255, 117, 106)',
  transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
  marginTop: '10px',
};
const personalDetailsSelector = formValueSelector('personal-details');
export class PersonalDetailsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  handleAddressButtonsClicked = () => {
    const { setAddressSelected } = this.props;
    setAddressSelected();
  }
  render() {
    const styles = {
      row: {
        marginBottom: '20px',
        color: '#4e4e4e',
      },
      row2: {
        marginTop: '10px',
        // marginBottom: '12px',
        color: '#4e4e4e',
      },
      radioButton: {
        marginTop: '1px',
        display: 'block',
        height: 40,
        // borderRight: '1px solid #dadada',
        background: '#fff none repeat scroll 0 0',
        border: '1px solid #dadada',
      },
      radioButtonSelected: {
        marginTop: '1px',
        display: 'block',
        height: 40,
        background: 'rgba(121, 205, 213, 0.15) none repeat scroll 0 0',
        border: '1px solid #79cdd5',
        boxShadow: '0 0',
        color: '#79cdd5',
      },
      iconStyle: {
        marginRight: '2px',
        color: '#4e4e4e',
        display: 'none',
      },
      labelStyle: {
        color: '#303030',
        fontSize: '14px',
        lineHeight: 3,
        textAlign: 'center',
        width: 'calc(100% - 0px)',
      },
      labelStyleSelected: {
        color: '#79cdd5',
        fontSize: '14px',
        lineHeight: 3,
        textAlign: 'center',
        width: 'calc(100% - 0px)',
      },
      borderRadiusLeft: {
        borderBottomLeftRadius: 4,
        borderTopLeftRadius: 4,
      },
      borderRadiusRight: {
        borderBottomRightRadius: 4,
        borderTopRightRadius: 4,
      },
      spanLabel: {
        lineHeight: 'normal',
        transition: 'all 240ms ease 0s',
        zIndex: 10,
        opacity: 1,
        color: 'rgba(0, 0, 0, 0.298039)',
        fontSize: 10,
        fontWeight: 400,
        letterSpacing: '0.025em',
        marginBottom: 0,
        textTransform: 'capitalize',
      },
      dobError: {
        position: 'relative',
        bottom: '15px',
        fontSize: '12px',
        lineHeight: '15px',
        color: 'rgb(255, 117, 106)',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        textAlign: 'left',
        marginTop: '6px',
      },
    };

    let { permanentResidentialAddressSameAsCurrent } = this.props;
    const { date, month, year, errorMsg, errorYMMsg, currentAddressYear, addressNotSelected } = this.props;

    // When page is loaded for the first time neither Yes nor No choice should be selected
    // as default, this variable will help in doing that.
    const pageLoadedFirstTime = permanentResidentialAddressSameAsCurrent === undefined;

    permanentResidentialAddressSameAsCurrent = permanentResidentialAddressSameAsCurrent === 'true';

    const { reviewPage } = this.props;
    const AddressIconStyle = styled.label``;

    // check if Date of Birth is valid and also checking if Current Residence Year and Month are selected.
    const disableNextButton = (errorMsg || (!date || !month || !year)) ||
      (errorYMMsg || ((!currentAddressYear)));
    return (
      <LoanStep
        name={'personal-details'}
        title={messages.header}
        simulateCurrentRoute={reviewPage}
        nextButtonName={reviewPage && 'Go to review'}
        noBack={reviewPage !== undefined}
        payloadGenerator={({ aadhaarNumber, panNumber }) => ({
          aadhaarNumber: normalizeAadhar(aadhaarNumber),
          panNumber: panNumber.toUpperCase(),
          dateOfBirth: `${date}/${month}/${year}`,
          ...(permanentResidentialAddressSameAsCurrent ? {
            permanentResidentialAddress: undefined,
          } : {}),
          errorMsg,
        })}
        disableNext={disableNextButton && true}
      >
        <BlockLabel>Identification Details</BlockLabel>
        <span style={styles.spanLabel}>Date Of Birth (DD/MM/YYYY)</span>
        <DateMonthYear date={date} month={month} year={year} />
        <div className={errorMsg ? '' : 'hidden'} style={styles.dobError}>
          {errorMsg}
        </div>
        {/* <TextField
           name="dateOfBirth"
           fullWidth
           hintText="DD/MM/YYYY"
           mask={dateMask}
           pipe={datePipe}
           floatingLabelText="Date Of Birth (DD/MM/YYYY)"
           enforceMasking
           date
           guide
           extraValidators={[(value) => {
            if (value.length >= 8) {
              const initialDob = value.split(/\//);
              const dob = new Date([initialDob[1], initialDob[0], initialDob[2]].join('/'));
              const todaysDate = new Date();
              if (dob.setHours(0, 0, 0, 0) >= todaysDate.setHours(0, 0, 0, 0)) {
                return "The date doesn't look right. Be sure to use your actual date of birth.";
              }
            }
            return undefined;
          }]}
         /> */}
        <TextField
          name="aadhaarNumber"
          fullWidth
          hintText="Aadhaar Number"
          floatingLabelText="Aadhaar Number"
          // minLen={14}
          // maxLen={14}
          mask={aadharMask}
          pipe={toUpperPipe}
          extraValidators={[(value) => value && normalizeAadhar(value).match(/^[a-z0-9]+$/i) ? undefined : 'Aadhar number must be a 12 digit code madeup of numbers and alphabets']}
          inputStyle={{ textTransform: 'uppercase' }}
        />
        {/* <HintText message={"If you don't have an Aadhaar number, please enter zero 12 times (i.e. 000000000000)"} />*/}
        {/* <HintText message={'As per RBI, it is mandatory to quote your Aadhaar Number while applying for loans. If you do not have it then please enter 0 twelve times to proceed with the application. In the meantime, please apply for your Aadhaar Card as soon as possible because we cannot disburse your loan without it.'} /> */}

        <TextField
          name="panNumber"
          fullWidth
          hintText="PAN Number"
          floatingLabelText="PAN Number"
          mask={[/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/]}
          extraValidators={[(value) => value && value.match(/^[A-Z]{3}[P][A-Z][0-9]{4}[A-Z]{1}$/i) ? undefined : 'PAN Number is invalid']}
          // pipe={toUpperPipe}
          inputStyle={{ textTransform: 'uppercase' }}
          enforceMasking
        />

        <BlockLabel>Current Residential Address</BlockLabel>
        <HintText message={'You should be currently living at this address. We may do a residence verification at this address.'} />
        <AddressDetails
          addressLine1="currentResidentialAddress.addressLine1"
          addressLine2="currentResidentialAddress.addressLine2"
          locality="currentResidentialAddress.locality"
          pincode="currentResidentialAddress.pinCode"
          city="currentResidentialAddress.city"
          state="currentResidentialAddress.state"
        />
        <BlockLabel>How many years have you lived in your current residential address?</BlockLabel>
        <YearMonth addYear={currentAddressYear} name="currentAddressYear" />
        <div className={errorYMMsg ? '' : 'hidden'} style={styles.dobError}>
          {errorYMMsg}
        </div>

        <BlockLabel>Permanent Residential Address</BlockLabel>
        <div style={styles.row2}>
          <span style={styles.spanLabel}> Is Your Permanent Address Same As Your Residential Address?</span>
          <Field name="permanentResidentialAddressSameAsCurrent" style={{ display: 'flex' }} validate={(value) => value ? undefined : 'Is Your Address (As Per Your Address Proof) Same As Your Residential Address?'} component={RadioButtonGroup} onChange={() => this.handleAddressButtonsClicked()}>
            <RadioButton
              value={'true'}
              label="Yes"
              checkedIcon={<AddressIconStyle />}
              uncheckedIcon={<AddressIconStyle />}
              labelStyle={!pageLoadedFirstTime && permanentResidentialAddressSameAsCurrent ? { ...styles.labelStyleSelected, ...styles.borderRadiusLeft } : { ...styles.labelStyle, ...styles.borderRadiusLeft }}
              iconStyle={styles.iconStyle}
              style={!pageLoadedFirstTime && permanentResidentialAddressSameAsCurrent ? { ...styles.radioButtonSelected, ...styles.borderRadiusLeft } : { ...styles.radioButton, ...styles.borderRadiusLeft, borderRight: `${pageLoadedFirstTime && !permanentResidentialAddressSameAsCurrent ? '1px solid #dadada' : 'none'}` }}
            />
            <RadioButton
              value={'false'}
              labelStyle={pageLoadedFirstTime || permanentResidentialAddressSameAsCurrent ? { ...styles.labelStyle, ...styles.borderRadiusRight } : { ...styles.labelStyleSelected, ...styles.borderRadiusRight }}
              checkedIcon={<AddressIconStyle />}
              uncheckedIcon={<AddressIconStyle />}
              label="No"
              iconStyle={styles.iconStyle}
              style={pageLoadedFirstTime || permanentResidentialAddressSameAsCurrent ? { ...styles.radioButton, ...styles.borderRadiusRight, borderLeft: 'none' } : { ...styles.radioButtonSelected, ...styles.borderRadiusRight }}
            />
          </Field>
          <span style={{ display: `${addressNotSelected && addressNotSelected === true ? 'block' : 'none'}`, ...errorStyle }}>
            Is Your Address (As Per Your Address Proof) Same As Your Residential Address?
          </span>
        </div>

        <br />
        <AddressDetails
          className={pageLoadedFirstTime || permanentResidentialAddressSameAsCurrent ? 'hidden' : ''}
          optional={permanentResidentialAddressSameAsCurrent}
          addressLine1="permanentResidentialAddress.addressLine1"
          addressLine2="permanentResidentialAddress.addressLine2"
          locality="permanentResidentialAddress.locality"
          pincode="permanentResidentialAddress.pinCode"
          city="permanentResidentialAddress.city"
          state="permanentResidentialAddress.state"
        />

        <TextField
          name="sodexoCode"
          fullWidth
          hintText="Sodexo Code (Optional)"
          floatingLabelText="Sodexo Code (Optional)"
          optional
          mask={alphaNumeric}
          enforceMasking
        />
      </LoanStep>
    );
  }
}

PersonalDetailsPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  permanentResidentialAddressSameAsCurrent: PropTypes.string,
  reviewPage: PropTypes.string,
  date: PropTypes.string,
  month: PropTypes.string,
  year: PropTypes.string,
  currentAddressYear: PropTypes.string,
  errorMsg: PropTypes.string,
  errorYMMsg: PropTypes.string,
  addressNotSelected: PropTypes.bool,
  setAddressSelected: PropTypes.func,
};

// const mapStateToProps = createStructuredSelector({
//   PersonalDetailsPage: makeSelectPersonalDetailsPage(),
// });

const mapStateToProps = (state) => ({
  ...({ permanentResidentialAddressSameAsCurrent: personalDetailsSelector(state, 'permanentResidentialAddressSameAsCurrent') }),
  ...(personalDetailsSelector(state, 'date', 'month', 'year', 'currentAddressYear') || {}),
  ...makeAppSelect()(state),
  ...(makeSelectPersonalDetailsPage()(state) || {}),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    setAddressSelected: () => dispatch(addressSelectedActionCreator()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonalDetailsPage);
