/*
 *
 * PersonalDetailsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/PersonalDetailsPage/DEFAULT_ACTION';
export const SET_DOB_ERROR_MSG = 'app/PersonalDetailsPage/SET_DOB_ERROR_MSG';
export const SET_YM_ERROR_MSG = 'app/PersonalDetailsPage/SET_YM_ERROR_MSG';
export const ADDRESS_NOT_SELECTED = 'app/PersonalDetailsPage/ADDRESS_NOT_SELECTED';
export const ADDRESS_SELECTED = 'app/PersonalDetailsPage/ADDRESS_SELECTED';
