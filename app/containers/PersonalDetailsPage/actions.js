/*
 *
 * PersonalDetailsPage actions
 *
 */

import {
  DEFAULT_ACTION,
  ADDRESS_SELECTED,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function addressSelected() {
  return {
    type: ADDRESS_SELECTED,
  };
}
