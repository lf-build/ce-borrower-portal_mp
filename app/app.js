/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
import 'babel-polyfill';

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import uplink from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import { Provider } from 'react-redux';
import { applyRouterMiddleware, Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { useScroll } from 'react-router-scroll';
import 'sanitize.css/sanitize.css';
import 'assets/bootstrap.grid.css';
import 'assets/style.css';
// import 'assets/roboto.fonts.css';

// Material-ui   - http://www.material-ui.com/#/get-started/usage
// import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';
import {
  cyan500, cyan700,
  pinkA200,
  grey100, grey300, grey400, grey500,
  white, darkBlack, fullBlack,
} from 'material-ui/styles/colors';
import { fade } from 'material-ui/utils/colorManipulator';

// Import root app
import App from 'containers/App';
import Blank from 'containers/Blank';

// Import selector for `syncHistoryWithStore`
import { makeSelectLocationState } from 'containers/App/selectors';

// Import Language Provider
import LanguageProvider from 'containers/LanguageProvider';

// Import React Google Analytics
// import ReactGA from 'react-ga'; commented as per CEPROD-492

// Load the favicon, the manifest.json file and the .htaccess file
/* eslint-disable import/no-unresolved, import/extensions */
import '!file-loader?name=[name].[ext]!./favicon.ico';
import '!file-loader?name=[name].[ext]!./manifest.json';
import 'file-loader?name=[name].[ext]!./.htaccess';
/* eslint-enable import/no-unresolved, import/extensions */

import configureStore from './store';

// Import i18n messages
import { translationMessages } from './i18n';

// Import CSS reset and Global Styles
import './global-styles';

// Import root routes
import createRoutes from './routes';
import createBlankRoutes from './blank-routes';
import { version } from '../package.json';

const setupStore = () => {
// Create redux store with history
// this uses the singleton browserHistory provided by react-router
// Optionally, this could be changed to leverage a created history
// e.g. `const browserHistory = useRouterHistory(createBrowserHistory)();`
  const initialState = {};
  const store = configureStore(initialState, browserHistory);

// Sync history and store, as the react-router-redux reducer
// is under the non-default key ("routing"), selectLocationState
// must be provided for resolving how to retrieve the "route" in the state
  const history = syncHistoryWithStore(browserHistory, store, {
    selectLocationState: makeSelectLocationState(),
  });

// Set up the router, wrapping all Routes in the App component
  const rootRoute = [{
    component: App,
    path: 'application',
  // indexRoute: { onEnter: (nextState, replace) => replace('/123') },
    childRoutes: createRoutes(store),
  }, {
    component: Blank,
    path: 'user',
  // indexRoute: { onEnter: (nextState, replace) => replace('user/sign-in') },
    childRoutes: createBlankRoutes(store),
  }];

  const ourTheme = {
    spacing: {
      iconSize: 24,
      desktopGutter: 24,
      desktopGutterMore: 32,
      desktopGutterLess: 16,
      desktopGutterMini: 8,
      desktopKeylineIncrement: 64,
      desktopDropDownMenuItemHeight: 32,
      desktopDropDownMenuFontSize: 15,
      desktopDrawerMenuItemHeight: 48,
      desktopSubheaderHeight: 48,
      desktopToolbarHeight: 56,
    },
    fontFamily: 'open-sans, sans-serif',
    palette: {
      primary1Color: '#40C1C5',
      primary2Color: cyan700,
      primary3Color: grey400,
      accent1Color: pinkA200,
      accent2Color: grey100,
      accent3Color: grey500,
      textColor: darkBlack,
      alternateTextColor: white,
      canvasColor: white,
      borderColor: grey300,
      disabledColor: fade(darkBlack, 0.3),
      pickerHeaderColor: cyan500,
      clockCircleColor: fade(darkBlack, 0.07),
      shadowColor: fullBlack,
    },
  };

  const theame = {
    ...getMuiTheme(ourTheme), // lightBaseTheme),
  };

  theame.slider = {
    ...theame.slider,
  // handleFillColor: theame.slider.selectionColor,
    trackSize: 2,
    handleSize: 12,
    handleSizeActive: 12,
    handleSizeDisabled: 40,
  };
// theame.fontFamily = 'open-sans, sans-serif';
// theame.baseTheme.fontFamily = theame.fontFamily;

// commenting as per CEPROD-492
// ReactGA.initialize('UA-88737427-1'); // Unique Google Analytics tracking number

// const fireTracking = () => {
//   const pathSplit = location.pathname.split('/');
//   const page = pathSplit.length >= 3 ? pathSplit[2] : 'none';
//   ReactGA.pageview(page);
// };

  window.dataLayer = window.dataLayer || [];
function gtag() { window.dataLayer.push(arguments); } // eslint-disable-line
  gtag('js', new Date());

  const fireGtag = () => {
    const pathSplit = location.pathname.split('/');
    const page = pathSplit.length >= 3 ? pathSplit[2] : 'none';
    gtag('config', 'UA-88737427-1', {
      page_path: `/${page}`,
    });
  };

  const render = (messages) => {
    ReactDOM.render(
      <MuiThemeProvider muiTheme={getMuiTheme(theame)}>
        <Provider store={store}>
          <LanguageProvider messages={messages}>
            <Router
              onUpdate={fireGtag}
              history={history}
              routes={rootRoute}
              render={
              // Scroll to top when going to a new page, imitating default browser
              // behaviour
              applyRouterMiddleware(useScroll(() => true))
            }
            />
          </LanguageProvider>
        </Provider>
      </MuiThemeProvider>,
    document.getElementById('app')
  );
  };


// Needed for onTouchTap material-ui
// http://stackoverflow.com/a/34015469/988941
  injectTapEventPlugin();


// Hot reloadable translation json files
  if (module.hot) {
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
    module.hot.accept('./i18n', () => {
      render(translationMessages);
    });
  }

// Chunked polyfill for browsers without Intl support
  if (!window.Intl) {
    (new Promise((resolve) => {
      resolve(import('intl'));
    }))
    .then(() => Promise.all([
      import('intl/locale-data/jsonp/en.js'),
    ]))
    .then(() => render(translationMessages))
    .catch((err) => {
      throw err;
    });
  } else {
    render(translationMessages);
  }
};


uplink.init({
  client: process.env.ORBIT_IDENTITY_CLIENT,
  username: process.env.ORBIT_IDENTITY_USER,
  password: process.env.ORBIT_IDENTITY_PASSWORD,
  realm: process.env.ORBIT_IDENTITY_REALM,
  identity: process.env.ORBIT_IDENTITY,
  baseStation: process.env.UPLINK_ENDPOINT,
}).then(() => {
  setupStore();
  console.log('store configured'); // eslint-disable-line
  console.log('verison ', version); // eslint-disable-line
})
  .catch((err) => {
    console.log('error occurred ', err); // eslint-disable-line
    console.log('verison ', version); // eslint-disable-line
    setTimeout(() => {
      localStorage.clear();
      window.location.reload();
    }, 10000);
  })// eslint-disable-line

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install(); // eslint-disable-line global-require
}
