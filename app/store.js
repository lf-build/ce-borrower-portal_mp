/**
 * Create the store with asynchronously loaded reducers
 */

import { createStore, applyMiddleware, compose } from 'redux';
import { fromJS } from 'immutable';
import { persistStore, autoRehydrate } from 'redux-persist-immutable';
import { routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import createReducer from './reducers';
import globalSagas from './sagas';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(initialState = {}, history) {
  // Create the store with two middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  const middlewares = [
    sagaMiddleware,
    routerMiddleware(history),
  ];

  const enhancers = [
    applyMiddleware(...middlewares),
    autoRehydrate(),
  ];

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers =
   // process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;
  /* eslint-enable */

  const store = createStore(
    createReducer(),
    fromJS(initialState),
    composeEnhancers(...enhancers)
  );

  // Extensions
  store.runSaga = sagaMiddleware.run;
  store.asyncReducers = {}; // Async reducer registry
  store.asyncSagas = {}; // Async saga registry
  store.runningSagas = [];

  const runSagas = (sagas) => sagas.map((saga) => store.runSaga(saga)).map((task) => store.runningSagas.push(task));

  // Run default sagas
  runSagas(globalSagas);

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      import('./reducers').then((reducerModule) => {
        const createReducers = reducerModule.default;
        const nextReducers = createReducers(store.asyncReducers);

        store.replaceReducer(nextReducers);
      });
    });

    module.hot.accept('./sagas', () => {
      import('./sagas').then((sagaModule) => {
        let nextSagas = sagaModule.default;
        Promise.all(store.runningSagas.map((m) => m.cancel() && m.done()))
               .then(() => {
                 store.runningSagas = [];
                 nextSagas = Object.keys(store.asyncSagas)
                 .reduce((a, c) => [...a, ...store.asyncSagas[c]], nextSagas);
                 runSagas(nextSagas);
               });
      });
    });
  }

  persistStore(store);
  // const persistor = persistStore(store, {
  //   blacklist: ['route'],
  // });
  // persistor.pause();
  // setTimeout(() => {
  //   persistor.rehydrate({});
  // }, 2000);
  return store;
}
