// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars
  return [
    {
      path: '/',
      name: 'default',
      indexRoute: {
        onEnter: (nextState, replace) => {
          const { utm_source: utmSource, utm_medium: utmMedium, gclid } = nextState.location.query;
          let replaceValue;
          if (!utmSource && !utmMedium && !gclid) {
            replaceValue = '/user/welcome/direct';
          } else {
            const source = utmSource !== null && utmSource !== undefined ? utmSource : undefined;
            const medium = utmMedium !== null && utmMedium !== undefined ? utmMedium : undefined;
            const gclId = gclid !== null && gclid !== undefined ? gclid : undefined;
            replaceValue = `/user/welcome/${source}/${medium}/${gclId}`;
          }

          // replace(`/user/welcome${nextState.location.query.utm_source ? `/${nextState.location.query.utm_source}` : '/direct'}`);
          replace(replaceValue);
        },
      },
    },
    {
      path: 'amount',
      name: 'amountPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/AmountPage/reducer'),
          import('containers/AmountPage/sagas'),
          import('containers/AmountPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('amountPage', reducer.default);
          injectSagas('amountPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'reason',
      name: 'reasonPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          // import('containers/ReasonPage/reducer'),
          import('containers/ReasonPage/sagas'),
          import('containers/ReasonPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([sagas, component]) => {
          // injectReducer('reasonPage', reducer.default);
          injectSagas('reasonPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'basic-information',
      name: 'basicInformationPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/BasicInformationPage/reducer'),
          import('containers/BasicInformationPage/sagas'),
          import('containers/BasicInformationPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('basicInformationPage', reducer.default);
          injectSagas('basicInformationPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'mobile',
      name: 'mobilePage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/MobilePage/reducer'),
          import('containers/MobilePage/sagas'),
          import('containers/MobilePage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('mobilePage', reducer.default);
          injectSagas('mobilePage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'otp',
      name: 'otpPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/OtpPage/reducer'),
          import('containers/OtpPage/sagas'),
          import('containers/OtpPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('otpPage', reducer.default);
          injectSagas('otpPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'create-account',
      name: 'createAccountPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/CreateAccountPage/reducer'),
          import('containers/CreateAccountPage/sagas'),
          import('containers/CreateAccountPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('createAccountPage', reducer.default);
          injectSagas('createAccountPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'work-details',
      name: 'workDetailsPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/WorkDetailsPage/reducer'),
          import('containers/WorkDetailsPage/sagas'),
          import('containers/WorkDetailsPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('workDetailsPage', reducer.default);
          injectSagas('workDetailsPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: 'personal-details',
      name: 'personalDetailsPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/PersonalDetailsPage/reducer'),
          import('containers/PersonalDetailsPage/sagas'),
          import('containers/PersonalDetailsPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('personalDetailsPage', reducer.default);
          injectSagas('personalDetailsPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'review-application',
      name: 'reviewApplicationPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ReasonPage/reducer'),
          import('containers/ReviewApplicationPage/reducer'),
          import('containers/ReviewApplicationPage/sagas'),
          import('containers/ReviewApplicationPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reasonPage, reducer, sagas, component]) => {
          injectReducer('reasonPage', reasonPage.default);
          injectReducer('reviewApplicationPage', reducer.default);
          injectSagas('reviewApplicationPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'residence-expense-details',
      name: 'residenceExpenseDetailsPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ResidenceExpenseDetailsPage/reducer'),
          import('containers/ResidenceExpenseDetailsPage/sagas'),
          import('containers/ResidenceExpenseDetailsPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('residenceExpenseDetailsPage', reducer.default);
          injectSagas('residenceExpenseDetailsPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'credit-card-expense-details',
      name: 'creditCardExpenseDetailsPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/CreditCardExpenseDetailsPage/reducer'),
          import('containers/CreditCardExpenseDetailsPage/sagas'),
          import('containers/CreditCardExpenseDetailsPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('creditCardExpenseDetailsPage', reducer.default);
          injectSagas('creditCardExpenseDetailsPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'other-expense-details',
      name: 'otherExpenseDetailsPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/OtherExpenseDetailsPage/reducer'),
          import('containers/OtherExpenseDetailsPage/sagas'),
          import('containers/OtherExpenseDetailsPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('otherExpenseDetailsPage', reducer.default);
          injectSagas('otherExpenseDetailsPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'initial-offer',
      name: 'initialOfferPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('components/NotInterested/reducer'),
          import('containers/InitialOfferPage/reducer'),
          import('containers/InitialOfferPage/sagas'),
          import('components/NotInterested/sagas'),
          import('containers/InitialOfferPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([notInterestedReducer, reducer, notInterestedSagas, sagas, component]) => {
          injectReducer('notInterested', notInterestedReducer.default);
          injectReducer('initialOfferPage', reducer.default);
          injectSagas('notInterested', notInterestedSagas.default);
          injectSagas('initialOfferPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'banking',
      name: 'bankingPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/BankingPage/reducer'),
          import('containers/BankingPage/sagas'),
          import('containers/BankingPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('bankingPage', reducer.default);
          injectSagas('bankingPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'education',
      name: 'educationPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/EducationPage/reducer'),
          import('containers/EducationPage/sagas'),
          import('containers/EducationPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('educationPage', reducer.default);
          injectSagas('educationPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'employment',
      name: 'employmentPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/WorkDetailsPage/reducer'),
          import('containers/EmploymentPage/reducer'),
          import('containers/EmploymentPage/sagas'),
          import('containers/KycPage/sagas'),
          import('containers/EmploymentPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([workReducer, reducer, sagas, kycSaga, component]) => {
          injectReducer('workDetailsPage', workReducer.default);
          injectReducer('employmentPage', reducer.default);
          injectSagas('employmentPage', sagas.default);
          injectSagas('kycPage', kycSaga.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'social',
      name: 'socialPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          // import('containers/ReviewApplicationPage/reducer'),
          import('containers/SocialPage/reducer'),
          import('containers/SocialPage/sagas'),
          import('containers/SocialPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          // injectReducer('reviewApplicationPage', reviewApplicationReducer.default);
          injectReducer('socialPage', reducer.default);
          injectSagas('socialPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'final-offer',
      name: 'finalOfferPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('components/NotInterested/reducer'),
          import('containers/FinalOfferPage/reducer'),
          import('components/NotInterested/sagas'),
          import('containers/FinalOfferPage/sagas'),
          import('containers/FinalOfferPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([notInterestedReducer, reducer, notInterestedSagas, sagas, component]) => {
          injectReducer('notInterested', notInterestedReducer.default);
          injectReducer('finalOfferPage', reducer.default);
          injectSagas('notInterested', notInterestedSagas.default);
          injectSagas('finalOfferPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'kyc',
      name: 'kycPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/KycPage/reducer'),
          import('containers/KycPage/sagas'),
          import('containers/KycPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('kycPage', reducer.default);
          injectSagas('kycPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'application-rejected',
      name: 'applicationRejected',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ApplicationRejected/reducer'),
          import('containers/ApplicationRejected/sagas'),
          import('containers/ApplicationRejected'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('applicationRejected', reducer.default);
          injectSagas('applicationRejected', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'application-under-review',
      name: 'applicationUnderReview',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ApplicationUnderReview/reducer'),
          import('containers/ApplicationUnderReview/sagas'),
          import('containers/ApplicationUnderReview'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('applicationUnderReview', reducer.default);
          injectSagas('applicationUnderReview', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'application-submitted',
      name: 'applicationSubmitted',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ApplicationSubmitted/reducer'),
          import('containers/ApplicationSubmitted/sagas'),
          import('containers/ApplicationSubmitted'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('applicationSubmitted', reducer.default);
          injectSagas('applicationSubmitted', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'not-interested',
      name: 'notInterestedPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          // import('containers/NotInterestedPage/reducer'),
          // import('containers/NotInterestedPage/sagas'),
          import('containers/NotInterestedPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          // injectReducer('applicationSubmitted', reducer.default);
          // injectSagas('applicationSubmitted', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'verification-failed',
      name: 'verificationFailed',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/VerificationFailed/reducer'),
          import('containers/VerificationFailed/sagas'),
          import('containers/VerificationFailed'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('verificationFailed', reducer.default);
          injectSagas('verificationFailed', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'application-loan-agreement',
      name: 'applicationLoanAgreement',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ApplicationLoanAgreement/reducer'),
          import('containers/ApplicationLoanAgreement/sagas'),
          import('containers/ApplicationLoanAgreement'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('applicationLoanAgreement', reducer.default);
          injectSagas('applicationLoanAgreement', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'funded',
      name: 'fundedPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/FundedPage/reducer'),
          import('containers/FundedPage/sagas'),
          import('containers/FundedPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('fundedPage', reducer.default);
          injectSagas('fundedPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'application-expired',
      name: 'applicationExpiredPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ApplicationExpiredPage/reducer'),
          import('containers/ApplicationExpiredPage/sagas'),
          import('containers/ApplicationExpiredPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('applicationExpiredPage', reducer.default);
          injectSagas('applicationExpiredPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'consent-declined',
      name: 'consentDeclined',
      getComponent(location, cb) {
        import('containers/ConsentDeclined')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
