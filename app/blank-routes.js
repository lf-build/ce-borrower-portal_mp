// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: 'sign-in',
      name: 'signInPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/SignInPage/reducer'),
          import('containers/SignInPage/sagas'),
          import('containers/SignInPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('signInPage', reducer.default);
          injectSagas('signInPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'forgot-password',
      name: 'forgotPasswordPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ForgotPasswordPage/reducer'),
          import('containers/ForgotPasswordPage/sagas'),
          import('containers/ForgotPasswordPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('forgotPasswordPage', reducer.default);
          injectSagas('forgotPasswordPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'reset-account/:token/:username',
      name: 'resetAccountPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ResetAccountPage/reducer'),
          import('containers/ResetAccountPage/sagas'),
          import('containers/ResetAccountPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('resetAccountPage', reducer.default);
          injectSagas('resetAccountPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'yodlee',
      name: 'yodleeEmbededForm',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/YodleeEmbededForm/reducer'),
          import('containers/YodleeEmbededForm/sagas'),
          import('containers/YodleeEmbededForm'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('YodleeEmbededForm', reducer.default);
          injectSagas('yodleeEmbededForm', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'verify-email',
      name: 'verifyEmailPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/VerifyEmailPage/reducer'),
          import('containers/VerifyEmailPage/sagas'),
          import('containers/VerifyEmailPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('verifyEmailPage', reducer.default);
          injectSagas('verifyEmailPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: 'request-failed',
      name: 'internalServerError',
      getComponent(nextState, cb) {
        import('components/InternalServerError')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
    {
      path: 'welcome/:utm_source(/:utm_medium)(/:gclid)',
      name: 'welcomePage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/WelcomePage/reducer'),
          import('containers/WelcomePage/sagas'),
          import('containers/WelcomePage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('welcomePage', reducer.default);
          injectSagas('welcomePage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'netlink',
      name: 'netLinkEmbeddedForm',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/NetLinkEmbeddedForm/reducer'),
          import('containers/NetLinkEmbeddedForm/sagas'),
          import('containers/NetLinkEmbeddedForm'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('netLinkEmbeddedForm', reducer.default);
          injectSagas('netLinkEmbeddedForm', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'cibil-failed',
      name: 'cibilFailed',
      getComponent(location, cb) {
        import('components/CibilFailed')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
