/**
*
* FaqList
*
*/

import React from 'react';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Faq from '../Faq';
import { QuestionMarkIcon } from '../Icons';
import faqLookup from './faq';

const Wrapper = styled.div`
    padding: 10px;
    padding-left: 0px;
    display: block;
    position: relative;
`;
const Header = styled.div`
  // padding: 5px;
  color: #404040;
`;

const HeaderTitle = styled.div`
  fontSize: 13px
`;

class FaqList extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { page } = this.props;
    const faqSet = faqLookup[page] || [];
    if (faqSet.length <= 0) {
      return <span />;
    }
    return (
      <Wrapper>
        <Header>
          <QuestionMarkIcon /> <HeaderTitle> <FormattedMessage {...messages.header} /> </HeaderTitle>
        </Header>
        <div style={{ marginTop: '-5px' }}>
          {faqSet.map((faq) => <Faq key={faq.question} {...faq} />)}
        </div>
      </Wrapper>
    );
  }
}

FaqList.propTypes = {
  page: React.PropTypes.string.isRequired,
};

export default FaqList;
