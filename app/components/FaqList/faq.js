export default {
  amount: [
    {
      question: 'How much time will it take to apply for a loan?',
      answer: 'It will not take you more than 10-15 minutes to complete your application.',
    }, {
      question: 'What is the processing time?',
      answer: 'Our technology allows us to disburse the sanctioned amount in 12-30 hours of successful application.',
    }, {
      question: 'What is unique about Qbera?',
      answer: 'Qbera is a technology company with strong strategic partnerships with banks. We are not a loan agent. If our system approves your application then we automatically disburse the loan amount from a bank into your account. We work with some of the leading banks in India.',
    }, {
      question: 'What is the eligibility criteria and what are the documents your require?',
      answer: 'You need to be an Indian citizen of 23 years age and should be employed in a job with a minimum salary of &#8377 20,000 per month.<br><br>' +
          'Documents you need handy:<br> <ul><li>PAN Card</li><li>Aadhaar Card </li> <li>Latest 3 months PDF Bank Statements (Optional). We would require you to upload this if your are unable to provide us with your transaction history via your NetBanking connection. </li> ' +
          '<li>Form 26AS (Optional). We would require you to upload this if you are unable to verify your employment using your official email ID</li></ul>',
    },
  ],

  reason: [
    {
      question: 'How much time will it take to apply for a loan?',
      answer: 'It will not take you more than 10-15 minutes to complete your application.',
    }, {
      question: 'What is the processing time?',
      answer: 'Our technology allows us to disburse the sanctioned amount in 12-30 hours of successful application.',
    }, {
      question: 'What is unique about Qbera?',
      answer: 'Qbera is a technology company with strong strategic partnerships with banks. We are not a loan agent. If our system approves your application then we automatically disburse the loan amount from a bank into your account. We work with some of the leading banks in India.',
    }, {
      question: 'What is the eligibility criteria and what are the documents your require?',
      answer: 'You need to be an Indian citizen of 23 years age and should be employed in a job with a minimum salary of &#8377 20,000 per month.<br><br>' +
          'Documents you need handy:<br> <ul><li>PAN Card</li><li>Aadhaar Card </li> <li>Latest 3 months PDF Bank Statements (Optional). We would require you to upload this if your are unable to provide us with your transaction history via your NetBanking connection. </li> ' +
          '<li>Form 26AS (Optional). We would require you to upload this if you are unable to verify your employment using your official email ID</li></ul>',
    },
  ],

  'basic-information': [
    {
      question: 'How much time will it take to apply for a loan?',
      answer: 'It will not take you more than 10-15 minutes to complete your application.',
    }, {
      question: 'What is the processing time?',
      answer: 'Our technology allows us to disburse the sanctioned amount in 12-30 hours of successful application.',
    }, {
      question: 'What is unique about Qbera?',
      answer: 'Qbera is a technology company with strong strategic partnerships with banks. We are not a loan agent. If our system approves your application then we automatically disburse the loan amount from a bank into your account. We work with some of the leading banks in India.',
    }, {
      question: 'What is the eligibility criteria and what are the documents your require?',
      answer: 'You need to be an Indian citizen of 23 years age and should be employed in a job with a minimum salary of &#8377 20,000 per month.<br><br>' +
          'Documents you need handy:<br> <ul><li>PAN Card</li><li>Aadhaar Card </li> <li>Latest 3 months PDF Bank Statements (Optional). We would require you to upload this if your are unable to provide us with your transaction history via your NetBanking connection. </li> ' +
          '<li>Form 26AS (Optional). We would require you to upload this if you are unable to verify your employment using your official email ID</li></ul>',
    },
  ],

  mobile: [
    {
      question: 'Why do you want to verify my mobile number?',
      answer: 'We need you to verify your mobile number so that we can verify your identity. Also, your mobile number is used to login and continue your application at any point of time.',
    },
  ],

  otp: [
    {
      question: 'Why do you want to verify my mobile number?',
      answer: 'We need you to verify your mobile number so that we can verify your identity. Also, your mobile number is used to login and continue your application at any point of time.',
    },
    {
      question: 'What should I do if I did not receive the OTP?',
      answer: 'Please check the mobile number you have entered. If it is correct and you haven’t received the OTP, you can request the OTP to be resent to you. If after 3 tries you still do not receive it, please call us on 1800 4198 121 / Whatsapp us on +91 8971 928 484 / email us at contact@qbera.com and we would be happy to help.',
    },

  ],
  'create-account': [
    // {
    //   question: 'What is the password validation we are using?',
    //   answer: 'Your password must be at least eight characters in length and must include one uppercase letter, one special character and one number.',
    // },
    {
      question: 'How much time will it take to apply for a loan?',
      answer: 'It will not take you more than 10-15 minutes to complete your application.',
    }, {
      question: 'What is the processing time?',
      answer: 'Our technology allows us to disburse the sanctioned amount in 12-30 hours of successful application.',
    }, {
      question: 'What is unique about Qbera?',
      answer: 'Qbera is a technology company with strong strategic partnerships with banks. We are not a loan agent. If our system approves your application then we automatically disburse the loan amount from a bank into your account. We work with some of the leading banks in India.',
    }, {
      question: 'What is the eligibility criteria and what are the documents your require?',
      answer: 'You need to be an Indian citizen of 23 years age and should be employed in a job with a minimum salary of &#8377 20,000 per month.<br><br>' +
          'Documents you need handy:<br> <ul><li>PAN Card</li><li>Aadhaar Card </li> <li>Latest 3 months PDF Bank Statements (Optional). We would require you to upload this if your are unable to provide us with your transaction history via your NetBanking connection. </li> ' +
          '<li>Form 26AS (Optional). We would require you to upload this if you are unable to verify your employment using your official email ID</li></ul>',
    },

  ],
  'work-details': [
    {
      question: 'Why do you want to know the name of my employer?',
      answer: 'This is a standard eligibility check we do to offer the best interest rates to our customers.',
    }, {
      question: "I can't find my employer's name in the list. What should I do?",
      answer: 'Please make sure that you are entering the registered name of your company and not the common or the brand name. If you cannot find the name in the list then please enter it and we will be happy to add it.',
    }, {
      question: 'Shall I provide my gross or net monthly salary?',
      answer: 'Please enter your monthly take-home amount.',
    }, {
      question: 'Why do you want my official email ID?',
      answer: 'We may send a verification link to you on your official email ID.',
    },
    {
      question: 'Do you share my loan application with my employer?',
      answer: 'This is a personal loan product. We do not involve your employer at any stage of the application.',
    },

  ],
  'residence-expense-details': [
    {
      question: 'Why are you asking about my monthly expenses?',
      answer: "We would like to package a loan for you that doesn't put unnecessary stress on your monthly finances. Please bear with us while we quickly collect this information.",
    },
    // {
    //   question: 'What do "credit card balances" mean?',
    //   answer: 'A credit card balance is the amount of charges owed to a credit card company based on purchases made that have not been paid yet. The balance includes recent purchases, any unpaid balance, interest charges, annual fee and any other fees associated with the credit card such as a late fee or inactivity fee. In case you service the credit card(s) of your spouse or family members, then please include the balances of all such credit cards into the calculation.',
    // },
  ],
  'credit-card-expense-details': [
    {
      question: 'Why are you asking about my monthly expenses?',
      answer: "We would like to package a loan for you that doesn't put unnecessary stress on your monthly finances. Please bear with us while we quickly collect this information.",
    },
    {
      question: 'What do "credit card balances" mean?',
      answer: 'A credit card balance is the amount of charges owed to a credit card company based on purchases made that have not been paid yet. The balance includes recent purchases, any unpaid balance, interest charges, annual fee and any other fees associated with the credit card such as a late fee or inactivity fee. In case you service the credit card(s) of your spouse or family members, then please include the balances of all such credit cards into the calculation.',
    },
  ],
  'other-expense-details': [
    {
      question: 'Why are you asking about my monthly expenses?',
      answer: "We would like to package a loan for you that doesn't put unnecessary stress on your monthly finances. Please bear with us while we quickly collect this information.",
    },
    {
      question: 'What do "credit card balances" mean?',
      answer: 'A credit card balance is the amount of charges owed to a credit card company based on purchases made that have not been paid yet. The balance includes recent purchases, any unpaid balance, interest charges, annual fee and any other fees associated with the credit card such as a late fee or inactivity fee. In case you service the credit card(s) of your spouse or family members, then please include the balances of all such credit cards into the calculation.',
    },
  ],
  'personal-details': [
    {
      question: 'Why do you need my PAN number?',
      answer: 'PAN serves as your identity proof.',
    },
    // {
    //   question: 'In which cities do you operate?',
    //   answer: 'Currently, we are operational in Bengaluru. We will be launching in other cities soon.',
    // },

  ],
  'review-application': [
    {
      question: 'Can I change the details that I have provided?',
      answer: 'Yes, this form is editable.',
    },
    {
      question: 'Will I be allowed to change my information after submitting this form?',
      answer: 'No, you will not be able to change your details later. Please review this page carefully as our lending decision will be based on it.',
    },

  ],
  banking: [
    {
      question: 'Why are you asking for my NetBanking credentials?',
      answer: "Our technology allows you to connect with your bank's servers directly from our website through a safe link and give us read-only access to your transactions. This connection bridge allows us to quickly analyse your income and spending patterns and give you a lending decision. Your credentials are never stored by us as the information flows directly to your bank via a secure connection.Alternatively, you can download your bank statements in PDF format and upload them to our system.",
    },
    {
      question: 'Can you operate my bank account if I connect to it via your website?',
      answer: 'There is no way we can do that. Your bank only gives us read-only access to the transactions. You may think of this connect as a "Sign in with Google" or "Sign in with Facebook" technology that allows 3rd party services to securely get your Google or Facebook profile information without sharing your username and password with them.',
    },
    {
      question: 'How many months of transactions do I need to provide?',
      answer: "We would need last 3 months' transactions.",
    },
  ],
  education: [
    {
      question: 'Why do you need my education details?',
      answer: 'Your education details help us in fixing the best interest rate for you.',
    },
  ],
  social: [
    {
      question: 'Why do I have to connect my Facebook account?',
      answer: 'Logging into your Facebook account will allow Qbera to understand a bit more about your profile. This will help us give you the best possible interest rate by taking into account your social score. We do not post anything on Facebook on your behalf.',
    },
    {
      question: 'Will you be able to operate my Facebook account if I provide my login credentials?',
      answer: 'Qbera does not receive, see, or store any credentials. When you provide your login information, it is encrypted and transmitted to Facebook for verification via a secure and direct bridge between you and Facebook.',
    },
    {
      question: 'Why do I have to connect my email account?',
      answer: 'Connecting your email account will allow Qbera to understand your mailing patterns for assessing your social score. We never read any emails. These broad patterns and trends help us create a better picture of your personality and in-turn enables you to attract lower interest rates.',
    },
    {
      question: 'Will you be able to operate my email account if I provide my login credentials?',
      answer: 'Similar to Facebook connect, Qbera does not receive, see, or store any credentials. When you provide your login information, it is encrypted and transmitted to the email provider for verification via a secure and direct bridge between you and the provider.',
    },

  ],
  kyc: [
    {
      question: 'Which document can I provide for KYC?',
      answer: `You can provide a copy of any one of the following documents:               
               <br />Voter's ID Card               
               <br />Valid Driving License               
               <br />Valid Passport               
               <br />UID (Aadhaar)               
               <br />Any other document notified by the central government`,
    },
  ],

};
