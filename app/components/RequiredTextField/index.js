/**
*
* RequiredTextField
*
*/

import React from 'react';
import { Field } from 'redux-form/immutable';
// import styled from 'styled-components';
import TextField from 'redux-form-material-ui/lib/TextField';
import MaskedTextField from '../../components/MaskedTextField';
import RealMaskedTextField from '../../components/RealMaskedTextField';

import {
  required,
  minLength,
  maxLength,
  isValidEmail,
  alwaysValid,
  defaultMaxLen,
  defaultMinLen,
  isDate,
} from '../validators';


class RequiredTextField extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    if (this.props.focus) {
      this.focus();
    }
  }

  focus() {
    document.getElementById(Array.from(document.getElementsByName(this.props.name)[0].children).filter((c) => c.tagName === 'INPUT')[0].id).focus();
    // console.log(this.textField.getRenderedComponent());
    // this.textField
    // .getRenderedComponent()
    // // .getRenderedComponent()
    // .focus();
  }

  render() {
    const registerTextField = (textField) => {
      if (textField) {
        this.textField = textField;
      }
    };

    // const styles = {
    //   errorStyle: {
    //     fontSize: 12,
    //     color: '#f2584d',
    //     fontWeight: 100,
    //   },
    // };

    const {
      floatingLabelText,
      minLen,
      maxLen,
      optional,
      email,
      name,
      focus, // eslint-disable-line no-unused-vars
      mask,
      date,
      multiLine,
      extraValidators,
      enforceMasking,
      ...rest
    } = this.props;

    const generateErrorMsg = (value) => {
      if (value === 'amountField') {
        // return `Please select ${value} from slider`;
        return 'Please enter an Amount';
      }

      if (value === 'mobileNumber') {
        return 'Please enter a valid mobile number';
      }

      if (value === 'otp' || value === 'password') {
        return 'This field is required';
      }

      if (value === 'monthlyTakeHomeSalary') {
        return 'Please enter amount';
      }

      if (value === 'dateOfBirth') {
        return 'Date Of Birth cannot be left blank';
      }

      if (value === 'homeLoanEmi') {
        return 'Please tell us how much your total EMI on home loans is';
      }

      return undefined;
    };
    const MaskedField = enforceMasking ? RealMaskedTextField : MaskedTextField;
    return (<Field
      name={name}
      component={mask ? MaskedField : TextField}
      mask={mask}
      floatingLabelText={floatingLabelText}
      floatingLabelStyle={{ fontSize: '14px' }}
      floatingLabelFocusStyle={{ fontSize: '14px' }}
      floatingLabelShrinkStyle={{ fontSize: '14px' }}
      autoComplete="off"
      multiLine={multiLine}
      ref={registerTextField}
      withRef
      errorStyle={{ fontSize: 12, color: '#ff756a', textAlign: 'left', lineHeight: '15px', marginTop: '6px' }}
      {...rest}
      validate={[
        date ? isDate(floatingLabelText) : alwaysValid,
        optional ? alwaysValid : required(floatingLabelText, generateErrorMsg(name)),
        minLength(floatingLabelText, minLen || defaultMinLen),
        maxLength(floatingLabelText, maxLen || defaultMaxLen),
        email ? isValidEmail(floatingLabelText) : alwaysValid,
        ...(extraValidators || []),
      ]}
    />);
  }
}
  /* style={{ height: '40px', marginTop: '10px' }}
      floatingLabelStyle={{ top: '10px' }}
      hintStyle={{ top: '6px' }}
      errorStyle={{ top: multiLine ? '0px' : '-17px', ...styles.errorStyle }}
      inputStyle={{ top: '-16px' }}*/

RequiredTextField.propTypes = {
  floatingLabelText: React.PropTypes.string,
  enforceMasking: React.PropTypes.bool,
  name: React.PropTypes.string,
  minLen: React.PropTypes.number,
  maxLen: React.PropTypes.number,
  optional: React.PropTypes.bool,
  email: React.PropTypes.bool,
  mask: React.PropTypes.any,
  match: React.PropTypes.string,
  focus: React.PropTypes.bool,
  date: React.PropTypes.bool,
  multiLine: React.PropTypes.bool,
  extraValidators: React.PropTypes.array,
};

export default RequiredTextField;
