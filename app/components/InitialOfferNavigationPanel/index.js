/**
*
* InitialOfferNavigationPanel
*
*/

import React from 'react';
// import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';
import NotInterested from '../NotInterested';

function InitialOfferNavigationPanel(props) {
  const { isLoadingOrSubmiting, handleGoNext, nextDisabled, offerType } = props;
  const reasons = offerType === 'initial' ? [
    {
      id: 'lesseramount',
      label: 'This amount is less than my loan application amount',
    },
    {
      id: 'otherlender',
      label: 'I\'m taking a loan from some other lender',
    },
    {
      id: 'tooslow',
      label: 'Application is taking too long a time',
    },
    {
      id: 'other',
      label: 'Other',
    }]
    // Final Offer related reasons
    : [{
      id: 'lessoffer',
      label: 'This amount is less than my loan application amount',
    }, {
      id: 'highinterest',
      label: 'The interest rate is high',
    }, {
      id: 'longduration',
      label: 'The duration is too long',
    }, {
      id: 'unexpectedemi',
      label: 'I\'m not happy with the EMI amount',
    }, {
      id: 'unexpectedprocessingfee',
      label: 'I\'m not happy with the processing fee',
    }, {
      id: 'unhappyoverall',
      label: 'I\'m not happy with the application experience',
    }, {
      id: 'otherlender',
      label: 'I\'m taking a loan from some other lender',
    }, {
      id: 'other',
      label: 'Other',
    }];
  return (
    <div className="center-block">
      <div className="col-xs-12">
        <RaisedButton
          disabled={isLoadingOrSubmiting || nextDisabled}
          onTouchTap={handleGoNext}
          style={{ marginLeft: 'calc(50% - 44px)' }}
          label="Next"
          labelColor="#fff"
          labelStyle={{ fontSize: '12px', letterSpacing: '1.2px' }}
          backgroundColor="#3663ad"
        />
      </div>
      <div className="col-xs-12" style={{ textAlign: 'center', marginTop: '20px' }}>
        <NotInterested reasons={reasons} />
      </div>
    </div>

  );
}

InitialOfferNavigationPanel.propTypes = {
  isLoadingOrSubmiting: React.PropTypes.bool.isRequired,
  handleGoNext: React.PropTypes.func.isRequired,
  nextDisabled: React.PropTypes.bool.isRequired,
  offerType: React.PropTypes.string.isRequired,
};

export default InitialOfferNavigationPanel;
