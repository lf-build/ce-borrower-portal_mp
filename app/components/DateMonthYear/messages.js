/*
 * DateMonthYear Messages
 *
 * This contains all the text for the DateMonthYear component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DateMonthYear.header',
    defaultMessage: 'This is the DateMonthYear component !',
  },
});
