/*
 * CibilFailed Messages
 *
 * This contains all the text for the CibilFailed component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.CibilFailed.header',
    defaultMessage: 'Oops ...',
  },
  body: {
    id: 'app.components.CibilFailed.body',
    defaultMessage: "It seems that CIBIL's servers are not responding and we are not able to pull your credit report. Please log back into your account after some time and try resubmitting the application. Don't worry, your details are stored in your account and you will not be required to re-enter the information. If the issue persists then please chat with our customer service representative who will assist you.",
  },
});
