import emailMaskRoot from 'text-mask-addons/dist/emailMask';
import createAutoCorrectedDatePipe from 'text-mask-addons/dist/createAutoCorrectedDatePipe';
const buildPlaceHolders = (count, placeholder = /\d/) => Array(count).join('a').split('a').map(() => placeholder);

export const dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
export const datePipe = createAutoCorrectedDatePipe('dd/mm/yyyy');
export const aadharMask = [...buildPlaceHolders(4, /[a-zA-Z0-9]/), ' ', ...buildPlaceHolders(4, /[a-zA-Z0-9]/), ' ', ...buildPlaceHolders(4, /[a-zA-Z0-9]/)];
export const panMask = [...buildPlaceHolders(3, /[a-zA-Z]/), 'P', /[a-zA-Z]/, ...buildPlaceHolders(4), /[a-zA-Z]/];
export const toUpperPipe = (value) => value && value.toString().toUpperCase();
export const placeholderChar = '●';

export const mobileMask = [/[1-9]/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/];
export const otpMask = [/\d/, ' ', /\d/, ' ', /\d/, ' ', /\d/, ' ', /\d/, ' ', /\d/];
export const pincodeMask = buildPlaceHolders(6);
export const moneyMask = (e) => {
  const eqvVal = normalizeMoney(e);
  const isAlphanumeric = new RegExp('[0-9,]$');
  // const leadingZeros = new RegExp('^[0]{1}');
  // const oneleadingZeros = new RegExp('^[0]{2}');
  const nonAlphanumeric = new RegExp('^[0-9]*$');
  const leadingZeroWithDigits = new RegExp('^([0]{1}|[0]{2}|[0][1-9])');

  // When user tries to enter only Leading Zeros.
  if (eqvVal && e !== '' && leadingZeroWithDigits.test(eqvVal)) {
    return [/\d/];
  }

  // When user tries to enters characters and special characters in money mask related inputs.
  if (!isAlphanumeric.test(e) || !nonAlphanumeric.test(eqvVal)) {
    if (eqvVal.length === 0 || eqvVal.length === 1) {
      return [];
    } else if (eqvVal.length <= 4) {
      return [...buildPlaceHolders(eqvVal.length - 1)];
    } else if (eqvVal.length <= 6) {
      return [...buildPlaceHolders(eqvVal.length - 4), ',', ...buildPlaceHolders(3)];
    }
  }

  if (eqvVal.length <= 3) {
    return [...buildPlaceHolders(eqvVal.length)];
  }

  if (eqvVal.length <= 5) {
    return [...buildPlaceHolders(eqvVal.length - 3), ',', ...buildPlaceHolders(3)];
  }

  if (eqvVal.length === 6) {
    return [/\d/, ',', /\d/, /\d/, ',', /\d/, /\d/, /\d/];
  }

  return [/\d/, /\d/, ',', /\d/, /\d/, ',', /\d/, /\d/, /\d/];
};

export const emailMask = emailMaskRoot;
export const inputMask = [/[a-zA-Z]/, ...buildPlaceHolders(49, /[a-zA-Z. ']/)];
export const alphaNumeric = [...buildPlaceHolders(20, /[a-zA-Z0-9]/)];
export const addressLineMask = [/[a-zA-Z0-9#'.,-/]/, ...buildPlaceHolders(39, /[a-zA-Z0-9 #'.,-/]/)];

/* NORMALIZE */
export const normalizeMoney = (money) => money.toString().replace(/,/g, '').replace(/ /g, '').replace('₹', '').replace(/●/g, '');
export const normalizeMobile = (number) => number.toString().replace('+91', '').replace(/ /g, '').replace(/●/g, '');
export const normalizeAadhar = (aadhar) => aadhar && aadhar.toString().replace(/ /g, '');
export const normalizePincode = (pincode) => pincode.toString().replace(/●/g, '');
