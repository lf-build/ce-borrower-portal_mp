/**
*
* NotInterested
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import CheckBoxOn from 'material-ui/svg-icons/toggle/check-box';
import CheckBoxOff from 'material-ui/svg-icons/toggle/check-box-outline-blank';
import TextField from 'material-ui/TextField';
import styled from 'styled-components';
import { notInterested as notInterestedActionCreator } from './actions';
import makeSelectNotInterested from './selectors';

const styles = {
  action: {
    borderTop: '1px solid #e5e5e5',
  },
  button: {
    margin: 5,
  },
  otherCommentsBlock: {
    border: '1px solid #ccc',
    paddingLeft: '10px',
    paddingRight: '10px',
  },
  otherCommentsUnderline: {
    border: 'none',
  },
  iconStyle: {
    marginRight: '2px',
    color: '#4e4e4e',
  },
  labelStyle: {
    color: '#5e5e5e',
    fontSize: 14,
    marginBottom: 5,
  },
};

const BoldTextBlock = styled.p`
  font-weight: bold;
`;

/*
Hidden as NotInterested link has to be hidden as a result of CR

const NotInterestedLink = styled.span`
  color: #79cdd5;
  cursor: pointer;
  text-align: center;
  z-index: 999;
  margin-top: 100;
`;
*/

class NotInterested extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      openDialog: false,
      submitted: false,
    };
  }
  render() {
    const { onOk, onCancel, notInterested, reasons } = this.props;
    const { openDialog, selected } = this.state;
/*
    Hidden as NotInterested link has to be hidden as a result of CR
    const handleOpen = () => {
      this.setState({ ...this.state, ...{ openDialog: true } });
    };
*/
    const closeAndReset = () => {
      this.setState({ openDialog: false });
     // updating state when Cancel or Submit buttons are clicked in dialog.
      this.setState({ selected: undefined });
    };

    const handleClose = (ok) => () => {
      if (ok) {
        this.setState({ ...this.state, submitted: true });
        (onOk || notInterested)(this.state.selected);
      } else if (onCancel) {
        onCancel();
        closeAndReset();
      } else {
        closeAndReset();
      }
    };

    const handleCheck = (event, checked) => {
      if (!checked && selected === '7') {
        this.setState({ selected: undefined });
      } else {
        this.setState({ selected: event.target.value });
      }
    };

    // const NotInterestedReasons = [
    //   {
    //     id: 'r5',
    //     label: 'Don\'t need the loan',
    //   },
    //   {
    //     id: 'r6',
    //     label: 'Interest too high',
    //   },
    //   {
    //     id: 'r7',
    //     label: 'Approved amount too low',
    //   },
    //   {
    //     id: 'r8',
    //     label: 'Competitor offered better terms',
    //   },
    // ];

    const actions = [
      <RaisedButton
        label="Cancel"
        primary
        style={styles.button}
        onTouchTap={handleClose(false)}
        disabled={(this.state && this.state.submitted)}
      />,
      <RaisedButton
        label="Submit"
        primary
        style={styles.button}
        onTouchTap={handleClose(true)}
        disabled={!(this.state && (this.state.selected && !this.state.submitted))}
      />,
    ];
    return (
      <div>
        <div className="center-block">
          {/* <NotInterestedLink onClick={handleOpen}>NOT INTERESTED</NotInterestedLink> */}
        </div>
        <Dialog
          actions={actions}
          modal={false}
          open={openDialog}
          onRequestClose={handleClose}
          actionsContainerStyle={styles.action}
        >
          <BoldTextBlock>
            We are sorry to see you go. Please select a reason why you would not like to proceed.
          </BoldTextBlock>

          <div>
            <RadioButtonGroup
              name="NotInterested"
              defaultSelected="not_light"
              onChange={handleCheck}
              disabled={(this.state && this.state.submitted)}
            >
              {
                reasons.map((reason, index) =>
                  <RadioButton
                    disabled={(this.state && this.state.submitted)}
                    label={reason.label}
                    value={reason.id}
                    key={index}
                    checkedIcon={<CheckBoxOn />}
                    uncheckedIcon={<CheckBoxOff />}
                    iconStyle={styles.iconStyle}
                    labelStyle={styles.labelStyle}
                  />
                )
              }
            </RadioButtonGroup>
          </div>

          <div className={selected === '7' ? '' : 'hidden'}>
            <BoldTextBlock>Comments:</BoldTextBlock>
            <TextField
              name={'comments'}
              multiLine
              rows={2}
              rowsMax={2}
              fullWidth
              style={styles.otherCommentsBlock}
              underlineStyle={styles.otherCommentsUnderline}
            />
          </div>

        </Dialog>

      </div>
    );
  }
}

NotInterested.propTypes = {
  onOk: React.PropTypes.func,
  onCancel: React.PropTypes.func,
  notInterested: React.PropTypes.func.isRequired,
  reasons: React.PropTypes.array.isRequired,
};

const mapStateToProps = () => createStructuredSelector({
  NotInterested: makeSelectNotInterested(),
});

function mapDispatchToProps(dispatch) {
  return {
    notInterested: (id) => dispatch(notInterestedActionCreator(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotInterested);
