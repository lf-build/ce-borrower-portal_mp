import { createSelector } from 'reselect';

/**
 * Direct selector to the notInterested state domain
 */
const selectNotInterestedDomain = () => (state) => state.get('notInterested');

/**
 * Other specific selectors
 */


/**
 * Default selector used by notInterested
 */

const makeSelectNotInterested = () => createSelector(
  selectNotInterestedDomain(),
  (substate) => substate.toJS()
);

export default makeSelectNotInterested;
export {
  selectNotInterestedDomain,
};
