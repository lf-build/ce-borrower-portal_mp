/*
 * ExpenseDetailsTabs Messages
 *
 * This contains all the text for the ExpenseDetailsTabs component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ExpenseDetailsTabs.header',
    defaultMessage: 'This is the ExpenseDetailsTabs component !',
  },
});
