/**
*
* VerificationProcess
*
*/

import React from 'react';
import styled from 'styled-components';

const A = styled.a``;

class VerificationProcess extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { step: stepIndex } = this.props;

    const getTabStyleByStepIndex = (index, currentIndx) => {
      if (index === currentIndx) {
        return 'active';
      } else if (index < currentIndx) {
        return 'completed';
      }

      return '';
    };

    return (
      <div style={{ textTransform: 'uppercase' }}>
        <ul className="verify-tabs">
          <li className={`tab1 ${getTabStyleByStepIndex(0, stepIndex)}`}><A href="#">Banking</A></li>
          <li className={`tab2 ${getTabStyleByStepIndex(1, stepIndex)}`}><A href="#">Social</A></li>
          <li className={`tab3 ${getTabStyleByStepIndex(2, stepIndex)}`}><A href="#">Education</A></li>
          <li className={`tab4 ${getTabStyleByStepIndex(3, stepIndex)}`}><A href="#">Employment</A></li>
        </ul>
      </div>
    );
  }
}

VerificationProcess.propTypes = {
  step: React.PropTypes.number.isRequired,
};

export default VerificationProcess;
