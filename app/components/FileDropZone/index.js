/**
*
* FileDropZone
*
*  To accept PDF and all images use following string in accpet
*  <FileDropZone accept={'application/pdf,image/*'} ..otherProps />
*
*/

import React from 'react';
import styled from 'styled-components';
import Dropzone from 'react-dropzone';
import PictureAsPdf from 'material-ui/svg-icons/image/picture-as-pdf';
import ImageIconMUI from 'material-ui/svg-icons/image/image';
const DataLinkContainer = styled.span`
  margin-left: 0px;
  float: left;
  display: block;
`;
const DataLink = styled.a`
  text-decoration: none;
  cursor: pointer;
  color: #333;
  float: left;  
  margin-left: 10px;
  word-wrap: break-word;
`;


const PdfIcon = styled(PictureAsPdf)`
  // color: #00BCD4 !important;
  color: #2cc6c7 !important;
  float: left;
`;
const ImageIcon = styled(ImageIconMUI)`
  color: #00BCD4 !important;
  float: left;
`;
const FilesContainer = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-direction: column;
    flex-direction: column;
    margin-bottom: 10px;
`;
const Message = styled.div`
  color: #00BCD4 !important;
`;

class FileDropZone extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { files, message, onDrop, allowMultiple, onRemove, ...otherProps } = this.props;

    // capital .PDF extension statement when uploaded and the same when tried to open in Back-Office was resulting in
    // issue so converting this function will convert captial .PDF to small .pdf extension.
    const handleCapitalFileExtension = (filename) => {
      // there can be a chance when file name may contain dots (eg: abc.def.PDF) so after split('.') taking length
      // to get the exact extension.
      const len = filename && filename.split('.').length;
      const extension = filename.split('.')[len - 1];
      if (extension === 'pdf' || extension === 'PDF') {
        return filename.replace('.PDF', '.pdf');
      }

      return filename;
    };

    const handleFileDropped = (blobs) => {
      blobs.forEach((file) => {
        const reader = new FileReader();
        reader.onload = (e) => {
          const filename = handleCapitalFileExtension(file.name);
          onDrop({ file: filename, type: file.type, dataUrl: e.target.result });
        };
        reader.readAsDataURL(file);
      });
    };
    return (
      <div>

        <FilesContainer>
          {/* <span> Upload queue </span>*/}
          {
            files.map((blob, index) => <DataLinkContainer key={blob.file}>
              {blob.type.startsWith('image') ? <ImageIcon /> : <PdfIcon />}
              <DataLink className={'bank-statement'} target="_blank" href={blob.dataUrl}> {blob.file} </DataLink>
              <DataLink style={{ width: '10%', float: 'right', position: 'absolute' }} onClick={() => onRemove(blob, index)}> <span className={'trash'} /> </DataLink>
            </DataLinkContainer>)
          }
        </FilesContainer>

        <Dropzone className={files.length && !allowMultiple ? 'hidden' : ''} style={{ fontSize: '12px', cursor: 'pointer', textAlign: 'center', border: '1px dashed #76cdd6', borderRadius: '5px', padding: '15px' }} onDrop={handleFileDropped} {...otherProps}>
          <Message>
            {message}
          </Message>
        </Dropzone>
      </div>
    );
  }
}

FileDropZone.propTypes = {
  files: React.PropTypes.array.isRequired,
  message: React.PropTypes.object.isRequired,
  onDrop: React.PropTypes.func.isRequired,
  onRemove: React.PropTypes.func.isRequired,
  allowMultiple: React.PropTypes.bool,
};

export default FileDropZone;
