/**
*
* ReApply
*
*/

import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
// import styled from 'styled-components';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

const styles = {
  action: {
    borderTop: '1px solid #e5e5e5',
  },
  button: {
    margin: 5,
  },
  otherCommentsBlock: {
    border: '1px solid #ccc',
    paddingLeft: '10px',
    paddingRight: '10px',
  },
  otherCommentsUnderline: {
    border: 'none',
  },
  iconStyle: {
    marginRight: '2px',
    color: '#4e4e4e',
  },
  labelStyle: {
    color: '#5e5e5e',
    fontSize: 14,
    marginBottom: 5,
  },
};

class ReApply extends React.Component { // eslint-disable-line react/prefer-stateless-function
  // state = {
  //   open: false,
  // };

  handleOpen = () => {
    // this.setState({ open: true });
  };

  handleClose = () => {
    // this.setState({ open: false });
  };

  render() {
    const { openDialog, acceptReapply, denyReapply } = this.props;

    const handleClose = () => {
      this.setState({ open: false });
    };

    const handleAcceptReapply = () => {
      acceptReapply();
      handleClose();
    };

    const handledenyReapply = () => {
      denyReapply();
      handleClose();
    };

    const actions = [
      <FlatButton
        label="Yes"
        primary
        style={styles.button}
        onTouchTap={() => handleAcceptReapply()}
      />,
      <FlatButton
        label="No"
        primary
        style={styles.button}
        onTouchTap={() => handledenyReapply()}
      />,
    ];

    return (
      <div>
        <Dialog
          title="Re Apply"
          actions={actions}
          modal={false}
          open={openDialog}
          onRequestClose={this.handleClose}
        >
          <div style={{ textAlign: 'center' }}>
            Would you like to reapply ?
          </div>
        </Dialog>
      </div>
    );
  }
}

ReApply.propTypes = {
  openDialog: React.PropTypes.bool,
  acceptReapply: React.PropTypes.func,
  denyReapply: React.PropTypes.func,
};

export default ReApply;
