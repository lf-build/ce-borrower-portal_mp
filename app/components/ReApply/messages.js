/*
 * ReApply Messages
 *
 * This contains all the text for the ReApply component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ReApply.header',
    defaultMessage: 'This is the ReApply component !',
  },
});
