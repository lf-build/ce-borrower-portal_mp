/**
*
* HintText
*
*/

import React from 'react';
import styled from 'styled-components';

const HintTextBlock = styled.span`
  min-height: 20px;
  font-size: 11px;
  padding-bottom: 10px;
  display: block;
`;

const Notes = styled.span`
  font-weight: 800;
`;

class HintText extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { message } = this.props;
    return (
      <HintTextBlock>
        <Notes>Note: </Notes>
        <span>{message}</span>
      </HintTextBlock>
    );
  }
}

HintText.propTypes = {
  message: React.PropTypes.string,
};

export default HintText;
