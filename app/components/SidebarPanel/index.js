/**
*
* SidebarPanel
*
*/

import React from 'react';
import Divider from 'material-ui/Divider';
import styled from 'styled-components';
import { ShieldIcon, CallIcon, LockIcon } from '../Icons';

const Header = styled.div`
  margin-top: 10px;
`;

const HeaderTitle = styled.div`
    line-height: 20px;
    text-transform: uppercase;
    color: rgb(68, 68, 68);
    font-size: 11px;
    font-weight: 600;
    letter-spacing: 0.125em;
    margin: 0px;
    padding: 0px;
`;

const PanelBody = styled.p`
    margin-top: -2px;
    margin-left: 30px;
    font-size: 11px;
    line-height: 17px;
    color: rgb(157, 160, 160);
    font-weight: 400;
`;


class SidebarPanel extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div style={{ float: 'left', marginTop: '5px' }}>
        {/* <div style={{ marginTop: '8px' }}>
          <Divider />
          <Header></Header>
          <PanelBody>Loan amounts range from Rs.50,000 to Rs.7,50,000. APR ranges from 14% to 24%. Loan lengths range from 12 to 48 months. Administration fee ranges from 2% to 4%.</PanelBody>
        </div> */}
        <Divider />
        <Header>
          <ShieldIcon /> <HeaderTitle> PRIVACY </HeaderTitle>
        </Header>
        <PanelBody>We do not use or share your personal information beyond the purpose of assessing your loan application.</PanelBody>
        <div style={{ marginTop: '8px' }}>
          <Divider />
          <Header>
            <LockIcon /> <HeaderTitle> SAFE & SECURE </HeaderTitle>
          </Header>
          <PanelBody>128 bit SSL encryption. We use bank level security to keep all your data secure.</PanelBody>
        </div>
        <div style={{ marginTop: '8px' }}>
          <Divider />
          <Header>
            <CallIcon /> <HeaderTitle> NEED HELP </HeaderTitle>
          </Header>
          <PanelBody><br />
            <a href="https://api.whatsapp.com/send?phone=918971928484&text=Hi%20Team%20Qbera,%20can%20you%20help%20me%20out?">
              <img
                alt="Whatsapp"
                src="https://marketing-image-production.s3.amazonaws.com/uploads/4de3d2da3bb7251dda961f69eb8c7e26984e0c10eb213ce17c5a60049992b7fe025cf443f6d07a2815d1799ca785e4c7a66956871238befc730839a17e2df595.png"
                style={{
                  cursor: 'pointer',
                  width: '100px',
                  backgroundColor: '#455a64',
                  padding: '10px 15px',
                  boxShadow: '0 7px 14px rgba(50,50,93,.1), 0 3px 6px rgba(0,0,0,.08)',
                  webkitBoxShadow: '0 7px 14px rgba(50,50,93,.1), 0 3px 6px rgba(0,0,0,.08)',
                  mozBoxShadow: '0 7px 14px rgba(50,50,93,.1), 0 3px 6px rgba(0,0,0,.08)',
                  borderRadius: '5px'
                }}
              />
            </a>
          </PanelBody>
        </div>
      </div>
    );
  }
}

SidebarPanel.propTypes = {

};

export default SidebarPanel;
