/**
*
* AddressDetails
*
*/

import React from 'react';
import TextField from '../../components/RequiredTextField';
import HintText from '../../components/HintText';
import { pincodeMask, normalizePincode, addressLineMask } from '../../components/MaskedTextField/masks';

class AddressDetails extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { optional, className } = this.props;
    return (
      <div className={className}>
        <TextField
          enforceMasking
          name={this.props.addressLine1}
          fullWidth
          hintText="Address Line 1"
          floatingLabelText="Address Line 1"
          optional={optional}
          maxLen={40}
          minLen={optional ? 1 : 3}
          mask={addressLineMask}
        />
        <TextField
          enforceMasking
          name={this.props.addressLine2}
          fullWidth
          hintText="Address Line 2 (Optional)"
          floatingLabelText="Address Line 2 (Optional)"
          optional
          maxLen={40}
          minLen={optional ? 1 : 3}
          mask={addressLineMask}
        />
        <TextField
          enforceMasking
          name={this.props.locality}
          fullWidth
          hintText="Locality"
          floatingLabelText="Locality"
          optional={optional}
          maxLen={40}
          minLen={optional ? 1 : 3}
          mask={addressLineMask}
        />
        <TextField
          name={this.props.pincode}
          fullWidth
          hintText="Pin Code"
          floatingLabelText="Pin Code"
          minLen={6}
          maxLen={6}
          optional={optional}
          mask={pincodeMask}
          enforceMasking
          extraValidators={[(value) => (optional || (value && normalizePincode(value.toString()).match(/^([1-9])([0-9]){5}$/))) ? undefined : 'Pin Code is invalid']}
        />
        <TextField
          name={this.props.city}
          fullWidth
          hintText="Please enter Pin Code above"
          floatingLabelText="City"
          optional={optional}
          inputStyle={{ textTransform: 'capitalize' }}
          readOnly
        />
        <HintText message={'City will be filled automatically on the basis of the Pin Code.'} />

        <TextField
          name={this.props.state}
          fullWidth
          hintText="Please enter Pin Code above"
          floatingLabelText="State"
          optional={optional}
          inputStyle={{ textTransform: 'capitalize' }}
          readOnly
        />
        <HintText message={'State will be filled automatically on the basis of the Pin Code.'} />

      </div>
    );
  }
}

AddressDetails.propTypes = {
  addressLine1: React.PropTypes.string,
  addressLine2: React.PropTypes.string,
  locality: React.PropTypes.string,
  pincode: React.PropTypes.string,
  city: React.PropTypes.string,
  state: React.PropTypes.string,
  className: React.PropTypes.string,
  optional: React.PropTypes.bool,
};

export default AddressDetails;
