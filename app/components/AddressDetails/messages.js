/*
 * AddressDetails Messages
 *
 * This contains all the text for the AddressDetails component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AddressDetails.header',
    defaultMessage: 'This is the AddressDetails component !',
  },
});
