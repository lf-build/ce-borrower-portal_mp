/*
 * InternalServerError Messages
 *
 * This contains all the text for the InternalServerError component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.InternalServerError.header',
    defaultMessage: 'Oops ...',
  },
  body: {
    id: 'app.components.InternalServerError.body',
    defaultMessage: 'Something went wrong, Please try after sometime',
  },
});
