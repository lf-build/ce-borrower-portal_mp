/*
 * IdentitySessionPanel Messages
 *
 * This contains all the text for the IdentitySessionPanel component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  loginText: {
    id: 'app.components.IdentitySessionPanel.loginText',
    defaultMessage: 'Login',
  },
  logoutText: {
    id: 'app.components.IdentitySessionPanel.logoutText',
    defaultMessage: 'Logout',
  },
});
