/**
*
* IdentitySessionPanel
*
*/

import React from 'react';
// import styled from 'styled-components';
import FlatButton from 'material-ui/FlatButton';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { createStructuredSelector } from 'reselect';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import makeAuthSelector from '../../sagas/auth/selectors';
import { userSignedOut } from '../../sagas/auth/actions';
import messages from './messages';

function IdentitySessionPanel(props) {
  const { auth: { user }, signOut } = props;
  return (
    <div>
      <div className={user ? 'welcome-msg-desktop' : 'welcomeMsg'}>
        <FlatButton onTouchTap={() => browserHistory.push('/user/sign-in')} className={user ? 'hidden' : ''} label={<FormattedMessage {...messages.loginText} />} />
        <span className={user ? '' : 'hidden'}> Hello {user && user.name} ! </span>
        <FlatButton onTouchTap={signOut} className={user ? '' : 'hidden'} label={<FormattedMessage {...messages.logoutText} />} />
      </div>
      <div className={user ? 'welcome-msg-mobile' : 'hidden'}>
        <IconMenu
          iconButtonElement={
            <IconButton style={{ color: '#79cdd5' }}><MoreVertIcon /></IconButton>
        }
          anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
          targetOrigin={{ horizontal: 'right', vertical: 'top' }}
        >
          <MenuItem disabled style={{ color: 'rgba(0, 0, 0, 0.870588)' }}><span className={user ? '' : 'hidden'}> Hello {user && user.name} ! </span></MenuItem>
          <MenuItem onTouchTap={signOut} className={user ? '' : 'hidden'} primaryText={<FormattedMessage {...messages.logoutText} />}></MenuItem>
        </IconMenu>
      </div>
    </div>
  );
}

IdentitySessionPanel.propTypes = {
  auth: React.PropTypes.object,
  signOut: React.PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    signOut: () => dispatch(userSignedOut()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(IdentitySessionPanel);
