/**
*
* AutoComplete
*
*/

import AutoComplete from 'material-ui/AutoComplete';
import createComponent from './createComponent';
import mapError from './mapError';

// export default createComponent(
//   AutoComplete,
//   ({ input: { value, name, ...inputProps }, onNewRequest: onNewRequestFunc, dataSourceConfig, ...props }) => ({
//     ...mapError(props),
//     ...inputProps,
//     searchText: dataSourceConfig ? value[dataSourceConfig.text] : value,
//     dataSourceConfig,
//     onNewRequest: (newValue) => {
//       inputProps.onChange(newValue);
//       if (onNewRequestFunc && typeof onNewRequestFunc === 'function') {
//         onNewRequestFunc(newValue);
//       }
//     },
//   }));

export default createComponent(AutoComplete, ({
  input: { onChange, value },
  onNewRequest,
  dataSourceConfig,
  ...props
}) => ({
  ...mapError(props),
  dataSourceConfig,
  searchText: dataSourceConfig ? value[dataSourceConfig.text] : value,
  onNewRequest: (onNewValue) => {
    onChange(onNewValue);
    if (onNewRequest) {
      onNewRequest(onNewValue);
    }
  },
}))
;
