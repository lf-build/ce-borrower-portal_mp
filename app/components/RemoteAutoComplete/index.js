/**
*
* RemoteAutoComplete
*
*/

import React from 'react';
import MUIAutoComplete from 'material-ui/AutoComplete';
import uplink from '@sigma-infosolutions/uplink/sagas/uplink/uplink';

import AutoComplete from '../AutoComplete';

// import styled from 'styled-components';

const executeLink = ({ dock, section, command, payload, token, workflowId }) => uplink.execute(token, workflowId, dock, section, command, payload);

const styles = {
  loaderStyle: {
    backgroundImage: 'url(data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==)',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'right center',
    textTransform: 'uppercase',
  },
};

class RemoteAutoComplete extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.onUpdateInput = this.onUpdateInput.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.state = {
      dataSource: [],
      inputValue: '',
      debounce: 0,
    };
  }
  onUpdateInput(inputValue) {
    const self = this;
    this.setState({
      inputValue,
    }, () => {
      self.performSearch();
    });
  }
  resetDebounce(input) {
    return new Promise((resolve) => {
      setTimeout(() => {
        if (input === this.state.inputValue) {
          this.setState({ debounce: this.state.debounce + 1 });
          executeLink({
            dock: 'on-boarding',
            section: 'lookup',
            command: 'company',
            payload: {
              searchText: this.state.inputValue,
            },
          }).then((d) => {
            this.setState({ debounce: this.state.debounce - 1 });
            resolve();
            if (this.state.inputValue !== '') {
              this.setState({
                dataSource: d.body.map((company) => ({ cin: company.cin, name: company.name })),
              });
            }
          })
          .catch(() => {
            this.setState({
              debounce: this.state.debounce - 1,
              dataSource: [],
            });
          });
        }
      }, 1000);
    });
  }
  performSearch() {
    if (this.state.inputValue !== '' && this.state.inputValue.length >= 3) {
      this.resetDebounce(this.state.inputValue);
    } else {
      this.setState({
        dataSource: [],
      });
    }
    this.props.empNameChange(this.state.inputValue);
  }

  handleBlur() {
    if (this.state.inputValue !== '') {
      this.props.onBlurEvent(this.state.inputValue);
    }
  }
  render() {
    const { empNameChange, onBlurEvent, ...rest } = this.props; // eslint-disable-line
    const menuProps = {
      menuItemStyle: {
        overflowWrap: 'break-word',
        whiteSpace: 'normal',
        // borderBottom: '1px solid #79cdd5',
        padding: '0px !important',
        margin: '0px !important',
        lineHeight: '0px !important',
        verticalAlign: 'middle',
      },
    };
    return (<AutoComplete
      // filter={MUIAutoComplete.fuzzyFilter} // here using db to get company names.
      filter={MUIAutoComplete.noFilter} // changed to noFilter, because now using autocompleteapi.
      dataSource={this.state.dataSource}
      onUpdateInput={this.onUpdateInput}
      menuProps={menuProps}
      {...rest}
      inputStyle={this.state.debounce ? { ...styles.loaderStyle } : { textTransform: 'uppercase' }}
      onBlur={this.handleBlur}
    />);
  }
}

RemoteAutoComplete.propTypes = {
  empNameChange: React.PropTypes.func,
  onBlurEvent: React.PropTypes.func,
};

export default RemoteAutoComplete;
