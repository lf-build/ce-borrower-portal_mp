/**
*
* Faq
*
*/

import React from 'react';
import styled from 'styled-components';

const Header = styled.p`
    margin-left: 0px;
    color: #565656;
    font-size: 13px;
    font-weight: 600;
    margin-bottom: 10px;
    cursor: pointer;
`;

class Faq extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
    };
  }

  render() {
    const { question, answer } = this.props;

    const Panel = styled.div`
    margin-left: 20px;
    margin-top: 15px;
      `;
    const PanelContent = styled.div`
    display: none;
    color: #888888;
    font-size: 13px;
    font-weight: 400;
    margin-bottom: 10px;
    margin-top: -7px;
    // margin-left: 10px;
      `;

    const questionsExpanded = this.state.expanded;
    const handleToggle = (expanded) => () => this.setState({ expanded: !expanded });
    return (
      <Panel>
        {/* <FormattedMessage {...messages.header} />*/}
        <Header onClick={handleToggle(questionsExpanded)}>{question}</Header>
        <PanelContent style={{ display: questionsExpanded ? 'block' : 'none' }}>
          {/* eslint-disable react/no-danger */}
          <div dangerouslySetInnerHTML={{ __html: answer }} />
          {/* eslint-enable react/no-danger */}
        </PanelContent>
      </Panel>
    );
  }
}

Faq.propTypes = {
  question: React.PropTypes.string,
  answer: React.PropTypes.string,
};

export default Faq;
