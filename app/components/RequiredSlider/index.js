/**
*
* RequiredSlider
*
*/

import React from 'react';
// import styled from 'styled-components';
import { Field } from 'redux-form/immutable';
// import Slider from 'redux-form-material-ui/lib/Slider';
import Slider from '../FormSlider';
import {
  required,
  minValue,
  maxValue,
  defaultMaxLen,
  defaultMinLen,
} from '../validators';

class RequiredSlider extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { name } = this.props;
    return (
      <Field
        name={name}
        component={Slider}
        disableFocusRipple
        defaultValue={25000}
        format={null}
        min={25000}
        max={1000000}
        step={1000}
        warn={[
          required(name, `Please select ${name} from slider`),
          minValue(name, 25000 || defaultMinLen),
          maxValue(name, 1000000 || defaultMaxLen),
        ]}
      />
    );
  }
}

RequiredSlider.propTypes = {
  name: React.PropTypes.string,
};

export default RequiredSlider;
