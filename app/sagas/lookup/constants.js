/*
 *
 * workflow constants
 *
 */

export const LOOKUP_REQUEST_REQUEST = 'app/lookup/LOOKUP_REQUEST_REQUEST';
export const LOOKUP_REQUEST_STARTED = 'app/lookup/LOOKUP_REQUEST_STARTED';
export const LOOKUP_REQUEST_FULFILLED = 'app/lookup/LOOKUP_REQUEST_FULFILLED';
export const LOOKUP_REQUEST_FAILED = 'app/lookup/LOOKUP_REQUEST_FAILED';
export const LOOKUP_REQUEST_ENDED = 'app/lookup/LOOKUP_REQUEST_ENDED';
