import uplinkSaga from '@sigma-infosolutions/uplink/sagas/uplink';
import LoanStepSaga from '../containers/LoanStep/sagas';
import NavigationSaga from '../containers/NavigationHelper/sagas';
// import AppSaga from '../containers/App/sagas';
// import workflowSaga from './workflow';
import lookupSaga from './lookup';
import authSaga from './auth';
export default [
  // ...AppSaga,
  ...NavigationSaga,
  ...LoanStepSaga,
  ...uplinkSaga,
  ...lookupSaga,
  ...authSaga,
];
