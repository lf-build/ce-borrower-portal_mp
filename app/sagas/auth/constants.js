/*
 *
 * SignInPage constants
 *
 */

export const DEFAULT_ACTION = 'app/SignInPage/DEFAULT_ACTION';
export const USER_SIGNED_IN = 'app/SignInPage/USER_SIGNED_IN';
export const LOAD_PROFILE = 'app/SignInPage/LOAD_PROFILE';
export const USER_INFO_AVAILABLE = 'app/SignInPage/USER_INFO_AVAILABLE';
export const USER_SIGNED_OUT = 'app/SignInPage/USER_SIGNED_OUT';
